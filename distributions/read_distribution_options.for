      subroutine read_distribution_options(datafile)
      implicit none
      include 'max.inc'
      include 'formats.inc'
      include 'distribution_parameters.inc'
c     
c     Purpose: to read the user file controlling the
c     options relative to distributions 
c     
c     Input:
c       + datafile: file to read
c     
c     Output:
c       + 
c     
c     I/O
      character*(Nchar_mx) datafile
c     temp
      integer i,ios,io_err
      logical keep_reading
      character*(Nchar_mx) line
      integer idx
      integer type(1:Ndist_mx)
      integer dist(1:Ndist_mx)
      integer vali(1:Ndist_mx)
      double precision vald(1:Ndist_mx)
      integer mini(1:Ndist_mx)
      double precision mind(1:Ndist_mx)
      integer maxi(1:Ndist_mx)
      double precision maxd(1:Ndist_mx)
      integer rafi,tmpi1,tmpi2,tmpi3
      double precision tmpd1,tmpd2
      integer Nparameters
c     label
      character*(Nchar_mx) label
      label='subroutine read_distribution_options'

      open(11,file=trim(datafile),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(datafile)
         stop
      endif
c     The 9 first lines should be commentaries
      do i=1,7
         read(11,*,iostat=io_err)
         if (io_err.ne.0) then
            call error(label)
            write(*,*) 'while reading preamble comments'
            stop
         endif
      enddo                     ! i
      idx=0
      keep_reading=.true.
      do while (keep_reading)
         read(11,10,iostat=io_err) line ! comment
         if (io_err.ne.0) then
            keep_reading=.false.
            goto 111
         else
            idx=idx+1
            if (idx.gt.Ndist_mx) then
               call error(label)
               write(*,*) 'Ndist_mx was reached'
               stop
            endif               ! idx>Ndist_mx
         endif                  ! io_err
         call identify_data_type(line,type(idx))
         read(11,*,iostat=io_err) dist(idx)
         if (io_err.ne.0) then
            call error(label)
            write(*,*) 'Data should be provided for parameter:'
            write(*,*) trim(line)
            stop
         endif
         backspace(11)
         if (dist(idx).eq.1) then ! fixed distribution for the parameter
            if (type(idx).eq.1) then ! parameter takes integer values
               read(11,*,iostat=io_err) rafi,vali(idx)
            else                ! parameter takes double values
               read(11,*,iostat=io_err) rafi,vald(idx)
            endif               ! type(idx)
         else                   ! other distributions (uniform, normal, etc)
            if (type(idx).eq.1) then ! parameter takes integer values
               read(11,*,iostat=io_err) rafi,mini(idx),maxi(idx)
            else                ! parameter takes double values
               read(11,*,iostat=io_err) rafi,mind(idx),maxd(idx)
            endif               ! type(idx)
         endif                  ! dist(idx)
         if (io_err.ne.0) then
            call error(label)
            write(*,*) 'Bad data for parameter:'
            write(*,*) trim(line)
            write(*,*) 'Maybe data type mismatch ?'
            stop
         endif
 111     continue
      enddo ! while (keep_reading)
      close(11)
      Nparameters=idx

c     Set parameters that will be used by the code
      building0_level1_Nfloor_dist=dist(1)
      building0_level1_Nfloor_N=vali(1)
      building0_level1_Nfloor_Nmin=mini(1)
      building0_level1_Nfloor_Nmax=maxi(1)
      building1_level1_Nfloor_dist=dist(1)
      building1_level1_Nfloor_N=vali(1)
      building1_level1_Nfloor_Nmin=mini(1)
      building1_level1_Nfloor_Nmax=maxi(1)
      building2_level1_Nfloor_dist=dist(1)
      building2_level1_Nfloor_N=vali(1)
      building2_level1_Nfloor_Nmin=mini(1)
      building2_level1_Nfloor_Nmax=maxi(1)
      building3_level1_Nfloor_dist=dist(1)
      building3_level1_Nfloor_N=vali(1)
      building3_level1_Nfloor_Nmin=mini(1)
      building3_level1_Nfloor_Nmax=maxi(1)
      building4_level1_Nfloor_dist=dist(1)
      building4_level1_Nfloor_N=vali(1)
      building4_level1_Nfloor_Nmin=mini(1)
      building4_level1_Nfloor_Nmax=maxi(1)
      
      return
      end


      
      subroutine identify_data_type(line,type)
      implicit none
      include 'max.inc'
c     
c     Purpose: identify the type of distribution data
c     
c     Input:
c       + line: character string (comment for the distribution)
c     
c     Output:
c       + type: data distribution type (1: integer, 2: double)
c     
c     I/O
      character*(Nchar_mx) line
      integer type
c     temp
      integer length
      character*3 tstr
c     label
      character*(Nchar_mx) label
      label='subroutine identify_data_type'

      length=len_trim(line)
      tstr=line(length-2:length)
c     Debug
c      write(*,*) 'line='
c      write(*,*) trim(line)
c      write(*,*) 'tstr="',trim(tstr),'"'
c     Debug
      if (trim(tstr).eq.'[I]') then
         type=1
      else if (trim(tstr).eq.'[D]') then
         type=2
      else
         call error(label)
         write(*,*) 'Could not identify data type from line:'
         write(*,*) trim(line)
         stop
      endif
      
      return
      end
