# procedural_generator

The purpose of "procedural_generator" is to produce definition files
that are used by the "city_generator" program.

## How to build

procedural_generator is written in fortran; you should first install
a fortran compiler. It was developped using gfortran, but if you
want to use any other compiler, you should replace "gfortran"
by the name of your compiler in the "Makefile".

Then the program can be compiled using command "make". This should
produce the "procedural_generator.exe" executable, that can be run using
command "./procedural_generator.exe".

Whenever a modidication is made to a source file, running the "make" command
again will recompile the modified file(s), then link the object files
again in order to produce the new executable.

If a include file is modified, the executable must be recompiled
from scratch. In order to do this, run the "make clean" command
before running the "make" command again, in order to clean
existing object files.


## License

Copyright (C) 2020 [|Meso|Star>](http://www.meso-star.com). procedural_generator is free
software released under the GPL v3+ license: GNU GPL version 3 or later. You
are welcome to redistribute it under certain conditions; refer to the COPYING
file for details.
