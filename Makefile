# SHELL = /bin/sh
MAINDIR=$(shell /bin/pwd)
UN=$(shell echo ${USER})
HOST=$(shell echo `hostname`)
EXEC=PG2.exe
FOR=gfortran #-L/usr/lib
MAKE=make

ARCH=-m64 #-march=i686
OPTI=-O3 -static -ffixed-line-length-none #-fast
WARNING=-fbounds-check -Warray-bounds -Walign-commons
DEBUG=
OSV=Linux
OSTYPEV=$(shell echo ${OSTYPE})


#############################
# FFLAGS= -O
# FFLAGS= -ff90
# FFLAGS= -ftrength-reduce (les loop vont plus vite cf gcc)
# FFLAGS= -g -ftrenght-reduce (cf gcc option va unused corteu)
############################Mieux vaut ajuster par des directives de compilation
############################inserer dans le prog quand le compilateur le permet.
FFLAGS = $(ARCH) $(OPTI) $(WARNING) $(IEEE) $(EMUL90) $(DEBUG)

####################################################################################
#   DIR is the path to source files directory				#
#   OBJ is the path to object files directory				#
#   SRC_PROG is the list of source files to be compiled			#
#   INCLUDES is: -I+path to directory that contains 'include' files	#
####################################################################################

DIR=${MAINDIR}/source
OBJ=${MAINDIR}/objects
DAT=${MAINDIR}/data


SRC_PROG = \
	$(DIR)/add_backslash_if_absent.for \
	$(DIR)/add_slash_if_absent.for \
	$(DIR)/add_semicolon_if_absent.for \
	$(DIR)/arctan.for \
	$(DIR)/contours.for \
	$(DIR)/coordinates.for \
	$(DIR)/dble2str.for \
	$(DIR)/delaunay_triangulation.for \
	$(DIR)/dummy.for \
	$(DIR)/error.for \
	$(DIR)/exec.for \
	$(DIR)/files.for \
	$(DIR)/geometry.for \
	$(DIR)/get_nlines.for \
	$(DIR)/generate_bridges.for \
	$(DIR)/generate_rivers.for \
	$(DIR)/generate_roads.for \
	$(DIR)/generate_zones.for \
	$(DIR)/init.for \
	$(DIR)/intersections.for \
	$(DIR)/matrix.for \
	$(DIR)/num2str.for \
	$(DIR)/print_size.for \
	$(DIR)/procedural_generator.for \
	$(DIR)/random_gen.for \
	$(DIR)/read_building_probabilities.for \
	$(DIR)/read_data.for \
	$(DIR)/read_distribution_options.for \
	$(DIR)/read_materials_file.for \
	$(DIR)/read_options.for \
	$(DIR)/read_tree_file.for \
	$(DIR)/rectangular_map.for \
	$(DIR)/rotation.for \
	$(DIR)/sample_normal_law.for \
	$(DIR)/tetris3.for \
	$(DIR)/tracks.for \
	$(DIR)/trees.for \
	$(DIR)/triangle_files.for \
	$(DIR)/yaml.for \
	$(DIR)/zufall.for

INCLUDES= -I./includes

#dependencies
#quoi compiler ?
#OBJ_PROG=$(SRC_PROG:$(DIR)/%.for=$(OBJ)/%.o)
OBJ_PROG=$(SRC_PROG:$(DIR)/%.for=$(OBJ)/%.o)
#comment compiler ?
$(OBJ)/%.o: $(DIR)/%.for
	$(FOR) $(FFLAGS) $(INCLUDES) -c -o $@ $(@:$(OBJ)/%.o=$(DIR)/%.for)

to :	all

clean :
	@rm -f *.o $(OBJ)/*.o *.oub *.oup *.oug
	@rm -f *%
	@rm -f *~
	@rm -f $(DIR)/*.~
	@rm -f $(DIR)/*.*~
	@rm -f tmp.tmp
	@rm -f *.sum
	@rm -f *.eps
	@rm -f *.ps
	@rm -f *.jpg
	@rm -f core
	@rm -f last.kumac
	@rm -f paw.metafile
	@rm -f core
	@touch core
	@chmod 000 core
	@chmod a+r core
	@echo 'Files have been removed'

dat :
	@cd ./data;f0;make_data.exe
	@echo 'Program has been compiled'

wipe :
	@rm -f *%
	@rm -f *~
	@rm -f $(DIR)/*.~
	@rm -f $(DIR)/*.*~
	@rm -f tmp.tmp
	@rm -f *.sum
	@rm -f *.eps
	@rm -f *.ps
	@rm -f *.jpg
	@rm -f core
	@rm -f last.kumac
	@rm -f paw.metafile
	@rm -f core
	@touch core
	@chmod 000 core
	@echo 'Files have been removed'

skel :	clean
	rm -f $(EXEC)
	@echo 'Files have been removed'

where :
	@echo Current directory: $(MAINDIR)
	@echo Source directory: $(DIR)

info :
	@echo Host name: $(HOSTNAME)
	@echo Operating system: $(OSV)
	@echo Type of system: $(OSTYPEV)
	@echo User name: $(USER)
	@echo Special compilation options: $(SPEC)


all :	
	@echo $(DIR)
	@echo '------ Compilation -------'
	make -f Makefile main

main :	$(OBJ_PROG)
	@echo '------ Edition de liens -------'
	$(FOR) -o $(EXEC) $(OBJ_PROG) $(SPEC)
	@echo '--- Use program ' $(EXEC)  '(compiled for :' $(OSV) $(OSTYPEV) 'by:' $(USER) ')'
	@date > compdate.in

run :
	@$(EXEC)
	@echo 'Done'

runmulti :
	@echo 'Differents MCM stationnaires'
	@echo '/!\Pousuivre si les fichiers *.in existent'
	@echo 'de 1 a 19'
	@echo '#! /bin/sh' > z1.txt
	@echo 'N=1' > z2.txt
	@echo 'FIN=19' > z3.txt
	read leurre
	cat z1.txt z2.txt z3.txt monlotmulti2_sstete.bat > monlotmulti2.bat
	rm z1.txt z2.txt z3.txt
	at now +0 minutes < monlotmulti.bat

#outils pour debugger autres que print
gbd :
	@echo 'gdb ou xxgdb (main)'

#outils pour profiling : optimisations scalaire (ou vectorielle)
gprof :
#	@gprof -s -l main.exe gmon.out > gprof_sl	
	@gprof $(EXEC) gmon.out > gprof_sl
	@more gprof_sl

edit :
	xemacs &

save :
	@sauve.1d
	@echo 'done'

shell :
	xterm

man :
	make help

help :
	@echo '|------------------------------------------|'
	@echo '|------------------------------------------|'
	@echo '|  AIDE-MEMOIRE                            |'
	@echo '|------------------------------------------|'
	@echo '| help, edit, shell                        |'
	@echo '| gdb, gprof for debug                     |'
	@echo '|------------------------------------------|'
	@echo '| clean: remove  (.o, *~, etc.)            |'
	@echo '| skel: remove ' $(EXEC) '                 |'
	@echo '| to, all, run, runmulti                   |'
	@echo '|------------------------------------------|'
