c     ----------------------------------------------------
c	Number of floors
c     ----------------------------------------------------
	integer building0_level1_Nfloor_dist
	integer building1_level1_Nfloor_dist
	integer building2_level1_Nfloor_dist
	integer building3_level1_Nfloor_dist
	integer building4_level1_Nfloor_dist
	integer building0_level2_Nfloor_dist
	integer building1_level2_Nfloor_dist
	integer building2_level2_Nfloor_dist
	integer building3_level2_Nfloor_dist
	integer building4_level2_Nfloor_dist
	integer building0_level1_Nfloor_N
	integer building0_level1_Nfloor_Nmin
	integer building0_level1_Nfloor_Nmax
	double precision building0_level1_Nfloor_mu
	double precision building0_level1_Nfloor_sigma
	integer building1_level1_Nfloor_N
	integer building1_level1_Nfloor_Nmin
	integer building1_level1_Nfloor_Nmax
	double precision building1_level1_Nfloor_mu
	double precision building1_level1_Nfloor_sigma
	integer building2_level1_Nfloor_N
	integer building2_level1_Nfloor_Nmin
	integer building2_level1_Nfloor_Nmax
	double precision building2_level1_Nfloor_mu
	double precision building2_level1_Nfloor_sigma
	integer building3_level1_Nfloor_N
	integer building3_level1_Nfloor_Nmin
	integer building3_level1_Nfloor_Nmax
	double precision building3_level1_Nfloor_mu
	double precision building3_level1_Nfloor_sigma
	integer building4_level1_Nfloor_N
	integer building4_level1_Nfloor_Nmin
	integer building4_level1_Nfloor_Nmax
	double precision building4_level1_Nfloor_mu
	double precision building4_level1_Nfloor_sigma
	integer building0_level2_Nfloor_N
	integer building0_level2_Nfloor_Nmin
	integer building0_level2_Nfloor_Nmax
	double precision building0_level2_Nfloor_mu
	double precision building0_level2_Nfloor_sigma
	integer building1_level2_Nfloor_N
	integer building1_level2_Nfloor_Nmin
	integer building1_level2_Nfloor_Nmax
	double precision building1_level2_Nfloor_mu
	double precision building1_level2_Nfloor_sigma
	integer building2_level2_Nfloor_N
	integer building2_level2_Nfloor_Nmin
	integer building2_level2_Nfloor_Nmax
	double precision building2_level2_Nfloor_mu
	double precision building2_level2_Nfloor_sigma
	integer building3_level2_Nfloor_N
	integer building3_level2_Nfloor_Nmin
	integer building3_level2_Nfloor_Nmax
	double precision building3_level2_Nfloor_mu
	double precision building3_level2_Nfloor_sigma
	integer building4_level2_Nfloor_N
	integer building4_level2_Nfloor_Nmin
	integer building4_level2_Nfloor_Nmax
	double precision building4_level2_Nfloor_mu
	double precision building4_level2_Nfloor_sigma
c     ----------------------------------------------------
c	Height of floors
c     ----------------------------------------------------
	integer building0_level1_Hfloor_dist
	integer building1_level1_Hfloor_dist
	integer building2_level1_Hfloor_dist
	integer building3_level1_Hfloor_dist
	integer building4_level1_Hfloor_dist
	integer building0_level2_Hfloor_dist
	integer building1_level2_Hfloor_dist
	integer building2_level2_Hfloor_dist
	integer building3_level2_Hfloor_dist
	integer building4_level2_Hfloor_dist
	integer building0_level1_Hfloor_H
	double precision building0_level1_Hfloor_Hmin
	double precision building0_level1_Hfloor_Hmax
	double precision building0_level1_Hfloor_mu
	double precision building0_level1_Hfloor_sigma
	double precision building1_level1_Hfloor_H
	double precision building1_level1_Hfloor_Hmin
	double precision building1_level1_Hfloor_Hmax
	double precision building1_level1_Hfloor_mu
	double precision building1_level1_Hfloor_sigma
	double precision building2_level1_Hfloor_H
	double precision building2_level1_Hfloor_Hmin
	double precision building2_level1_Hfloor_Hmax
	double precision building2_level1_Hfloor_mu
	double precision building2_level1_Hfloor_sigma
	double precision building3_level1_Hfloor_H
	double precision building3_level1_Hfloor_Hmin
	double precision building3_level1_Hfloor_Hmax
	double precision building3_level1_Hfloor_mu
	double precision building3_level1_Hfloor_sigma
	double precision building4_level1_Hfloor_H
	double precision building4_level1_Hfloor_Hmin
	double precision building4_level1_Hfloor_Hmax
	double precision building4_level1_Hfloor_mu
	double precision building4_level1_Hfloor_sigma
	double precision building0_level2_Hfloor_H
	double precision building0_level2_Hfloor_Hmin
	double precision building0_level2_Hfloor_Hmax
	double precision building0_level2_Hfloor_mu
	double precision building0_level2_Hfloor_sigma
	double precision building1_level2_Hfloor_H
	double precision building1_level2_Hfloor_Hmin
	double precision building1_level2_Hfloor_Hmax
	double precision building1_level2_Hfloor_mu
	double precision building1_level2_Hfloor_sigma
	double precision building2_level2_Hfloor_H
	double precision building2_level2_Hfloor_Hmin
	double precision building2_level2_Hfloor_Hmax
	double precision building2_level2_Hfloor_mu
	double precision building2_level2_Hfloor_sigma
	double precision building3_level2_Hfloor_H
	double precision building3_level2_Hfloor_Hmin
	double precision building3_level2_Hfloor_Hmax
	double precision building3_level2_Hfloor_mu
	double precision building3_level2_Hfloor_sigma
	double precision building4_level2_Hfloor_H
	double precision building4_level2_Hfloor_Hmin
	double precision building4_level2_Hfloor_Hmax
	double precision building4_level2_Hfloor_mu
	double precision building4_level2_Hfloor_sigma
c     ----------------------------------------------------
c     Trees
c     ----------------------------------------------------
      integer type01_tree_Height_dist
      double precision type01_tree_Height_H
      double precision type01_tree_Height_Hmin
      double precision type01_tree_Height_Hmax
      double precision type01_tree_Height_mu
      double precision type01_tree_Height_sigma
      integer type02_tree_Height_dist
      double precision type02_tree_Height_H
      double precision type02_tree_Height_Hmin
      double precision type02_tree_Height_Hmax
      double precision type02_tree_Height_mu
      double precision type02_tree_Height_sigma
      integer type03_tree_Height_dist
      double precision type03_tree_Height_H
      double precision type03_tree_Height_Hmin
      double precision type03_tree_Height_Hmax
      double precision type03_tree_Height_mu
      double precision type03_tree_Height_sigma
      integer type04_tree_Height_dist
      double precision type04_tree_Height_H
      double precision type04_tree_Height_Hmin
      double precision type04_tree_Height_Hmax
      double precision type04_tree_Height_mu
      double precision type04_tree_Height_sigma
      integer type05_tree_Height_dist
      double precision type05_tree_Height_H
      double precision type05_tree_Height_Hmin
      double precision type05_tree_Height_Hmax
      double precision type05_tree_Height_mu
      double precision type05_tree_Height_sigma
      integer type06_tree_Height_dist
      double precision type06_tree_Height_H
      double precision type06_tree_Height_Hmin
      double precision type06_tree_Height_Hmax
      double precision type06_tree_Height_mu
      double precision type06_tree_Height_sigma
      integer type07_tree_Height_dist
      double precision type07_tree_Height_H
      double precision type07_tree_Height_Hmin
      double precision type07_tree_Height_Hmax
      double precision type07_tree_Height_mu
      double precision type07_tree_Height_sigma
      integer type08_tree_Height_dist
      double precision type08_tree_Height_H
      double precision type08_tree_Height_Hmin
      double precision type08_tree_Height_Hmax
      double precision type08_tree_Height_mu
      double precision type08_tree_Height_sigma
c     ----------------------------------------------------
c     Terrains length and width
c     ----------------------------------------------------
      integer terrain_level1_length_dist
      double precision terrain_level1_length_L
      double precision terrain_level1_length_Lmin
      double precision terrain_level1_length_Lmax
      double precision terrain_level1_length_mu
      double precision terrain_level1_length_sigma
      integer terrain_level1_width_dist
      double precision terrain_level1_width_L
      double precision terrain_level1_width_Lmin
      double precision terrain_level1_width_Lmax
      double precision terrain_level1_width_mu
      double precision terrain_level1_width_sigma
      integer terrain_level2_length_dist
      double precision terrain_level2_length_L
      double precision terrain_level2_length_Lmin
      double precision terrain_level2_length_Lmax
      double precision terrain_level2_length_mu
      double precision terrain_level2_length_sigma
      integer terrain_level2_width_dist
      double precision terrain_level2_width_L
      double precision terrain_level2_width_Lmin
      double precision terrain_level2_width_Lmax
      double precision terrain_level2_width_mu
      double precision terrain_level2_width_sigma
c     ----------------------------------------------------
c     Fraction of terrains area used by buildings
c     ----------------------------------------------------
      integer tafubb_level1_dist
      double precision tafubb_level1_val
      double precision tafubb_level1_valmin
      double precision tafubb_level1_valmax
      double precision tafubb_level1_mu
      double precision tafubb_level1_sigma
      integer tafubb_level2_dist
      double precision tafubb_level2_val
      double precision tafubb_level2_valmin
      double precision tafubb_level2_valmax
      double precision tafubb_level2_mu
      double precision tafubb_level2_sigma
c     ----------------------------------------------------
c     Length and width of rooms
c     ----------------------------------------------------
      integer room_level1_length_dist
      double precision room_level1_length_L
      double precision room_level1_length_Lmin
      double precision room_level1_length_Lmax
      double precision room_level1_length_sigma
      double precision room_level1_length_mu
      integer room_level1_width_dist
      double precision room_level1_width_L
      double precision room_level1_width_Lmin
      double precision room_level1_width_Lmax
      double precision room_level1_width_sigma
      double precision room_level1_width_mu
      integer room_level2_length_dist
      double precision room_level2_length_L
      double precision room_level2_length_Lmin
      double precision room_level2_length_Lmax
      double precision room_level2_length_sigma
      double precision room_level2_length_mu
      integer room_level2_width_dist
      double precision room_level2_width_L
      double precision room_level2_width_Lmin
      double precision room_level2_width_Lmax
      double precision room_level2_width_sigma
      double precision room_level2_width_mu
c     ----------------------------------------------------
c     Walls thickness
c     ----------------------------------------------------
      integer ein_level1_dist
      double precision ein_level1_L
      double precision ein_level1_Lmin
      double precision ein_level1_Lmax
      double precision ein_level1_mu
      double precision ein_level1_sigma
      integer eext_level1_dist
      double precision eext_level1_L
      double precision eext_level1_Lmin
      double precision eext_level1_Lmax
      double precision eext_level1_mu
      double precision eext_level1_sigma
      integer ein_level2_dist
      double precision ein_level2_L
      double precision ein_level2_Lmin
      double precision ein_level2_Lmax
      double precision ein_level2_mu
      double precision ein_level2_sigma
      integer eext_level2_dist
      double precision eext_level2_L
      double precision eext_level2_Lmin
      double precision eext_level2_Lmax
      double precision eext_level2_mu
      double precision eext_level2_sigma
c     ----------------------------------------------------
c     Glass thickness
c     ----------------------------------------------------
      integer eglass_level1_dist
      double precision eglass_level1_L
      double precision eglass_level1_Lmin
      double precision eglass_level1_Lmax
      double precision eglass_level1_mu
      double precision eglass_level1_sigma
      integer eglass_level2_dist
      double precision eglass_level2_L
      double precision eglass_level2_Lmin
      double precision eglass_level2_Lmax
      double precision eglass_level2_mu
      double precision eglass_level2_sigma
c     ----------------------------------------------------
c     Fraction of surface used by windows
c     ----------------------------------------------------
      integer fwall_level1_dist
      double precision fwall_level1_val
      double precision fwall_level1_valmin
      double precision fwall_level1_valmax
      double precision fwall_level1_mu
      double precision fwall_level1_sigma
      integer ffloor_level1_dist
      double precision ffloor_level1_val
      double precision ffloor_level1_valmin
      double precision ffloor_level1_valmax
      double precision ffloor_level1_mu
      double precision ffloor_level1_sigma
      integer fwall_level2_dist
      double precision fwall_level2_val
      double precision fwall_level2_valmin
      double precision fwall_level2_valmax
      double precision fwall_level2_mu
      double precision fwall_level2_sigma
      integer ffloor_level2_dist
      double precision ffloor_level2_val
      double precision ffloor_level2_valmin
      double precision ffloor_level2_valmax
      double precision ffloor_level2_mu
      double precision ffloor_level2_sigma
c     ----------------------------------------------------
c     Rivers & bridges
c     ----------------------------------------------------
      integer river_width_dist
      double precision river_width_val
      double precision river_width_valmin
      double precision river_width_valmax
      double precision river_width_mu
      double precision river_width_sigma
      integer interbridge_distance_dist
      double precision interbridge_distance_val
      double precision interbridge_distance_valmin
      double precision interbridge_distance_valmax
      double precision interbridge_distance_mu
      double precision interbridge_distance_sigma
c     ----------------------------------------------------
	common /com1/ building0_level1_Nfloor_dist
	common /com2/ building1_level1_Nfloor_dist
	common /com3/ building2_level1_Nfloor_dist
	common /com4/ building3_level1_Nfloor_dist
	common /com5/ building4_level1_Nfloor_dist
	common /com6/ building0_level2_Nfloor_dist
	common /com7/ building1_level2_Nfloor_dist
	common /com8/ building2_level2_Nfloor_dist
	common /com9/ building3_level2_Nfloor_dist
	common /com10/ building4_level2_Nfloor_dist
	common /com11/ building0_level1_Nfloor_N
	common /com12/ building1_level1_Nfloor_N
	common /com13/ building2_level1_Nfloor_N
	common /com14/ building3_level1_Nfloor_N
	common /com15/ building4_level1_Nfloor_N
	common /com16/ building0_level2_Nfloor_N
	common /com17/ building1_level2_Nfloor_N
	common /com18/ building2_level2_Nfloor_N
	common /com19/ building3_level2_Nfloor_N
	common /com20/ building4_level2_Nfloor_N
	common /com21/ building0_level1_Nfloor_Nmin
	common /com22/ building1_level1_Nfloor_Nmin
	common /com23/ building2_level1_Nfloor_Nmin
	common /com24/ building3_level1_Nfloor_Nmin
	common /com25/ building4_level1_Nfloor_Nmin
	common /com26/ building0_level2_Nfloor_Nmin
	common /com27/ building1_level2_Nfloor_Nmin
	common /com28/ building2_level2_Nfloor_Nmin
	common /com29/ building3_level2_Nfloor_Nmin
	common /com30/ building4_level2_Nfloor_Nmin
	common /com31/ building0_level1_Nfloor_Nmax
	common /com32/ building1_level1_Nfloor_Nmax
	common /com33/ building2_level1_Nfloor_Nmax
	common /com34/ building3_level1_Nfloor_Nmax
	common /com35/ building4_level1_Nfloor_Nmax
	common /com36/ building0_level2_Nfloor_Nmax
	common /com37/ building1_level2_Nfloor_Nmax
	common /com38/ building2_level2_Nfloor_Nmax
	common /com39/ building3_level2_Nfloor_Nmax
	common /com40/ building4_level2_Nfloor_Nmax
	common /com41/ building0_level1_Nfloor_mu
	common /com42/ building1_level1_Nfloor_mu
	common /com43/ building2_level1_Nfloor_mu
	common /com44/ building3_level1_Nfloor_mu
	common /com45/ building4_level1_Nfloor_mu
	common /com46/ building0_level2_Nfloor_mu
	common /com47/ building1_level2_Nfloor_mu
	common /com48/ building2_level2_Nfloor_mu
	common /com49/ building3_level2_Nfloor_mu
	common /com50/ building4_level2_Nfloor_mu
	common /com51/ building0_level1_Nfloor_sigma
	common /com52/ building1_level1_Nfloor_sigma
	common /com53/ building2_level1_Nfloor_sigma
	common /com54/ building3_level1_Nfloor_sigma
	common /com55/ building4_level1_Nfloor_sigma
	common /com56/ building0_level2_Nfloor_sigma
	common /com57/ building1_level2_Nfloor_sigma
	common /com58/ building2_level2_Nfloor_sigma
	common /com59/ building3_level2_Nfloor_sigma
	common /com60/ building4_level2_Nfloor_sigma
c     ----------------------------------------------------
	common /com61/ building0_level1_Hfloor_dist
	common /com62/ building1_level1_Hfloor_dist
	common /com63/ building2_level1_Hfloor_dist
	common /com64/ building3_level1_Hfloor_dist
	common /com65/ building4_level1_Hfloor_dist
	common /com66/ building0_level2_Hfloor_dist
	common /com67/ building1_level2_Hfloor_dist
	common /com68/ building2_level2_Hfloor_dist
	common /com69/ building3_level2_Hfloor_dist
	common /com70/ building4_level2_Hfloor_dist
	common /com71/ building0_level1_Hfloor_H
	common /com72/ building1_level1_Hfloor_H
	common /com73/ building2_level1_Hfloor_H
	common /com74/ building3_level1_Hfloor_H
	common /com75/ building4_level1_Hfloor_H
	common /com76/ building0_level2_Hfloor_H
	common /com77/ building1_level2_Hfloor_H
	common /com78/ building2_level2_Hfloor_H
	common /com79/ building3_level2_Hfloor_H
	common /com80/ building4_level2_Hfloor_H
	common /com81/ building0_level1_Hfloor_Hmin
	common /com82/ building1_level1_Hfloor_Hmin
	common /com83/ building2_level1_Hfloor_Hmin
	common /com84/ building3_level1_Hfloor_Hmin
	common /com85/ building4_level1_Hfloor_Hmin
	common /com86/ building0_level2_Hfloor_Hmin
	common /com87/ building1_level2_Hfloor_Hmin
	common /com88/ building2_level2_Hfloor_Hmin
	common /com89/ building3_level2_Hfloor_Hmin
	common /com90/ building4_level2_Hfloor_Hmin
	common /com91/ building0_level1_Hfloor_Hmax
	common /com92/ building1_level1_Hfloor_Hmax
	common /com93/ building2_level1_Hfloor_Hmax
	common /com94/ building3_level1_Hfloor_Hmax
	common /com95/ building4_level1_Hfloor_Hmax
	common /com96/ building0_level2_Hfloor_Hmax
	common /com97/ building1_level2_Hfloor_Hmax
	common /com98/ building2_level2_Hfloor_Hmax
	common /com99/ building3_level2_Hfloor_Hmax
	common /com100/ building4_level2_Hfloor_Hmax
	common /com101/ building0_level1_Hfloor_mu
	common /com102/ building1_level1_Hfloor_mu
	common /com103/ building2_level1_Hfloor_mu
	common /com104/ building3_level1_Hfloor_mu
	common /com105/ building4_level1_Hfloor_mu
	common /com106/ building0_level2_Hfloor_mu
	common /com107/ building1_level2_Hfloor_mu
	common /com108/ building2_level2_Hfloor_mu
	common /com109/ building3_level2_Hfloor_mu
	common /com110/ building4_level2_Hfloor_mu
	common /com111/ building0_level1_Hfloor_sigma
	common /com112/ building1_level1_Hfloor_sigma
	common /com113/ building2_level1_Hfloor_sigma
	common /com114/ building3_level1_Hfloor_sigma
	common /com115/ building4_level1_Hfloor_sigma
	common /com116/ building0_level2_Hfloor_sigma
	common /com117/ building1_level2_Hfloor_sigma
	common /com118/ building2_level2_Hfloor_sigma
	common /com119/ building3_level2_Hfloor_sigma
	common /com120/ building4_level2_Hfloor_sigma
c     ----------------------------------------------------
	common /com121/ type01_tree_Height_dist
	common /com122/ type01_tree_Height_H
	common /com123/ type01_tree_Height_Hmin
	common /com124/ type01_tree_Height_Hmax
	common /com125/ type01_tree_Height_mu
	common /com126/ type01_tree_Height_sigma
	common /com127/ type02_tree_Height_dist
	common /com128/ type02_tree_Height_H
	common /com129/ type02_tree_Height_Hmin
	common /com130/ type02_tree_Height_Hmax
	common /com131/ type02_tree_Height_mu
	common /com132/ type02_tree_Height_sigma
	common /com133/ type03_tree_Height_dist
	common /com134/ type03_tree_Height_H
	common /com135/ type03_tree_Height_Hmin
	common /com136/ type03_tree_Height_Hmax
	common /com137/ type03_tree_Height_mu
	common /com138/ type03_tree_Height_sigma
	common /com139/ type04_tree_Height_dist
	common /com140/ type04_tree_Height_H
	common /com141/ type04_tree_Height_Hmin
	common /com142/ type04_tree_Height_Hmax
	common /com143/ type04_tree_Height_mu
	common /com144/ type04_tree_Height_sigma
	common /com145/ type05_tree_Height_dist
	common /com146/ type05_tree_Height_H
	common /com147/ type05_tree_Height_Hmin
	common /com148/ type05_tree_Height_Hmax
	common /com149/ type05_tree_Height_mu
	common /com150/ type05_tree_Height_sigma
	common /com151/ type06_tree_Height_dist
	common /com152/ type06_tree_Height_H
	common /com153/ type06_tree_Height_Hmin
	common /com154/ type06_tree_Height_Hmax
	common /com155/ type06_tree_Height_mu
	common /com156/ type06_tree_Height_sigma
	common /com157/ type07_tree_Height_dist
	common /com158/ type07_tree_Height_H
	common /com159/ type07_tree_Height_Hmin
	common /com160/ type07_tree_Height_Hmax
	common /com161/ type07_tree_Height_mu
	common /com162/ type07_tree_Height_sigma
	common /com163/ type08_tree_Height_dist
	common /com164/ type08_tree_Height_H
	common /com165/ type08_tree_Height_Hmin
	common /com166/ type08_tree_Height_Hmax
	common /com167/ type08_tree_Height_mu
	common /com168/ type08_tree_Height_sigma
c     ----------------------------------------------------
	common /com169/ terrain_level1_length_dist
	common /com170/ terrain_level1_length_L
	common /com171/ terrain_level1_length_Lmin
	common /com172/ terrain_level1_length_Lmax
	common /com173/ terrain_level1_length_mu
	common /com174/ terrain_level1_length_sigma
	common /com175/ terrain_level1_width_dist
	common /com176/ terrain_level1_width_L
	common /com177/ terrain_level1_width_Lmin
	common /com178/ terrain_level1_width_Lmax
	common /com179/ terrain_level1_width_mu
	common /com180/ terrain_level1_width_sigma
	common /com181/ terrain_level2_length_dist
	common /com182/ terrain_level2_length_L
	common /com183/ terrain_level2_length_Lmin
	common /com184/ terrain_level2_length_Lmax
	common /com185/ terrain_level2_length_mu
	common /com186/ terrain_level2_length_sigma
	common /com187/ terrain_level2_width_dist
	common /com188/ terrain_level2_width_L
	common /com189/ terrain_level2_width_Lmin
	common /com190/ terrain_level2_width_Lmax
	common /com191/ terrain_level2_width_mu
	common /com192/ terrain_level2_width_sigma
c     ----------------------------------------------------
	common /com193/ tafubb_level1_dist
	common /com194/ tafubb_level1_val
	common /com195/ tafubb_level1_valmin
	common /com196/ tafubb_level1_valmax
	common /com197/ tafubb_level1_mu
	common /com198/ tafubb_level1_sigma
	common /com199/ tafubb_level2_dist
	common /com200/ tafubb_level2_val
	common /com201/ tafubb_level2_valmin
	common /com202/ tafubb_level2_valmax
	common /com203/ tafubb_level2_mu
	common /com204/ tafubb_level2_sigma
c     ----------------------------------------------------
	common /com205/ room_level1_length_dist
	common /com206/ room_level1_length_L
	common /com207/ room_level1_length_Lmin
	common /com208/ room_level1_length_Lmax
	common /com209/ room_level1_length_mu
	common /com210/ room_level1_length_sigma
	common /com211/ room_level1_width_dist
	common /com212/ room_level1_width_L
	common /com213/ room_level1_width_Lmin
	common /com214/ room_level1_width_Lmax
	common /com215/ room_level1_width_mu
	common /com216/ room_level1_width_sigma
	common /com217/ room_level2_length_dist
	common /com218/ room_level2_length_L
	common /com219/ room_level2_length_Lmin
	common /com220/ room_level2_length_Lmax
	common /com221/ room_level2_length_mu
	common /com222/ room_level2_length_sigma
	common /com223/ room_level2_width_dist
	common /com224/ room_level2_width_L
	common /com225/ room_level2_width_Lmin
	common /com226/ room_level2_width_Lmax
	common /com227/ room_level2_width_mu
	common /com228/ room_level2_width_sigma
c     ----------------------------------------------------
	common /com229/ ein_level1_dist
	common /com230/ ein_level1_L
	common /com231/ ein_level1_Lmin
	common /com232/ ein_level1_Lmax
	common /com233/ ein_level1_mu
	common /com234/ ein_level1_sigma
	common /com235/ eext_level1_dist
	common /com236/ eext_level1_L
	common /com237/ eext_level1_Lmin
	common /com238/ eext_level1_Lmax
	common /com239/ eext_level1_mu
	common /com240/ eext_level1_sigma
	common /com241/ ein_level2_dist
	common /com242/ ein_level2_L
	common /com243/ ein_level2_Lmin
	common /com244/ ein_level2_Lmax
	common /com245/ ein_level2_mu
	common /com246/ ein_level2_sigma
	common /com247/ eext_level2_dist
	common /com248/ eext_level2_L
	common /com249/ eext_level2_Lmin
	common /com250/ eext_level2_Lmax
	common /com251/ eext_level2_mu
	common /com252/ eext_level2_sigma
c     ----------------------------------------------------
	common /com253/ eglass_level1_dist
	common /com254/ eglass_level1_L
	common /com255/ eglass_level1_Lmin
	common /com256/ eglass_level1_Lmax
	common /com257/ eglass_level1_mu
	common /com258/ eglass_level1_sigma
	common /com259/ eglass_level2_dist
	common /com260/ eglass_level2_L
	common /com261/ eglass_level2_Lmin
	common /com262/ eglass_level2_Lmax
	common /com263/ eglass_level2_mu
	common /com264/ eglass_level2_sigma
c     ----------------------------------------------------
	common /com265/ fwall_level1_dist
	common /com266/ fwall_level1_val
	common /com267/ fwall_level1_valmin
	common /com268/ fwall_level1_valmax
	common /com269/ fwall_level1_mu
	common /com270/ fwall_level1_sigma
	common /com271/ ffloor_level1_dist
	common /com272/ ffloor_level1_val
	common /com273/ ffloor_level1_valmin
	common /com274/ ffloor_level1_valmax
	common /com275/ ffloor_level1_mu
	common /com276/ ffloor_level1_sigma
	common /com277/ fwall_level2_dist
	common /com278/ fwall_level2_val
	common /com279/ fwall_level2_valmin
	common /com280/ fwall_level2_valmax
	common /com281/ fwall_level2_mu
	common /com282/ fwall_level2_sigma
	common /com283/ ffloor_level2_dist
	common /com284/ ffloor_level2_val
	common /com285/ ffloor_level2_valmin
	common /com286/ ffloor_level2_valmax
	common /com287/ ffloor_level2_mu
	common /com288/ ffloor_level2_sigma
c     ----------------------------------------------------
	common /com289/ river_width_dist
	common /com290/ river_width_val
	common /com291/ river_width_valmin
	common /com292/ river_width_valmax
	common /com293/ river_width_mu
	common /com294/ river_width_sigma
	common /com295/ interbridge_distance_dist
	common /com296/ interbridge_distance_val
	common /com297/ interbridge_distance_valmin
	common /com298/ interbridge_distance_valmax
	common /com299/ interbridge_distance_mu
	common /com300/ interbridge_distance_sigma
c     ----------------------------------------------------
c     ----------------------------------------------------