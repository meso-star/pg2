c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine read_tree_file(filename,
     &     Nmat,mat,obj_file,cdf)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read a file that defines a list
c     of possible materials along with their probability of occurence
c     
c     Input:
c       + filename: name of the file
c     
c     Output:
c       + Nmat: number of materials
c       + mat: list of materials
c       + obj_file: obj file associated to each material
c       + cdf: cumulated density function for choosing materials
c     
c     I/O
      character*(Nchar_mx) filename
      integer Nmat
      character*(Nchar_mx) mat(1:Nmat_mx)
      character*(Nchar_mx) obj_file(1:Nmat_mx)
      double precision cdf(0:Nproba_mx)
c     temp
      integer ios,iostatus,i
      logical keep_looking
      double precision p,fcdf
      character*(Nchar_mx) str,str2
      double precision proba(1:Nmat_mx)
c     label
      character*(Nchar_mx) label
      label='read_tree_file'

      open(11,file=trim(filename),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(filename)
         stop
      else
         do i=1,5
            read(11,*)
         enddo                  ! i
         Nmat=0
         keep_looking=.true.
         do while (keep_looking)
            read(11,*,iostat=iostatus) p,str,str2
            if (iostatus.eq.0) then
               Nmat=Nmat+1
               if (Nmat.gt.Nmat_mx) then
                  call error(label)
                  write(*,*) 'Nmat=',Nmat
                  write(*,*) '> Nmat_mx=',Nmat_mx
                  stop
               endif
               proba(Nmat)=p
               mat(Nmat)=trim(str)
               obj_file(Nmat)=trim(str2)
            else
               keep_looking=.false.
            endif
         enddo                  ! while (keep_looking)
      endif
      close(11)

      if (Nmat.gt.Nproba_mx) then
         call error(label)
         write(*,*) 'Nmat=',Nmat
         write(*,*) '> Nproba_mx=',Nproba_mx
         stop
      else
         cdf(0)=0.0D+0
         do i=1,Nmat
            cdf(i)=cdf(i-1)+proba(i)
         enddo                  ! i
         fcdf=cdf(Nmat)
         if (fcdf.gt.1) then
            do i=1,Nmat
               cdf(i)=cdf(i)/fcdf
            enddo               ! i
         endif                  ! fcdf>1
      endif                     ! Nmat>Nproba_mx
      
      return
      end
