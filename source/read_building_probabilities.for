c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine read_building_probabilities(filename,global_p)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read a file that defines the probability to draw a building
c     
c     Input:
c       + filename: name of the file
c     
c     Output:
c       + global_p: global probability to draw a building
c     
c     I/O
      character*(Nchar_mx) filename
      double precision global_p
c     temp
      integer ios,i
c     label
      character*(Nchar_mx) label
      label='read_building_probabilities'

      open(11,file=trim(filename),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(filename)
         stop
      else
         do i=1,3
            read(11,*)
         enddo                  ! i
         read(11,*) global_p
         if ((global_p.lt.0.0D+0).or.(global_p.gt.1.0D+0)) then
            call error(label)
            write(*,*) 'global_p=',global_p
            write(*,*) 'should be in the [0,1] range'
            stop
         endif
      endif
      close(11)

      return
      end
