      subroutine read_distribution_options(datafile)
      implicit none
      include 'max.inc'
      include 'formats.inc'
      include 'distribution_parameters.inc'
c     
c     Purpose: to read the user file controlling the
c     options relative to distributions 
c     
c     Input:
c       + datafile: file to read
c     
c     Output:
c       + all parameters that are defined in 'includes/distribution_parameters.inc'
c     
c     I/O
      character*(Nchar_mx) datafile
c     temp
      integer i,ios,io_err
      logical keep_reading
      character*(Nchar_mx) line
      integer idx
      integer type(1:Ndist_mx)
      integer dist(1:Ndist_mx)
      double precision val(1:Ndist_mx)
      double precision d1(1:Ndist_mx)
      double precision d2(1:Ndist_mx)
      integer rafi,tmpi1,tmpi2,tmpi3
      double precision tmpd1,tmpd2
      integer Nparameters
c     label
      character*(Nchar_mx) label
      label='subroutine read_distribution_options'

      open(11,file=trim(datafile),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(datafile)
         stop
      endif
c     The 9 first lines should be commentaries
      do i=1,7
         read(11,*,iostat=io_err)
         if (io_err.ne.0) then
            call error(label)
            write(*,*) 'while reading preamble comments'
            stop
         endif
      enddo                     ! i
      idx=0
      keep_reading=.true.
      do while (keep_reading)
         read(11,10,iostat=io_err) line ! comment
         if (io_err.ne.0) then
            keep_reading=.false.
            goto 111
         else
            idx=idx+1
            if (idx.gt.Ndist_mx) then
               call error(label)
               write(*,*) 'Ndist_mx was reached'
               stop
            endif               ! idx>Ndist_mx
         endif                  ! io_err
         read(11,*,iostat=io_err) dist(idx)
         if (io_err.ne.0) then
            call error(label)
            write(*,*) 'Data should be provided for parameter:'
            write(*,*) trim(line)
            stop
         endif
         if ((dist(idx).lt.1).or.(dist(idx).gt.3)) then
            call error(label)
            write(*,*) 'Distribution type for:'
            write(*,*) trim(line)
            write(*,*) 'should be in the [1-3] range'
            stop
         endif
         backspace(11)
         if (dist(idx).eq.1) then ! fixed distribution for the parameter
            read(11,*,iostat=io_err) rafi,val(idx)
            if (io_err.ne.0) then
               call error(label)
               write(*,*) 'Bad data for parameter:'
               write(*,*) trim(line)
               stop
            endif
            if (val(idx).le.0) then
               call error(label)
               write(*,*) 'Single value for:'
               write(*,*) trim(line)
               write(*,*) 'should be positive'
               stop
            endif
         else if ((dist(idx).eq.2).or.(dist(idx).eq.3)) then ! uniform distribution
            read(11,*,iostat=io_err) rafi,d1(idx),d2(idx)
            if (io_err.ne.0) then
               call error(label)
               write(*,*) 'Bad data for parameter:'
               write(*,*) trim(line)
               stop
            endif
            if (d1(idx).le.0.0D+0) then
               call error(label)
               write(*,*) 'First value for:'
               write(*,*) trim(line)
               write(*,*) 'should be positive'
               stop
            endif
            if (d2(idx).le.0.0D+0) then
               call error(label)
               write(*,*) 'Second value for:'
               write(*,*) trim(line)
               write(*,*) 'should be positive'
               stop
            endif
            if ((dist(idx).eq.2).and.(d2(idx).lt.d1(idx))) then
               call error(label)
               write(*,*) 'Second value for:'
               write(*,*) trim(line)
               write(*,*) 'should be greater than the first'
               stop
            endif
         endif                  ! dist(idx)
 111     continue
      enddo ! while (keep_reading)
      close(11)
      Nparameters=idx

c     Set parameters that will be used by the code
c     ----------------------------------------------------
c     Number of floors
c     ----------------------------------------------------
c     level 1
      building0_level1_Nfloor_dist=dist(1)
      if (building0_level1_Nfloor_dist.eq.1) then
         building0_level1_Nfloor_N=val(1)
      else if (building0_level1_Nfloor_dist.eq.2) then
         building0_level1_Nfloor_Nmin=int(d1(1))
         building0_level1_Nfloor_Nmax=int(d2(1))
      else if (building0_level1_Nfloor_dist.eq.3) then
         building0_level1_Nfloor_mu=d1(1)
         building0_level1_Nfloor_sigma=d2(1)
      endif                     ! building0_level1_Nfloor_dist
      building1_level1_Nfloor_dist=dist(2)
      if (building1_level1_Nfloor_dist.eq.1) then
         building1_level1_Nfloor_N=val(2)
      else if (building1_level1_Nfloor_dist.eq.2) then
         building1_level1_Nfloor_Nmin=int(d1(2))
         building1_level1_Nfloor_Nmax=int(d2(2))
      else if (building1_level1_Nfloor_dist.eq.3) then
         building1_level1_Nfloor_mu=d1(2)
         building1_level1_Nfloor_sigma=d2(2)
      endif                     ! building1_level1_Nfloor_dist
      building2_level1_Nfloor_dist=dist(3)
      if (building2_level1_Nfloor_dist.eq.1) then
         building2_level1_Nfloor_N=val(3)
      else if (building2_level1_Nfloor_dist.eq.2) then
         building2_level1_Nfloor_Nmin=int(d1(3))
         building2_level1_Nfloor_Nmax=int(d2(3))
      else if (building2_level1_Nfloor_dist.eq.3) then
         building2_level1_Nfloor_mu=d1(3)
         building2_level1_Nfloor_sigma=d2(3)
      endif                     ! building2_level1_Nfloor_dist
      building3_level1_Nfloor_dist=dist(4)
      if (building3_level1_Nfloor_dist.eq.1) then
         building3_level1_Nfloor_N=val(4)
      else if (building3_level1_Nfloor_dist.eq.2) then
         building3_level1_Nfloor_Nmin=int(d1(4))
         building3_level1_Nfloor_Nmax=int(d2(4))
      else if (building3_level1_Nfloor_dist.eq.3) then
         building3_level1_Nfloor_mu=d1(4)
         building3_level1_Nfloor_sigma=d2(4)
      endif                     ! building3_level1_Nfloor_dist
      building4_level1_Nfloor_dist=dist(5)
      if (building4_level1_Nfloor_dist.eq.1) then
         building4_level1_Nfloor_N=val(5)
      else if (building4_level1_Nfloor_dist.eq.2) then
         building4_level1_Nfloor_Nmin=int(d1(5))
         building4_level1_Nfloor_Nmax=int(d2(5))
      else if (building4_level1_Nfloor_dist.eq.3) then
         building4_level1_Nfloor_mu=d1(5)
         building4_level1_Nfloor_sigma=d2(5)
      endif                     ! building4_level1_Nfloor_dist
c     level 2
      building0_level2_Nfloor_dist=dist(6)
      if (building0_level2_Nfloor_dist.eq.1) then
         building0_level2_Nfloor_N=val(6)
      else if (building0_level2_Nfloor_dist.eq.2) then
         building0_level2_Nfloor_Nmin=int(d1(6))
         building0_level2_Nfloor_Nmax=int(d2(6))
      else if (building0_level2_Nfloor_dist.eq.3) then
         building0_level2_Nfloor_mu=d1(6)
         building0_level2_Nfloor_sigma=d2(6)
      endif                     ! building0_level2_Nfloor_dist
      building1_level2_Nfloor_dist=dist(7)
      if (building1_level2_Nfloor_dist.eq.1) then
         building1_level2_Nfloor_N=val(7)
      else if (building1_level2_Nfloor_dist.eq.2) then
         building1_level2_Nfloor_Nmin=int(d1(7))
         building1_level2_Nfloor_Nmax=int(d2(7))
      else if (building1_level2_Nfloor_dist.eq.3) then
         building1_level2_Nfloor_mu=d1(7)
         building1_level2_Nfloor_sigma=d2(7)
      endif                     ! building1_level2_Nfloor_dist
      building2_level2_Nfloor_dist=dist(8)
      if (building2_level2_Nfloor_dist.eq.1) then
         building2_level2_Nfloor_N=val(8)
      else if (building2_level2_Nfloor_dist.eq.2) then
         building2_level2_Nfloor_Nmin=int(d1(8))
         building2_level2_Nfloor_Nmax=int(d2(8))
      else if (building2_level2_Nfloor_dist.eq.3) then
         building2_level2_Nfloor_mu=d1(8)
         building2_level2_Nfloor_sigma=d2(8)
      endif                     ! building2_level2_Nfloor_dist
      building3_level2_Nfloor_dist=dist(9)
      if (building3_level2_Nfloor_dist.eq.1) then
         building3_level2_Nfloor_N=val(9)
      else if (building3_level2_Nfloor_dist.eq.2) then
         building3_level2_Nfloor_Nmin=int(d1(9))
         building3_level2_Nfloor_Nmax=int(d2(9))
      else if (building3_level2_Nfloor_dist.eq.3) then
         building3_level2_Nfloor_mu=d1(9)
         building3_level2_Nfloor_sigma=d2(9)
      endif                     ! building3_level2_Nfloor_dist
      building4_level2_Nfloor_dist=dist(10)
      if (building4_level2_Nfloor_dist.eq.1) then
         building4_level2_Nfloor_N=val(10)
      else if (building4_level2_Nfloor_dist.eq.2) then
         building4_level2_Nfloor_Nmin=int(d1(10))
         building4_level2_Nfloor_Nmax=int(d2(10))
      else if (building4_level2_Nfloor_dist.eq.3) then
         building4_level2_Nfloor_mu=d1(10)
         building4_level2_Nfloor_sigma=d2(10)
      endif                     ! building4_level2_Nfloor_dist
c     ----------------------------------------------------
c     Height of floors
c     ----------------------------------------------------
c     level 1
      building0_level1_Hfloor_dist=dist(11)
      if (building0_level1_Hfloor_dist.eq.1) then
         building0_level1_Hfloor_H=val(1)
      else if (building0_level1_Hfloor_dist.eq.2) then
         building0_level1_Hfloor_Hmin=d1(11)
         building0_level1_Hfloor_Hmax=d2(11)
      else if (building0_level1_Hfloor_dist.eq.3) then
         building0_level1_Hfloor_mu=d1(11)
         building0_level1_Hfloor_sigma=d2(11)
      endif                     ! building0_level1_Hfloor_dist
      building1_level1_Hfloor_dist=dist(12)
      if (building1_level1_Hfloor_dist.eq.1) then
         building1_level1_Hfloor_H=val(12)
      else if (building1_level1_Hfloor_dist.eq.2) then
         building1_level1_Hfloor_Hmin=d1(12)
         building1_level1_Hfloor_Hmax=d2(12)
      else if (building1_level1_Hfloor_dist.eq.3) then
         building1_level1_Hfloor_mu=d1(12)
         building1_level1_Hfloor_sigma=d2(12)
      endif                     ! building1_level1_Hfloor_dist
      building2_level1_Hfloor_dist=dist(13)
      if (building2_level1_Hfloor_dist.eq.1) then
         building2_level1_Hfloor_H=val(13)
      else if (building2_level1_Hfloor_dist.eq.2) then
         building2_level1_Hfloor_Hmin=d1(13)
         building2_level1_Hfloor_Hmax=d2(13)
      else if (building2_level1_Hfloor_dist.eq.3) then
         building2_level1_Hfloor_mu=d1(13)
         building2_level1_Hfloor_sigma=d2(13)
      endif                     ! building2_level1_Hfloor_dist
      building3_level1_Hfloor_dist=dist(14)
      if (building3_level1_Hfloor_dist.eq.1) then
         building3_level1_Hfloor_H=val(14)
      else if (building3_level1_Hfloor_dist.eq.2) then
         building3_level1_Hfloor_Hmin=d1(14)
         building3_level1_Hfloor_Hmax=d2(14)
      else if (building3_level1_Hfloor_dist.eq.3) then
         building3_level1_Hfloor_mu=d1(14)
         building3_level1_Hfloor_sigma=d2(14)
      endif                     ! building3_level1_Hfloor_dist
      building4_level1_Hfloor_dist=dist(15)
      if (building4_level1_Hfloor_dist.eq.1) then
         building4_level1_Hfloor_H=val(15)
      else if (building4_level1_Hfloor_dist.eq.2) then
         building4_level1_Hfloor_Hmin=d1(15)
         building4_level1_Hfloor_Hmax=d2(15)
      else if (building4_level1_Hfloor_dist.eq.3) then
         building4_level1_Hfloor_mu=d1(15)
         building4_level1_Hfloor_sigma=d2(15)
      endif                     ! building4_level1_Hfloor_dist
c     level 2
      building0_level2_Hfloor_dist=dist(16)
      if (building0_level2_Hfloor_dist.eq.1) then
         building0_level2_Hfloor_H=val(16)
      else if (building0_level2_Hfloor_dist.eq.2) then
         building0_level2_Hfloor_Hmin=d1(16)
         building0_level2_Hfloor_Hmax=d2(16)
      else if (building0_level2_Hfloor_dist.eq.3) then
         building0_level2_Hfloor_mu=d1(16)
         building0_level2_Hfloor_sigma=d2(16)
      endif                     ! building0_level2_Hfloor_dist
      building1_level2_Hfloor_dist=dist(17)
      if (building1_level2_Hfloor_dist.eq.1) then
         building1_level2_Hfloor_H=val(17)
      else if (building1_level2_Hfloor_dist.eq.2) then
         building1_level2_Hfloor_Hmin=d1(17)
         building1_level2_Hfloor_Hmax=d2(17)
      else if (building1_level2_Hfloor_dist.eq.3) then
         building1_level2_Hfloor_mu=d1(17)
         building1_level2_Hfloor_sigma=d2(17)
      endif                     ! building1_level2_Hfloor_dist
      building2_level2_Hfloor_dist=dist(18)
      if (building2_level2_Hfloor_dist.eq.1) then
         building2_level2_Hfloor_H=val(18)
      else if (building2_level2_Hfloor_dist.eq.2) then
         building2_level2_Hfloor_Hmin=d1(18)
         building2_level2_Hfloor_Hmax=d2(18)
      else if (building2_level2_Hfloor_dist.eq.3) then
         building2_level2_Hfloor_mu=d1(18)
         building2_level2_Hfloor_sigma=d2(18)
      endif                     ! building2_level2_Hfloor_dist
      building3_level2_Hfloor_dist=dist(19)
      if (building3_level2_Hfloor_dist.eq.1) then
         building3_level2_Hfloor_H=val(19)
      else if (building3_level2_Hfloor_dist.eq.2) then
         building3_level2_Hfloor_Hmin=d1(19)
         building3_level2_Hfloor_Hmax=d2(19)
      else if (building3_level2_Hfloor_dist.eq.3) then
         building3_level2_Hfloor_mu=d1(19)
         building3_level2_Hfloor_sigma=d2(19)
      endif                     ! building3_level2_Hfloor_dist
      building4_level2_Hfloor_dist=dist(20)
      if (building4_level2_Hfloor_dist.eq.1) then
         building4_level2_Hfloor_H=val(20)
      else if (building4_level2_Hfloor_dist.eq.2) then
         building4_level2_Hfloor_Hmin=d1(20)
         building4_level2_Hfloor_Hmax=d2(20)
      else if (building4_level2_Hfloor_dist.eq.3) then
         building4_level2_Hfloor_mu=d1(20)
         building4_level2_Hfloor_sigma=d2(20)
      endif                     ! building4_level2_Hfloor_dist
c     ----------------------------------------------------
c     Height of trees
c     ----------------------------------------------------
      type01_tree_Height_dist=dist(21)
      if (type01_tree_Height_dist.eq.1) then
         type01_tree_Height_H=val(21)
      else if (type01_tree_Height_dist.eq.2) then
         type01_tree_Height_Hmin=d1(21)
         type01_tree_Height_Hmax=d1(21)
      else if (type01_tree_Height_dist.eq.3) then
         type01_tree_Height_mu=d1(21)
         type01_tree_Height_sigma=d2(21)
      endif
      type02_tree_Height_dist=dist(22)
      if (type02_tree_Height_dist.eq.1) then
         type02_tree_Height_H=val(22)
      else if (type02_tree_Height_dist.eq.2) then
         type02_tree_Height_Hmin=d1(22)
         type02_tree_Height_Hmax=d1(22)
      else if (type02_tree_Height_dist.eq.3) then
         type02_tree_Height_mu=d1(22)
         type02_tree_Height_sigma=d2(22)
      endif
      type03_tree_Height_dist=dist(23)
      if (type03_tree_Height_dist.eq.1) then
         type03_tree_Height_H=val(23)
      else if (type03_tree_Height_dist.eq.2) then
         type03_tree_Height_Hmin=d1(23)
         type03_tree_Height_Hmax=d1(23)
      else if (type03_tree_Height_dist.eq.3) then
         type03_tree_Height_mu=d1(23)
         type03_tree_Height_sigma=d2(23)
      endif
      type04_tree_Height_dist=dist(24)
      if (type04_tree_Height_dist.eq.1) then
         type04_tree_Height_H=val(24)
      else if (type04_tree_Height_dist.eq.2) then
         type04_tree_Height_Hmin=d1(24)
         type04_tree_Height_Hmax=d1(24)
      else if (type04_tree_Height_dist.eq.3) then
         type04_tree_Height_mu=d1(24)
         type04_tree_Height_sigma=d2(24)
      endif
      type05_tree_Height_dist=dist(25)
      if (type05_tree_Height_dist.eq.1) then
         type05_tree_Height_H=val(25)
      else if (type05_tree_Height_dist.eq.2) then
         type05_tree_Height_Hmin=d1(25)
         type05_tree_Height_Hmax=d1(25)
      else if (type05_tree_Height_dist.eq.3) then
         type05_tree_Height_mu=d1(25)
         type05_tree_Height_sigma=d2(25)
      endif
      type06_tree_Height_dist=dist(26)
      if (type06_tree_Height_dist.eq.1) then
         type06_tree_Height_H=val(26)
      else if (type06_tree_Height_dist.eq.2) then
         type06_tree_Height_Hmin=d1(26)
         type06_tree_Height_Hmax=d1(26)
      else if (type06_tree_Height_dist.eq.3) then
         type06_tree_Height_mu=d1(26)
         type06_tree_Height_sigma=d2(26)
      endif
      type07_tree_Height_dist=dist(27)
      if (type07_tree_Height_dist.eq.1) then
         type07_tree_Height_H=val(27)
      else if (type07_tree_Height_dist.eq.2) then
         type07_tree_Height_Hmin=d1(27)
         type07_tree_Height_Hmax=d1(27)
      else if (type07_tree_Height_dist.eq.3) then
         type07_tree_Height_mu=d1(27)
         type07_tree_Height_sigma=d2(27)
      endif
      type08_tree_Height_dist=dist(28)
      if (type08_tree_Height_dist.eq.1) then
         type08_tree_Height_H=val(28)
      else if (type08_tree_Height_dist.eq.2) then
         type08_tree_Height_Hmin=d1(28)
         type08_tree_Height_Hmax=d1(28)
      else if (type08_tree_Height_dist.eq.3) then
         type08_tree_Height_mu=d1(28)
         type08_tree_Height_sigma=d2(28)
      endif
c     ----------------------------------------------------
c     Terrains length and width
c     ----------------------------------------------------
      terrain_level1_length_dist=dist(29)
      if (terrain_level1_length_dist.eq.1) then
         terrain_level1_length_L=val(29)
      else if (terrain_level1_length_dist.eq.2) then
         terrain_level1_length_Lmin=d1(29)
         terrain_level1_length_Lmax=d2(29)
      else if (terrain_level1_length_dist.eq.3) then
         terrain_level1_length_mu=d1(29)
         terrain_level1_length_sigma=d2(29)
      endif
      terrain_level1_width_dist=dist(30)
      if (terrain_level1_width_dist.eq.1) then
         terrain_level1_width_L=val(30)
      else if (terrain_level1_width_dist.eq.2) then
         terrain_level1_width_Lmin=d1(30)
         terrain_level1_width_Lmax=d2(30)
      else if (terrain_level1_width_dist.eq.3) then
         terrain_level1_width_mu=d1(30)
         terrain_level1_width_sigma=d2(30)
      endif
      terrain_level2_length_dist=dist(31)
      if (terrain_level2_length_dist.eq.1) then
         terrain_level2_length_L=val(31)
      else if (terrain_level2_length_dist.eq.2) then
         terrain_level2_length_Lmin=d1(31)
         terrain_level2_length_Lmax=d2(31)
      else if (terrain_level2_length_dist.eq.3) then
         terrain_level2_length_mu=d1(31)
         terrain_level2_length_sigma=d2(31)
      endif
      terrain_level2_width_dist=dist(32)
      if (terrain_level2_width_dist.eq.1) then
         terrain_level2_width_L=val(32)
      else if (terrain_level2_width_dist.eq.2) then
         terrain_level2_width_Lmin=d1(32)
         terrain_level2_width_Lmax=d2(32)
      else if (terrain_level2_width_dist.eq.3) then
         terrain_level2_width_mu=d1(32)
         terrain_level2_width_sigma=d2(32)
      endif
c     ----------------------------------------------------
c     Terrains area fraction used by buildings
c     ----------------------------------------------------
      tafubb_level1_dist=dist(33)
      if (tafubb_level1_dist.eq.1) then
         tafubb_level1_val=val(33)
      else if (tafubb_level1_dist.eq.2) then
         tafubb_level1_valmin=d1(33)
         tafubb_level1_valmax=d2(33)
      else if (tafubb_level1_dist.eq.3) then
         tafubb_level1_mu=d1(33)
         tafubb_level1_sigma=d2(33)
      endif
      tafubb_level2_dist=dist(34)
      if (tafubb_level2_dist.eq.1) then
         tafubb_level2_val=val(34)
      else if (tafubb_level2_dist.eq.2) then
         tafubb_level2_valmin=d1(34)
         tafubb_level2_valmax=d2(34)
      else if (tafubb_level2_dist.eq.3) then
         tafubb_level2_mu=d1(34)
         tafubb_level2_sigma=d2(34)
      endif
c     ----------------------------------------------------
c     Rooms length and width
c     ----------------------------------------------------
      room_level1_length_dist=dist(35)
      if (room_level1_length_dist.eq.1) then
         room_level1_length_L=val(35)
      else if (room_level1_length_dist.eq.2) then
         room_level1_length_Lmin=d1(35)
         room_level1_length_Lmax=d2(35)
      else if (room_level1_length_dist.eq.3) then
         room_level1_length_mu=d1(35)
         room_level1_length_sigma=d2(35)
      endif
      room_level1_width_dist=dist(36)
      if (room_level1_width_dist.eq.1) then
         room_level1_width_L=val(36)
      else if (room_level1_width_dist.eq.2) then
         room_level1_width_Lmin=d1(36)
         room_level1_width_Lmax=d2(36)
      else if (room_level1_width_dist.eq.3) then
         room_level1_width_mu=d1(36)
         room_level1_width_sigma=d2(36)
      endif
      room_level2_length_dist=dist(37)
      if (room_level2_length_dist.eq.1) then
         room_level2_length_L=val(37)
      else if (room_level2_length_dist.eq.2) then
         room_level2_length_Lmin=d1(37)
         room_level2_length_Lmax=d2(37)
      else if (room_level2_length_dist.eq.3) then
         room_level2_length_mu=d1(37)
         room_level2_length_sigma=d2(37)
      endif
      room_level2_width_dist=dist(38)
      if (room_level2_width_dist.eq.1) then
         room_level2_width_L=val(38)
      else if (room_level2_width_dist.eq.2) then
         room_level2_width_Lmin=d1(38)
         room_level2_width_Lmax=d2(38)
      else if (room_level2_width_dist.eq.3) then
         room_level2_width_mu=d1(38)
         room_level2_width_sigma=d2(38)
      endif
c     ----------------------------------------------------
c     Thickness of walls
c     ----------------------------------------------------
      ein_level1_dist=dist(39)
      if (ein_level1_dist.eq.1) then
         ein_level1_L=val(39)
      else if (ein_level1_dist.eq.2) then
         ein_level1_Lmin=d1(39)
         ein_level1_Lmax=d2(39)
      else if (ein_level1_dist.eq.3) then
         ein_level1_mu=d1(39)
         ein_level1_sigma=d2(39)
      endif
      eext_level1_dist=dist(40)
      if (eext_level1_dist.eq.1) then
         eext_level1_L=val(40)
      else if (eext_level1_dist.eq.2) then
         eext_level1_Lmin=d1(40)
         eext_level1_Lmax=d2(40)
      else if (eext_level1_dist.eq.3) then
         eext_level1_mu=d1(40)
         eext_level1_sigma=d2(40)
      endif
      ein_level2_dist=dist(41)
      if (ein_level2_dist.eq.1) then
         ein_level2_L=val(41)
      else if (ein_level2_dist.eq.2) then
         ein_level2_Lmin=d1(41)
         ein_level2_Lmax=d2(41)
      else if (ein_level2_dist.eq.3) then
         ein_level2_mu=d1(41)
         ein_level2_sigma=d2(41)
      endif
      eext_level2_dist=dist(42)
      if (eext_level2_dist.eq.1) then
         eext_level2_L=val(42)
      else if (eext_level2_dist.eq.2) then
         eext_level2_Lmin=d1(42)
         eext_level2_Lmax=d2(42)
      else if (eext_level2_dist.eq.3) then
         eext_level2_mu=d1(42)
         eext_level2_sigma=d2(42)
      endif
c     ----------------------------------------------------
c     Thickness of glass
c     ----------------------------------------------------
      eglass_level1_dist=dist(43)
      if (eglass_level1_dist.eq.1) then
         eglass_level1_L=val(43)
      else if (eglass_level1_dist.eq.2) then
         eglass_level1_Lmin=d1(43)
         eglass_level1_Lmax=d2(43)
      else if (eglass_level1_dist.eq.3) then
         eglass_level1_mu=d1(43)
         eglass_level1_sigma=d2(43)
      endif
      eglass_level2_dist=dist(44)
      if (eglass_level2_dist.eq.1) then
         eglass_level2_L=val(44)
      else if (eglass_level2_dist.eq.2) then
         eglass_level2_Lmin=d1(44)
         eglass_level2_Lmax=d2(44)
      else if (eglass_level2_dist.eq.3) then
         eglass_level2_mu=d1(44)
         eglass_level2_sigma=d2(44)
      endif
c     ----------------------------------------------------
c     Fraction of surface used by windows
c     ----------------------------------------------------
      fwall_level1_dist=dist(45)
      if (fwall_level1_dist.eq.1) then
         fwall_level1_val=val(45)
      else if (fwall_level1_dist.eq.2) then
         fwall_level1_valmin=d1(45)
         fwall_level1_valmax=d2(45)
      else if (fwall_level1_dist.eq.3) then
         fwall_level1_mu=d1(45)
         fwall_level1_sigma=d2(45)
      endif
      ffloor_level1_dist=dist(46)
      if (ffloor_level1_dist.eq.1) then
         ffloor_level1_val=val(46)
      else if (ffloor_level1_dist.eq.2) then
         ffloor_level1_valmin=d1(46)
         ffloor_level1_valmax=d2(46)
      else if (ffloor_level1_dist.eq.3) then
         ffloor_level1_mu=d1(46)
         ffloor_level1_sigma=d2(46)
      endif
      fwall_level2_dist=dist(47)
      if (fwall_level2_dist.eq.1) then
         fwall_level2_val=val(47)
      else if (fwall_level2_dist.eq.2) then
         fwall_level2_valmin=d1(47)
         fwall_level2_valmax=d2(47)
      else if (fwall_level2_dist.eq.3) then
         fwall_level2_mu=d1(47)
         fwall_level2_sigma=d2(47)
      endif
      ffloor_level2_dist=dist(48)
      if (ffloor_level2_dist.eq.1) then
         ffloor_level2_val=val(48)
      else if (ffloor_level2_dist.eq.2) then
         ffloor_level2_valmin=d1(48)
         ffloor_level2_valmax=d2(48)
      else if (ffloor_level2_dist.eq.3) then
         ffloor_level2_mu=d1(48)
         ffloor_level2_sigma=d2(48)
      endif
c     ----------------------------------------------------
c     Rivers & bridges
c     ----------------------------------------------------
      river_width_dist=dist(49)
      if (river_width_dist.eq.1) then
         river_width_val=val(49)
      else if (river_width_dist.eq.2) then
         river_width_valmin=d1(49)
         river_width_valmax=d2(49)
      else if (river_width_dist.eq.3) then
         river_width_mu=d1(49)
         river_width_sigma=d2(49)
      endif
      interbridge_distance_dist=dist(50)
      if (interbridge_distance_dist.eq.1) then
         interbridge_distance_val=val(50)
      else if (interbridge_distance_dist.eq.2) then
         interbridge_distance_valmin=d1(50)
         interbridge_distance_valmax=d2(50)
      else if (interbridge_distance_dist.eq.3) then
         interbridge_distance_mu=d1(50)
         interbridge_distance_sigma=d2(50)
      endif
c     ----------------------------------------------------
      
      return
      end

      
      
      subroutine identify_data_type(line,type)
      implicit none
      include 'max.inc'
c     
c     Purpose: identify the type of distribution data
c     
c     Input:
c       + line: character string (comment for the distribution)
c     
c     Output:
c       + type: data distribution type (1: integer, 2: double)
c     
c     I/O
      character*(Nchar_mx) line
      integer type
c     temp
      integer length
      character*3 tstr
c     label
      character*(Nchar_mx) label
      label='subroutine identify_data_type'

      length=len_trim(line)
      tstr=line(length-2:length)
c     Debug
c      write(*,*) 'line='
c      write(*,*) trim(line)
c      write(*,*) 'tstr="',trim(tstr),'"'
c     Debug
      if (trim(tstr).eq.'[I]') then
         type=1
      else if (trim(tstr).eq.'[D]') then
         type=2
      else
         call error(label)
         write(*,*) 'Could not identify data type from line:'
         write(*,*) trim(line)
         stop
      endif
      
      return
      end
