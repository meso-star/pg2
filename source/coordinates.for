c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine distance_cart(x1,y1,z1,x2,y2,z2,d)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the distance between two points defined
c     by their cartesian coordinates
c
c     inputs
      double precision x1,y1,z1
      double precision x2,y2,z2
c     outputs
      double precision d
c     label
      integer stlren
      character*(Nchar_mx) label
      label='subroutine distance_cart'

      d=dsqrt((x2-x1)**2.0D+0+(y2-y1)**2.0D+0+(z2-z1)**2.0D+0)

      return
      end


      
      subroutine distance_between_two_points(dim,x1,x2,d)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the distance between two points
c     
c     Input:
c       + dim: dimension of space
c       + x1: first position
c       + x2: second position
c
c     Output:
c       + d: distance between x1 and x2      
c
c     I/O
      integer dim
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision d
c     temp
      integer i
      double precision d2
c     label
      integer stlren
      character*(Nchar_mx) label
      label='subroutine distance_between_two_points'

      d2=0.0D+0
      do i=1,dim
         d2=d2+(x2(i)-x1(i))**2.0D+0
      enddo                     ! i
      d=dsqrt(d2)

      return
      end
