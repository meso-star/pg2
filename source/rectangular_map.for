c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine rectangular_map(dim,periodic_map,mapXsize,mapYsize,plot_size,Nplot,draw_white_bands,
     &     Nroad,road_width,road_Nppt,road_track,
     &     Ncontour,Nppc,contour,
     &     on_left_side,on_right_side,on_bottom_side,on_top_side,
     &     contour_type)
      implicit none
      include 'max.inc'
      include 'city_parameters.inc'
c     
c     Purpose: to generate a rectangular city plan
c     
c     Input:
c       + dim: dimension of space
c       + periodic_map: T if map has to be periodic (when rectangular city plan)
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + plot_size: X/Y size of a land plot [m]
c       + Nplot: X/Y number of land plots between two roads
c       + draw_white_bands: true if white bands have to be drawn
c     
c     Output:
c       + Nroad: number of existing roads
c       + road_width: width for each road [m]
c       + road_Nppt: number of points for each road track
c       + road_track: track that define the position of each road
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + on_left_side: true if the footprint sticks to the left side of contour 1
c       + on_right_side: true if the footprint sticks to the right side of contour 1
c       + on_bottom_side: true if the footprint sticks to the bottom side of contour 1
c       + on_top_side: true if the footprint sticks to the top side of contour 1
c       + contour_type: type of contour
c         - 0: surrounding contour (contour index 1)
c         - 1: building only
c         - 2: terrain + building
c         - 3: terrain only (parks)
c         - 4: no man's land
c     
c     I/O
      integer dim
      logical periodic_map
      double precision mapXsize
      double precision mapYsize
      double precision plot_size(1:2)
      integer Nplot(1:2)
      logical draw_white_bands
      integer Nroad
      double precision road_width(1:Nroad_mx)
      integer road_Nppt(1:Nroad_mx)
      double precision road_track(1:Nroad_mx,1:Nppt_mx,1:Ndim_mx-1)
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      logical on_left_side(1:Ncontour_mx)
      logical on_right_side(1:Ncontour_mx)
      logical on_bottom_side(1:Ncontour_mx)
      logical on_top_side(1:Ncontour_mx)
      integer contour_type(1:Ncontour_mx)
c     temp
      double precision road_size
      integer Npx,Npy,Nr
      double precision px_limit(1:Nplot_mx,1:2)
      double precision py_limit(1:Nplot_mx,1:2)
      double precision r_limit(1:Nroad_mx,1:2)
      integer i,j,iroad
      integer Nppt
      double precision central_track(1:Nppt_mx,1:2)
      character*(Nchar_mx) road_mat
      character*(Nchar_mx) wbands_mat
c     label
      character*(Nchar_mx) label
      label='subroutine rectangular_map'

      Nroad=0
      Ncontour=0
      road_size=normal_road_width
c     
      call map_side_discretization(periodic_map,mapXsize,plot_size(1),Nplot(1),road_size,
     &     Npx,Nr,px_limit,r_limit)
      do iroad=1,Nr
         Nroad=Nroad+1
         if (Nroad.gt.Nroad_mx) then
            call error(label)
            write(*,*) 'Nroad_mx has been reached'
            stop
         endif
         if ((periodic_map).and.((iroad.eq.1).or.(iroad.eq.Nr))) then
            road_width(Nroad)=road_size/2.0D+0
         else
            road_width(Nroad)=road_size
         endif
         road_Nppt(Nroad)=2
         road_track(Nroad,1,1)=(r_limit(iroad,1)+r_limit(iroad,2))/2.0D+0
         road_track(Nroad,1,2)=0.0D+0
         road_track(Nroad,2,1)=road_track(Nroad,1,1)
         road_track(Nroad,2,2)=mapYsize
      enddo                     ! iroad
c     
      call map_side_discretization(periodic_map,mapYsize,plot_size(2),Nplot(2),road_size,
     &     Npy,Nr,py_limit,r_limit)
      do iroad=1,Nr
         Nroad=Nroad+1
         if (Nroad.gt.Nroad_mx) then
            call error(label)
            write(*,*) 'Nroad_mx has been reached'
            stop
         endif
         if ((periodic_map).and.((iroad.eq.1).or.(iroad.eq.Nr))) then
            road_width(Nroad)=road_size/2.0D+0
         else
            road_width(Nroad)=road_size
         endif
         road_Nppt(Nroad)=2
         road_track(Nroad,1,1)=0.0D+0
         road_track(Nroad,1,2)=(r_limit(iroad,1)+r_limit(iroad,2))/2.0D+0
         road_track(Nroad,2,1)=mapXsize
         road_track(Nroad,2,2)=road_track(Nroad,1,2)
      enddo                     ! iroad
c
      if (Npx*Npy.gt.Ncontour_mx) then
         call error(label)
         write(*,*) 'Npx=',Npx,' Npy=',Npy
         write(*,*) 'Ncontour will be equal to:',Npx*Npy
         write(*,*) '> Ncontour_mx=',Ncontour_mx
         stop
      endif
c     
      do j=1,Npy
         do i=1,Npx
            Ncontour=Ncontour+1
            if (Ncontour.gt.Ncontour_mx) then
               call error(label)
               write(*,*) 'Ncontour_mx has been reached'
               stop
            endif
            Nppc(Ncontour)=4
            contour(Ncontour,1,1)=px_limit(i,1)
            contour(Ncontour,1,2)=py_limit(j,1)
            contour(Ncontour,2,1)=px_limit(i,2)
            contour(Ncontour,2,2)=py_limit(j,1)
            contour(Ncontour,3,1)=px_limit(i,2)
            contour(Ncontour,3,2)=py_limit(j,2)
            contour(Ncontour,4,1)=px_limit(i,1)
            contour(Ncontour,4,2)=py_limit(j,2)
            on_left_side(Ncontour)=.false.
            on_right_side(Ncontour)=.false.
            on_bottom_side(Ncontour)=.false.
            on_top_side(Ncontour)=.false.
            contour_type(Ncontour)=2
         enddo                  ! i
      enddo                     ! j
c     Record road definition files
      road_mat=trim(roads_material)
      wbands_mat=trim(white_bands_material)
      do iroad=1,Nroad
         Nppt=road_Nppt(iroad)
         do i=1,Nppt
            do j=1,2
               central_track(i,j)=road_track(iroad,i,j)
            enddo               ! j
         enddo                  ! i
         call record_road_definition_file(dim,iroad,road_width(iroad),
     &        Nppt,central_track,draw_white_bands,
     &        road_mat,wbands_mat)
      enddo                     ! iroad

      return
      end



      subroutine map_side_discretization(periodic_map,mapsize,plot_size,Nplot,road_size,
     &     Np,Nr,p_limit,r_limit)
      implicit none
      include 'max.inc'
c     
c     Purpose: to discretize a map side into land plots and roads
c     
c     Input:
c       + periodic_map: T if map has to be periodic (when rectangular city plan)
c       + mapsize: size of the map side [m]
c       + plot_size: size of land plots along this side [m]
c       + Nplot: number of consecutive land plots between two roads, for this side
c       + road_size: width of a road [m]
c     
c     Output:
c       + Np: number of land plots
c       + Nr: number of roads
c       + p_limit: limits of each land plot
c       + r_limit: limits of each road
c     
c     I/O
      logical periodic_map
      double precision mapsize
      double precision plot_size
      integer Nplot
      double precision road_size
      integer Np,Nr
      double precision p_limit(1:Nplot_mx,1:2)
      double precision r_limit(1:Nroad_mx,1:2)
c     temp
      logical keep_going
      integer iplot,i
      double precision sigma
      integer Nitem
c     label
      character*(Nchar_mx) label
      label='subroutine map_side_discretization'

      Np=0
      Nr=0
      sigma=0.0D+0
      if (periodic_map) then
         Nitem=int(mapsize/(plot_size*Nplot+road_size))
c     start with half a road
         Nr=Nr+1
         if (Nr.gt.Nroad_mx) then
            call error(label)
            write(*,*) 'Nr=',Nr
            write(*,*) '> Nroad_mx=',Nroad_mx
            stop
         endif
         r_limit(Nr,1)=sigma
         sigma=sigma+road_size/2.0D+0
         r_limit(Nr,2)=sigma
c     then all items
         do i=1,Nitem
            do iplot=1,Nplot
               Np=Np+1
               if (Np.gt.Nplot_mx) then
                  call error(label)
                  write(*,*) 'Np=',Np
                  write(*,*) '> Nplot_mx=',Nplot_mx
                  stop
               endif
               p_limit(Np,1)=sigma
               sigma=sigma+plot_size
               p_limit(Np,2)=sigma
            enddo               ! iplot
            if (i.lt.Nitem) then
               Nr=Nr+1
               if (Nr.gt.Nroad_mx) then
                  call error(label)
                  write(*,*) 'Nr=',Nr
                  write(*,*) '> Nroad_mx=',Nroad_mx
                  stop
               endif
               r_limit(Nr,1)=sigma
               sigma=sigma+road_size
               r_limit(Nr,2)=sigma
            endif
         enddo                  ! i
c     finish with half a road
         Nr=Nr+1
         if (Nr.gt.Nroad_mx) then
            call error(label)
            write(*,*) 'Nr=',Nr
            write(*,*) '> Nroad_mx=',Nroad_mx
            stop
         endif
         r_limit(Nr,1)=sigma
         sigma=sigma+road_size/2.0D+0
         r_limit(Nr,2)=sigma
      else                      ! periodic_map=F
         keep_going=.true.
         do while (keep_going)
c     Always start with land plots
            do iplot=1,Nplot
               if (sigma+plot_size.le.mapsize) then
                  Np=Np+1
                  if (Np.gt.Nplot_mx) then
                     call error(label)
                     write(*,*) 'Np=',Np
                     write(*,*) '> Nplot_mx=',Nplot_mx
                     stop
                  endif
                  p_limit(Np,1)=sigma
                  sigma=sigma+plot_size
                  p_limit(Np,2)=sigma
               else
                  keep_going=.false.
                  goto 123
               endif
            enddo               ! iplot
c     Then a road
            if (sigma+road_size.le.mapsize) then
               Nr=Nr+1
               if (Nr.gt.Nroad_mx) then
                  call error(label)
                  write(*,*) 'Nr=',Nr
                  write(*,*) '> Nroad_mx=',Nroad_mx
                  stop
               endif
               r_limit(Nr,1)=sigma
               sigma=sigma+road_size
               r_limit(Nr,2)=sigma
            else
               keep_going=.false.
               goto 123
            endif
 123        continue
         enddo                  ! while (keep_going)
      endif                     ! periodic_map= T or F

      return
      end
