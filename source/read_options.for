c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine read_options(optionsfile,map_type,periodic_map)
      implicit none
      include 'max.inc'
c
c     Purpose: to read the options file
c     
c     Input:
c       + optionsfile: file to read
c     
c     Output:
c       + map_type: type of city plan [1: legacy spider's web, 2: rectangular]
c       + periodic_map: T if map has to be periodic (when rectangular city plan)
c     
c     I/O
      character*(Nchar_mx) optionsfile
      integer map_type
      logical periodic_map
c     temp
      integer ios,i
c     label
      character*(Nchar_mx) label
      label='subroutine read_options'

      open(12,file=trim(optionsfile),iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'Could not open file: ',trim(optionsfile)
         stop
      endif
      do i=1,3
         read(12,*)
      enddo                     ! i
      read(12,*) map_type
      read(12,*) 
      read(12,*) periodic_map
      close(12)

c     Consistency check
      if ((map_type.ne.1).and.(map_type.ne.2)) then
         call error(label)
         write(*,*) 'map_type=',map_type
         write(*,*) 'allowed values: 1, 2'
         stop
      endif
      
      return
      end
