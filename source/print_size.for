c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine print_size(byte_count)
      implicit none
      include 'max.inc'
c     
c     Purpose: to print the provided 'byte_count' argument as a size
c     in a friendly format
c     
c     Input:
c       + byte_count: value, interpreted as a number of bytes
c     
c     Output:
c       + on-screen message
c     
c     I/O
      integer byte_count
c     temp
      double precision value_in_Kb
      double precision value_in_Mb
      double precision value_in_Gb
      double precision value_in_Tb
c     label
      character*(Nchar_mx) label
      label='subroutine print_size'

      value_in_Kb=dble(byte_count)/1024.0D+0
      value_in_Mb=value_in_Kb/1024.0D+0
      value_in_Gb=value_in_Mb/1024.0D+0
      value_in_Tb=value_in_Gb/1024.0D+0

      if (byte_count.lt.1024) then
         write(*,*) byte_count,' bytes'
      else if (value_in_Kb.lt.1024.0D+0) then
         write(*,*) value_in_Kb,' Kb'
      else if (value_in_Mb.lt.1024.0D+0) then
         write(*,*) value_in_Mb,' Mb'
      else if (value_in_Gb.lt.1024.0D+0) then
         write(*,*) value_in_Gb,' Gb'
      else
         write(*,*) value_in_Tb,' Tb'
      endif
      
      return
      end
