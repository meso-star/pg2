c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine init()
      implicit none
      include 'max.inc'
c     
c     Purpose: to perform global initialization tasks
c     
c     Input:
c     
c     Output:
c     
c     I/O
c     temp
      character*(Nchar_mx) command,exec_file
      logical file_exists
c     label
      character*(Nchar_mx) label
      label='subroutine init'

      command='rm -f ./results/ground.in'
      call exec(command)
      command='rm -f ./results/river*.in'
      call exec(command)
      command='rm -f ./results/bridge*.in'
      call exec(command)
      command='rm -f ./results/road*.in'
      call exec(command)
      command='rm -f ./results/building*.in'
      call exec(command)
      command='rm -f ./results/zone*.in'
      call exec(command)
      command='rm -f ./results/swimmingpool*.in'
      call exec(command)
      command='rm -f ./results/vegetation.in'
      call exec(command)
      command='rm -f ./gnuplot/*.dat'
      call exec(command)
      command='rm -f ./gnuplot/gra_contours'
      call exec(command)
      command='rm -f ./results/contours_temp.dat'
      call exec(command)
      command='rm -f ./results/city.yaml'
      call exec(command)
c     Compilation of "triangle"
      exec_file='./triangle/triangle'
      command='rm -f '//trim(exec_file)
      command='cd ./triangle; ./compile_triangle.bash'
      call exec(command)
      inquire(file=trim(exec_file),exist=file_exists)
      if (.not.file_exists) then
         write(*,*) 'Compilation of TRIANGLE failed'
         stop
      endif


      return
      end
