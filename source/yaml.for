      subroutine append_yamlfile_building(id,model,dataset,height,Ncontour,Nppc,contour)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to append the "city.yaml" output file with a building
c     
c     Input:
c       + id: building index
c       + model: name of the model
c       + dataset: name of the data set
c       + height: total height of the building
c       + Ncontour: number of contours
c       + Nppc: number of points per contour
c       + contour: actual contours
c     
c     Output: appended results/city.yaml
c     
c     I/O
      integer id
      character*(Nchar_mx) model
      character*(Nchar_mx) dataset
      double precision height
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
c     temp
      character*(Nchar_mx) id_str
      character*(Nchar_mx) height_str
      character*(Nchar_mx) line
      logical err_code
      integer nap,i
      character*(Nchar_mx) xstr,ystr
c     label
      character*(Nchar_mx) label
      label='subroutine append_yamlfile_building'

      nap=6
      call num2str(id,id_str,err_code)
      if (err_code) then
         call error(label)
         write(*,*) 'Could not convert to character string: id=',id
         stop
      endif
      call dble2str(height,nap,height_str,err_code)
      if (err_code) then
         call error(label)
         write(*,*) 'Could not convert to character string: height=',height
         stop
      endif
c     
      open(31,file='./results/city.yaml',access='append')
      if (id.eq.0) then
         write(31,10) 'buildings:'
      endif
      line='  - name: batiment'//trim(id_str)
      write(31,10) trim(line)
      line='    construction_mode: '//trim(model)
      write(31,10) trim(line)
      line='    dataset: '//trim(dataset)
      write(31,10) trim(line)
      line='    height: '//trim(height_str)
      write(31,10) trim(line)
      if (Ncontour.ne.1) then
         call error(label)
         write(*,*) 'Ncontour=',Ncontour
         write(*,*) 'Only a value of 1 is valid at the moment'
         stop
      endif
      line='    footprint: ['
      do i=1,Nppc(1)
         call dble2str(contour(1,i,1),nap,xstr,err_code)
         if (err_code) then
            call error(label)
            write(*,*) 'Could not convert to character string: x=',contour(1,i,1)
            stop
         endif
         call dble2str(contour(1,i,2),nap,ystr,err_code)
         if (err_code) then
            call error(label)
            write(*,*) 'Could not convert to character string: y=',contour(1,i,2)
            stop
         endif
         line=trim(line)//' ['//trim(xstr)//', '//trim(ystr)//']'
         if (i.lt.Nppc(1)) then
            line=trim(line)//', '
         else
            line=trim(line)//']'
         endif
         if (len_trim(line).eq.Nchar_mx) then
            call error(label)
            write(*,*) 'Nchar_mx has been reached'
            stop
         endif
      enddo                     ! i
      write(31,10) trim(line)
      close(31)
      
      return
      end


      
      subroutine append_yamlfile_ground(mapXsize,mapYsize,depth)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to append the "city.yaml" output file with a ground
c     
c     Input:
c       + mapXsize: X-size of the map [m]
c       + mapYsize: Y-size of the map [m]
c       + depth: depth of the ground [m]
c     
c     Output: appended results/city.yaml
c     
c     I/O
      double precision mapXsize
      double precision mapYsize
      double precision depth
c     temp
      character*(Nchar_mx) mapXsize_str
      character*(Nchar_mx) mapYsize_str
      character*(Nchar_mx) depth_str
      character*(Nchar_mx) line
      integer nap
      logical err
c     label
      character*(Nchar_mx) label
      label='subroutine append_yamlfile_building'

      nap=6
      call dble2str(mapXsize,nap,mapXsize_str,err)
      if (err) then
         call error(label)
         write(*,*) 'Could not convert to character string: mapXsize=',mapXsize
         stop
      endif
      call dble2str(mapYsize,nap,mapYsize_str,err)
      if (err) then
         call error(label)
         write(*,*) 'Could not convert to character string: mapYsize=',mapYsize
         stop
      endif
      call dble2str(depth,nap,depth_str,err)
      if (err) then
         call error(label)
         write(*,*) 'Could not convert to character string: depth=',depth
         stop
      endif
      
      open(31,file='./results/city.yaml',access='append')
      write(31,10) 'ground:'
      line='  extent: [0.0, '//trim(mapXsize_str)//', 0.0, '//trim(mapYsize_str)//']'
      write(31,10) trim(line)
      line='  depth: '//trim(depth_str)
      write(31,10) trim(line)
      close(31)

      return
      end
      
