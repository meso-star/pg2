c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine global2local(dim,xcenter,
     &     Ncontour_global,Nppc_global,contour_global,
     &     Ncontour_local,Nppc_local,contour_local,
     &     M01,M10)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c     
c     Purpose: to convert all provided contours from the
c     global referential to a local referential
c     
c     Input:
c       + dim: dimension of space
c       + xcenter: position of the local referential center (in global referential)
c       + Ncontour_global: number of contours
c       + Nppc_global: number of points that define each contour
c       + contour_global: list of coordinates for each contour in global referential
c       
c     Output:
c       + Ncontour_local: number of contours
c       + Nppc_local: number of points that define each contour
c       + contour_local: list of coordinates for each contour in local referential
c       + M10: passage matrix from global to local referential
c       + M01: passage matrix from local to global referential
c     
c     I/O
      integer dim
      double precision xcenter(1:Ndim_mx)
      integer Ncontour_global
      integer Nppc_global(1:Ncontour_mx)
      double precision contour_global(1:Ncontour_mx,
     &     1:Nppc_mx,1:Ndim_mx-1)
      integer Ncontour_local
      integer Nppc_local(1:Ncontour_mx)
      double precision contour_local(1:Ncontour_mx,
     &     1:Nppc_mx,1:Ndim_mx-1)
      double precision M10(1:Ndim_mx,1:Ndim_mx)
      double precision M01(1:Ndim_mx,1:Ndim_mx)
c     temp
      integer icontour,i,j
      double precision theta,dx,dy
      double precision ux(1:Ndim_mx)
      double precision uy(1:Ndim_mx)
      double precision uz(1:Ndim_mx)
      double precision x_R0(1:Ndim_mx)
      double precision x_R1(1:Ndim_mx)
c     functions
      double precision arctan
c     label
      character*(Nchar_mx) label
      label='subroutine global2Local'
      
c     ux
      ux(1)=1.0D+0
      ux(2)=0.0D+0
      ux(3)=0.0D+0
c     uy
      uy(1)=0.0D+0
      uy(2)=1.0D+0
      uy(3)=0.0D+0
c     uz
      uz(1)=0.0D+0
      uz(2)=0.0D+0
      uz(3)=1.0D+0

      if (contour_global(1,1,1)-contour_global(1,4,1).eq.0.0D+0) then
         if (contour_global(1,1,2).lt.contour_global(1,4,2)) then
            theta=pi/2.0D+0
         else
            theta=-pi/2.0D+0
         endif
      else
         dx=contour_global(1,1,1)-contour_global(1,4,1)
         dy=contour_global(1,4,2)-contour_global(1,1,2)
         theta=arctan(dx,dy)
      endif
      call rotation_matrix(dim,theta,uz,M10)
      call invert_3x3_matrix(dim,M10,M01)

c     rotate the contour of the sector in order to work in local referential
      Ncontour_local=Ncontour_global
      do icontour=1,Ncontour_local
         Nppc_local(icontour)=Nppc_global(icontour)
         do i=1,Nppc_local(icontour)
            do j=1,dim-1
               x_R0(j)=contour_global(icontour,i,j)-xcenter(j)
            enddo               ! j
            x_R0(dim)=0.0D+0
            call matrix_vector(dim,M10,x_R0,x_R1)
            do j=1,dim-1
               contour_local(icontour,i,j)=x_R1(j)
            enddo               ! j
         enddo                  ! i
      enddo                     ! icontour

      return
      end


      
      subroutine local2global(dim,xcenter,
     &     Ncontour_local,Nppc_local,contour_local,
     &     Ncontour_global,Nppc_global,contour_global,
     &     M01,M10)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c     
c     Purpose: to convert all provided contours from the
c     local referential to a global referential
c     
c     Input:
c       + dim: dimension of space
c       + xcenter: position of the local referential center (in local referential)
c       + Ncontour_local: number of contours
c       + Nppc_local: number of points that define each contour
c       + contour_local: list of coordinates for each contour in local referential
c       + M10: passage matrix from global to global referential
c       + M01: passage matrix from global to global referential
c       
c     Output:
c       + Ncontour_global: number of contours
c       + Nppc_global: number of points that define each contour
c       + contour_global: list of coordinates for each contour in global referential
c     
c     I/O
      integer dim
      double precision xcenter(1:Ndim_mx)
      integer Ncontour_local
      integer Nppc_local(1:Ncontour_mx)
      double precision contour_local(1:Ncontour_mx,
     &     1:Nppc_mx,1:Ndim_mx-1)
      double precision M10(1:Ndim_mx,1:Ndim_mx)
      double precision M01(1:Ndim_mx,1:Ndim_mx)
      integer Ncontour_global
      integer Nppc_global(1:Ncontour_mx)
      double precision contour_global(1:Ncontour_mx,
     &     1:Nppc_mx,1:Ndim_mx-1)
c     temp
      integer icontour,i,j
      double precision x_R0(1:Ndim_mx)
      double precision x_R1(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine local2global'
      
c     rotate the contour of the sector in order to work in local referential
      Ncontour_global=Ncontour_local
      do icontour=1,Ncontour_global
         Nppc_global(icontour)=Nppc_local(icontour)
         do i=1,Nppc_global(icontour)
            do j=1,dim-1
               x_R1(j)=contour_local(icontour,i,j)
            enddo               ! j
            x_R1(dim)=0.0D+0
            call matrix_vector(dim,M01,x_R1,x_R0)
            do j=1,dim-1
               contour_global(icontour,i,j)=x_R0(j)+xcenter(j)
            enddo               ! j
         enddo                  ! i
      enddo                     ! icontour

      return
      end


      
      subroutine distance_to_vector(dim,x1,x2,P,dmin,A,
     &     A_is_at_end,end_side)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the distance between a given vector and a position
c     
c     Input:
c       + dim: dimension of space
c       + x1: first position that defines the vector
c       + x2: second position that defines the vector
c       + P: position
c     
c     Output:
c       + dmin: smallest distance between P and vector
c       + A: closest position to P that belongs to the vector
c       + A_is_at_end: true if A is at one end of the [x1,x2] segment
c       + end_side: index (1 or 2) or the end side if A_is_at_end=T
c     
c     I/O
      integer dim
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision P(1:Ndim_mx)
      double precision dmin
      double precision A(1:Ndim_mx)
      logical A_is_at_end
      integer end_side
c     temp
      integer j
      double precision u(1:Ndim_mx)
      double precision v(1:Ndim_mx)
      double precision nu(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
      double precision AP(1:Ndim_mx)
      double precision norm_u,norm_v,L1,d,sp
c     label
      character*(Nchar_mx) label
      label='subroutine distance_to_vector'

      call substract_vectors(dim,x2,x1,u)
      call normalize_vector(dim,u,nu)
      call substract_vectors(dim,P,x1,v)
      call vector_length(dim,u,norm_u)
      call vector_length(dim,v,norm_v)
      call vector_vector(dim,u,v,sp)
      if (norm_u.le.0.0D+0) then
         call error(label)
         write(*,*) 'norm_u=',norm_u
         write(*,*) 'u=',u
         stop
      else
         L1=sp/norm_u
      endif
      if (L1.le.0.0D+0) then
         call copy_vector(dim,x1,A)
         do j=1,dim
            tmp(j)=0.0D+0
         enddo                  ! j
         A_is_at_end=.true.
         end_side=1
      else if (L1.ge.norm_u) then
         call copy_vector(dim,x2,A)
         call copy_vector(dim,u,tmp)
         A_is_at_end=.true.
         end_side=2
      else
         call scalar_vector(dim,L1,nu,tmp)
         call add_vectors(dim,x1,tmp,A)
         A_is_at_end=.false.
      endif
      call substract_vectors(dim,v,tmp,AP)
      call vector_length(dim,AP,dmin)

      return
      end
      


      subroutine total_length(N,a,e_in,e_ext,length)
      implicit none
      include 'max.inc'
c
c     Purpose: to get the total legnth of a given building along a given axis
c
c     Input:
c       + N: number of rooms along the chosen axis
c       + a: length of a room along the chosen axis
c       + e_in: thickness in internal walls
c       + e_ext: thickness of external walls
c
c     Output:
c       + length: total length of the building along the selected axis
c
c     I/O
      integer N
      double precision a
      double precision e_in
      double precision e_ext
      double precision length
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine total_length'

      length=a*N+e_in*(N-1)+2*e_ext

      return
      end


      
      subroutine normal_for_segment(dim,P1,P2,n)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the normal between the two points that define a segment
c     The returned normal points towards the inside of a closed contour if P1 and P2
c     are two consecutive positions over a closed contour defined counter-clockwise
c     
c     Input:
c       + dim: dimension of space
c       + P1: first point of the segment
c       + P2: second point of the segment
c     
c     Output:
c       + n: normal for the segment
c     
c     I/O
      integer dim
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision n(1:Ndim_mx)
c     temp
      double precision t(1:Ndim_mx)
      double precision u(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine normal_for_segment'

      call substract_vectors(dim,P2,P1,t)
      u(1)=-t(2)
      u(2)=t(1)
      u(3)=t(3)
      call normalize_vector(dim,u,n)

      return
      end
      


      subroutine clamp_near_ground(dim,xmin,xmax,ymin,ymax,x)
      implicit none
      include 'max.inc'
c     
c     Purpose: to clamp a position close to ground zone contour
c     
c     Input:
c       + dim: dimension of space
c       + xmin,xmax,ymin,ymax: limits of the ground
c       + x: position to clamp
c     
c     Output:
c       + x: updated
c     
c     I/O
      integer dim
      double precision xmin,xmax,ymin,ymax
      double precision x(1:Ndim_mx)
c     temp
c     parameters
      double precision dx
      double precision dy
      parameter(dx=10.0D+0)
      parameter(dy=10.0D+0)
c     label
      character*(Nchar_mx) label
      label='subroutine clamp_near_ground'

      if (x(1).lt.xmin) then
         x(1)=xmin+dx
      endif
      if (x(1).gt.xmax) then
         x(1)=xmax-dx
      endif
      if (x(2).lt.ymin) then
         x(2)=ymin+dy
      endif
      if (x(2).gt.ymax) then
         x(2)=ymax-dy
      endif

      return
      end
      


      subroutine clamp_on_ground(dim,xmin,xmax,ymin,ymax,x)
      implicit none
      include 'max.inc'
c     
c     Purpose: to clamp a position on the ground zone
c     
c     Input:
c       + dim: dimension of space
c       + xmin,xmax,ymin,ymax: limits of the ground
c       + x: position to clamp
c     
c     Output:
c       + x: updated
c     
c     I/O
      integer dim
      double precision xmin,xmax,ymin,ymax
      double precision x(1:Ndim_mx)
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine clamp_on_ground'

      if (x(1).lt.xmin) then
         x(1)=xmin
      endif
      if (x(1).gt.xmax) then
         x(1)=xmax
      endif
      if (x(2).lt.ymin) then
         x(2)=ymin
      endif
      if (x(2).gt.ymax) then
         x(2)=ymax
      endif

      return
      end



      subroutine contour_bounding_box(dim,Ncontour,Nppc,contour,
     &     bounding_box)
      implicit none
      include 'max.inc'
c     
c     Purpose: to find the closest bounding box for a number of contours
c     
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c     
c     Output:
c       + bounding_box: closest bounding box for provided contours
c
c     I/O
      integer dim
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      double precision bounding_box(1:Ndim_mx-1,1:2)
c     temp
      integer icontour,i,j
c     label
      character*(Nchar_mx) label
      label='subroutine contour_bounding_box'

      do i=1,dim-1
         do j=1,2
            bounding_box(i,j)=contour(1,1,i)
         enddo                  ! j
      enddo                     ! i

      do icontour=1,Ncontour
         do i=1,Nppc(icontour)
            do j=1,dim-1
               if (contour(icontour,i,j).lt.bounding_box(j,1)) then
                  bounding_box(j,1)=contour(icontour,i,j)
               endif
               if (contour(icontour,i,j).gt.bounding_box(j,2)) then
                  bounding_box(j,2)=contour(icontour,i,j)
               endif
            enddo               ! j
         enddo                  ! i
      enddo                     ! icontour
c     
c     "bounding_box(i,j)" is for dimension i, min (j=1) or max (j=2)
c     
c                                  |                        |
c     bounding_box(i=2,j=2) --------------------------------------------
c                                  |                        |
c                                  |                        |
c                                  |                        |
c                                  |                        |
c                                  |                        |
c                                  |                        |
c                                  |                        |     
c     bounding_box(i=2,j=1) --------------------------------------------
c                                  |                        |
c                                  |                        |
c                                  |                        |
c                        bounding_box(i=1,j=1)      bounding_box(i=1,j=2)
c     
      return
      end



      subroutine get_angle(dim,u,angle)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c     
c     Purpose: to get the angle corresponding to a given vector
c     
c     Input:
c       + dim: dimension of space
c       + u: vector (better if normalized)
c     
c     Output:
c       + angle: angle [rad]
c     
c     I/O
      integer dim
      double precision u(1:Ndim_mx)
      double precision angle
c     temp
      double precision theta
c     label
      character*(Nchar_mx) label
      label='subroutine get_angle'

      if (u(1).eq.0.0D+0) then
         if (u(2).gt.0.0D+0) then
            angle=pi/2.0D+0
         else
            angle=-pi/2.0D+0
         endif
      else
         theta=dacos(dabs(u(2))/dabs(u(1)))
         if ((u(1).gt.0.0D+0).and.(u(2).gt.0.0D+0)) then
            angle=theta
         else  if ((u(1).lt.0.0D+0).and.(u(2).gt.0.0D+0)) then
            angle=pi-theta
         else  if ((u(1).lt.0.0D+0).and.(u(2).lt.0.0D+0)) then
            angle=pi+theta
         else  if ((u(1).gt.0.0D+0).and.(u(2).lt.0.0D+0)) then
            angle=2.0D+0*pi-theta
         endif
      endif                     ! u(1)=0

      return
      end



      subroutine triangle_area(dim,p1,p2,p3,area)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the area of a triangle
c     
c     Input:
c       + dim: dimension of space 
c       + p1: coordinates of the 1st point
c       + p2: coordinates of the 2nd point
c       + p3: coordinates of the 3rd point
c     
c     Output:
c       + area: area of the triangle
c     
c     I/O
      integer dim
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision p3(1:Ndim_mx)
      double precision area
c     temp
      double precision v12(1:Ndim_mx)
      double precision v13(1:Ndim_mx)
      double precision w(1:Ndim_mx)
      double precision length
c     label
      character*(Nchar_mx) label
      label='subroutine triangle_area'

      call substract_vectors(dim,p2,p1,v12)
      call substract_vectors(dim,p3,p1,v13)
      call vectorial_product(dim,v12,v13,w)
      call vector_length(dim,w,length)
      area=length/2.0D+0

      return
      end
