c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine tetris3(dim,mapXsize,mapYsize,
     &     Nsector,alpha1,xcenter,ilevel,isector,
     &     Ncontour_global,Nppc_global,contour_global,
     &     on_left_side,on_right_side,on_bottom_side,on_top_side,
     &     contour_type)
      implicit none
      include 'max.inc'
      include 'formats.inc'
      include 'constants.inc'
      include 'city_parameters.inc'
      include 'distribution_parameters.inc'
c     
c     Purpose: to subdivide a level 1 or level 2 sector
c     into a number of building contours
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Nsector: number of angular sectors
c       + alpha1: angle for generating level1 ring road contour
c       + xcenter: position of the ring center
c       + ilevel: level index [1,2]
c       + isector: index of the sector
c       + Ncontour_global: number of contours (only one at input time)
c       + Nppc_global: number of points that define each contour
c       + contour_global: list of coordinates for each contour
c     
c     Output:
c       + Ncontour_global: number of contours (updated)
c       + Nppc_global: number of points that define each contour (updated)
c       + contour_global: list of coordinates for each contour (updated)
c       + on_left_side: true if the footprint sticks to the left side of contour 1
c       + on_right_side: true if the footprint sticks to the right side of contour 1
c       + on_bottom_side: true if the footprint sticks to the bottom side of contour 1
c       + on_top_side: true if the footprint sticks to the top side of contour 1
c       + contour_type: type of contour
c         - 0: surrounding contour (contour index 1)
c         - 1: building only
c         - 2: terrain + building
c         - 3: terrain only
c     
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Nsector
      double precision alpha1
      double precision xcenter(1:Ndim_mx)
      integer ilevel
      integer isector
      integer Ncontour_global
      integer Nppc_global(1:Ncontour_mx)
      double precision contour_global(1:Ncontour_mx,
     &     1:Nppc_mx,1:Ndim_mx-1)
      logical on_left_side(1:Ncontour_mx)
      logical on_right_side(1:Ncontour_mx)
      logical on_bottom_side(1:Ncontour_mx)
      logical on_top_side(1:Ncontour_mx)
      integer contour_type(1:Ncontour_mx)
c     temp
      double precision xmin_sm1,xmax_sm1,ymin_sm1,ymax_sm1
      logical generate_sector_plot
      integer ifootprint
      double precision deltaX,deltaY
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,
     &     1:Nppc_mx,1:Ndim_mx-1)
      double precision M10(1:Ndim_mx,1:Ndim_mx)
      double precision M01(1:Ndim_mx,1:Ndim_mx)
      character*(Nchar_mx) filename
      integer Ncontour_sm
      integer Nppc_sm(1:Ncontour_mx)
      double precision contour_sm(1:Ncontour_mx,
     &     1:Nppc_mx,1:Ndim_mx-1)
      logical keep_generating_footprints
      integer Niter_max
      logical success
      double precision x0_seed(1:Ndim_mx)
      integer Np_seed
      double precision ctr_seed(1:Nppc_mx,1:Ndim_mx-1)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision P3(1:Ndim_mx)
      double precision P4(1:Ndim_mx)
      double precision P1P2(1:Ndim_mx)
      double precision P2P3(1:Ndim_mx)
      double precision P3P4(1:Ndim_mx)
      double precision P4P1(1:Ndim_mx)
      double precision u12(1:Ndim_mx)
      double precision u23(1:Ndim_mx)
      double precision u34(1:Ndim_mx)
      double precision u41(1:Ndim_mx)
      double precision u21(1:Ndim_mx)
      double precision u32(1:Ndim_mx)
      double precision u43(1:Ndim_mx)
      double precision u14(1:Ndim_mx)
      integer icontour,i,j
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      integer Np_tmp
      double precision ctr_tmp(1:Nppc_mx,1:Ndim_mx-1)
      double precision width
      logical inside
      integer Np2
      double precision ctr2(1:Nppc_mx,1:Ndim_mx-1)
      integer Np0
      double precision ctr0(1:Nppc_mx,1:Ndim_mx-1)
      integer Np_base
      double precision ctr_base(1:Nppc_mx,1:Ndim_mx-1)
      logical debug
c     label
      character*(Nchar_mx) label
      label='subroutine tetris3'

      Ncontour=1
      do icontour=2,Ncontour_mx
         Nppc(icontour)=0
      enddo                     ! icontour

      if ((ilevel.eq.2).and.(isector.eq.1)) then
         generate_sector_plot=.true.
      else
         generate_sector_plot=.false.
      endif
      Niter_max=1000

      if (Ncontour_global.ne.1) then
         call error(label)
         write(*,*) 'Ncontour_global=',Ncontour_global
         write(*,*) 'that seems awkward'
         stop
      endif
      if (Nppc_global(1).ne.4) then
         call error(label)
         write(*,*) 'Npcc_global(1)=',Nppc_global(1)
         write(*,*) 'should be equal to 4'
         stop
      endif
c     convert input "contour_global" to local "contour"
      call global2local(dim,xcenter,
     &     Ncontour_global,Nppc_global,contour_global,
     &     Ncontour,Nppc,contour,
     &     M01,M10)
c     generate inner contour for contour index 1
      call get_contour(dim,Ncontour,Nppc,contour,1,
     &     Np,ctr)
c     ctr_base is the inner contour at a distance "pavement_width'
c      call generate_close_contour(dim,Np,ctr,
c     &     pavement_width,.true.,
c     &     Np_base,ctr_base)
      call generate_close_contour2(dim,.false.,Np,ctr,
     &     0.0D+0,pavement_width,.true.,.false.,success,
     &     Np_base,ctr_base)
      if (.not.success) then
         call error(label)
         write(*,*) 'could not generate inward contour'
         write(*,*) 'for the sector at distance:'
         write(*,*) 'pavement_width=',pavement_width
         write(*,*) 'ilevel=',ilevel
         write(*,*) 'isector=',isector
         filename='./gnuplot/sector.dat'
         call record_gnuplot_contour(filename,dim,
     &        Ncontour,Nppc,contour)
         stop
      endif                     ! success=F
c     ctr0 is the inner contour at a distance "pavement_width"+0.10
c      call generate_close_contour(dim,Np,ctr,
c     &     pavement_width+0.10D+0,.true.,
c     &     Np0,ctr0)
      call generate_close_contour2(dim,.false.,Np,ctr,
     &     0.0D+0,pavement_width+0.10D+0,.true.,.false.,success,
     &     Np0,ctr0)
      if (.not.success) then
         call error(label)
         write(*,*) 'could not generate inward contour'
         write(*,*) 'for the sector at distance:'
         write(*,*) 'pavement_width+0.1=',pavement_width+0.1D+0
         write(*,*) 'ilevel=',ilevel
         write(*,*) 'isector=',isector
         stop
      endif                     ! success=F
      Ncontour_sm=0
      call add_ctr_to_contour(dim,Ncontour_sm,Nppc_sm,contour_sm,
     &     Np0,ctr0)
c     generate ctr2 for sector's bounding box
      call analyze_contour(dim,
     &     Ncontour_sm,Nppc_sm,contour_sm,1,
     &     xmin_sm1,xmax_sm1,ymin_sm1,ymax_sm1)
      Np2=4
      ctr2(1,1)=xmin_sm1
      ctr2(1,2)=ymin_sm1
      ctr2(2,1)=xmax_sm1
      ctr2(2,2)=ymin_sm1
      ctr2(3,1)=xmax_sm1
      ctr2(3,2)=ymax_sm1
      ctr2(4,1)=xmin_sm1
      ctr2(4,2)=ymax_sm1
c     then replace the first 'shadow mask' contour by ctr2
      Ncontour_sm=0
      call add_ctr_to_contour(dim,Ncontour_sm,Nppc_sm,contour_sm,
     &     Np2,ctr2)
c     Find all border directions
      call get_position_from_ctr(dim,Np0,ctr0,1,P1)
      call get_position_from_ctr(dim,Np0,ctr0,2,P2)
      call get_position_from_ctr(dim,Np0,ctr0,3,P3)
      call get_position_from_ctr(dim,Np0,ctr0,4,P4)
      call substract_vectors(dim,P2,P1,P1P2)
      call substract_vectors(dim,P3,P2,P2P3)
      call substract_vectors(dim,P4,P3,P3P4)
      call substract_vectors(dim,P1,P4,P4P1)
      call normalize_vector(dim,P1P2,u12)
      call normalize_vector(dim,P2P3,u23)
      call normalize_vector(dim,P3P4,u34)
      call normalize_vector(dim,P4P1,u41)
      call scalar_vector(dim,-1.0D+0,u12,u21)
      call scalar_vector(dim,-1.0D+0,u23,u32)
      call scalar_vector(dim,-1.0D+0,u34,u43)
      call scalar_vector(dim,-1.0D+0,u41,u14)
c     Main loop
      ifootprint=0
      keep_generating_footprints=.true.
      do while (keep_generating_footprints)
         ifootprint=ifootprint+1
c     Debug
         if (ifootprint.eq.0) then
            debug=.true.
         else
            debug=.false.
         endif
c     Debug
c     seeding
         call seeding(dim,debug,mapXsize,mapYsize,
     &        Ncontour_sm,Nppc_sm,contour_sm,
     &        Np0,ctr0,
     &        Niter_max,success,x0_seed,Np_seed,ctr_seed)
c     Debug
         if (debug) then
            if (.not.success) then
               write(*,*) 'Seeding failed, ifootprint=',ifootprint
            endif
         endif
c     Debug
         
c     Debug
         if (debug) then
            write(*,*) 'After seeding, success=',success
         endif
c     Debug
         if (ilevel.eq.1) then
            if (terrain_level1_length_dist.eq.1) then
               deltaX=terrain_level1_length_L
            else if (terrain_level1_length_dist.eq.2) then
               call uniform(terrain_level1_length_Lmin,
     &              terrain_level1_length_Lmax,
     &              deltaX)
            else if (terrain_level1_length_dist.eq.3) then               
               call sample_normal(terrain_level1_length_mu,
     &              terrain_level1_length_sigma,deltaX)
            endif               ! terrain_level1_length_dist
            if (terrain_level1_width_dist.eq.1) then
               deltaY=terrain_level1_width_L
            else if (terrain_level1_width_dist.eq.2) then
               call uniform(terrain_level1_width_Lmin,
     &              terrain_level1_width_Lmax,
     &              deltaY)
            else if (terrain_level1_width_dist.eq.3) then               
               call sample_normal(terrain_level1_width_mu,
     &              terrain_level1_width_sigma,deltaY)
            endif               ! terrain_level1_width_dist
         else if (ilevel.eq.2) then
            if (terrain_level2_length_dist.eq.1) then
               deltaX=terrain_level2_length_L
            else if (terrain_level2_length_dist.eq.2) then
               call uniform(terrain_level2_length_Lmin,
     &              terrain_level2_length_Lmax,
     &              deltaX)
            else if (terrain_level2_length_dist.eq.3) then               
               call sample_normal(terrain_level2_length_mu,
     &              terrain_level2_length_sigma,deltaX)
            endif               ! terrain_level2_length_dist
            if (terrain_level2_width_dist.eq.1) then
               deltaY=terrain_level2_width_L
            else if (terrain_level2_width_dist.eq.2) then
               call uniform(terrain_level2_width_Lmin,
     &              terrain_level2_width_Lmax,
     &              deltaY)
            else if (terrain_level2_width_dist.eq.3) then               
               call sample_normal(terrain_level2_width_mu,
     &              terrain_level2_width_sigma,deltaY)
            endif               ! terrain_level2_width_dist
         else
            call error(label)
            write(*,*) 'ilevel=',ilevel
            stop
         endif
         if (success) then
c     Debug
            if (debug) then
               write(*,*) 'ctr_seed:'
               write(*,*) 'Np_seed=',Np_seed
               do i=1,Np
                  write(*,*) (ctr_seed(i,j),j=1,dim-1)
               enddo            ! i
            endif
c     Debug
            call growth(dim,debug,mapXsize,mapYsize,
     &           Ncontour_sm,Nppc_sm,contour_sm,
     &           x0_seed,Np_seed,ctr_seed,
     &           deltaX,deltaY,ifootprint,
     &           P1,P2,P3,P4,
     &           u12,u23,u34,u41,u21,u32,u43,u14,
     &           Np,ctr,
     &           on_left_side,on_right_side,
     &           on_bottom_side,on_top_side)
c     Adding ctr to contour_global
            call add_ctr_to_contour(dim,
     &           Ncontour,Nppc,contour,
     &           Np,ctr)
c     Generate surrounding contour and add it to contour_sm
c            call generate_close_contour(dim,Np,ctr,
c     &           small_street_width,.false.,
c     &           Np_tmp,ctr_tmp)
            call generate_close_contour2(dim,.false.,Np,ctr,
     &           0.0D+0,small_street_width,.false.,.true.,success,
     &           Np_tmp,ctr_tmp)
            if (.not.success) then
               call error(label)
               write(*,*) 'could not generate outward contour'
               write(*,*) 'for the footprint at distance:'
               write(*,*) 'small_street_width=',small_street_width
               write(*,*) 'ilevel=',ilevel
               write(*,*) 'isector=',isector
               write(*,*) 'ifootprint=',ifootprint
               stop
            endif               ! success=F
            call add_ctr_to_contour(dim,Ncontour_sm,Nppc_sm,contour_sm,
     &           Np_tmp,ctr_tmp)
         else
            keep_generating_footprints=.false.
         endif                  ! success
      enddo                     ! while (keep_generating_footprints)

c     replace first contour in "contour" by ctr_base
      Nppc(1)=Np_base
      do i=1,Np_base
         do j=1,dim-1
            contour(1,i,j)=ctr_base(i,j)
         enddo                  ! j
      enddo                     ! i
c     reshape contours that do not fit in ctr0
      if ((ilevel.eq.1).and.(isector.eq.0)) then
         debug=.true.
      else
         debug=.false.
      endif
      call reshape_contours(dim,debug,mapXsize,mapYsize,
     &     Ncontour,Nppc,contour,
     &     P1,P2,P3,P4,
     &     u12,u23,u34,u41,u21,u32,u43,u14,
     &     on_left_side,on_right_side,on_bottom_side,on_top_side)

c     set contour types
      call identify_contour_type(dim,
     &     mapXsize,mapYsize,
     &     Ncontour,Nppc,contour,
     &     on_left_side,on_right_side,on_bottom_side,on_top_side,
     &     contour_type)
      
c     generate gnuplot contour files (of contour in local referential)
      if (generate_sector_plot) then
         filename='./gnuplot/gra_sector'
         call record_sector_contour(filename,dim,
     &        Ncontour,Nppc,contour)
      endif                     ! generate_sector_plot

c     revert back to the global referential
      call local2global(dim,xcenter,
     &     Ncontour,Nppc,contour,
     &     Ncontour_global,Nppc_global,contour_global,
     &     M01,M10)

      return
      end



      subroutine growth(dim,debug,mapXsize,mapYsize,
     &     Ncontour_sm,Nppc_sm,contour_sm,
     &     x0_seed,Np_seed,ctr_seed,
     &     deltaX,deltaY,ifootprint,
     &     P1,P2,P3,P4,
     &     u12,u23,u34,u41,u21,u32,u43,u14,
     &     Np,ctr,
     &     on_left_side,on_right_side,
     &     on_bottom_side,on_top_side)
      implicit none
      include 'max.inc'
c     
c     Purpose: to grow a contour from a seed
c
c     Input:
c       + dim: dimension of space
c       + debug: true for debug mode
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Ncontour_sm: number of contours in the shadow mask
c       + Nppc_sm: numher of points for each contour in the shadow mask
c       + contour_sm: shadow mask contours
c       + x0_seed: central position of seeding contour
c       + Np_seed: number of points for seeding contour
c       + ctr_seed: seeding contour
c       + deltaX: maximum possible extension in X direction(s)
c       + deltaY: maximum possible extension in Y direction(s)
c       + ifootprint: index of the growing footprint
c       + P1,P2,P3,P4: positions of the 4 corners of contour index 1 of "contour_sm"
c       + u12,u23,u34,u41,u21,u32,u43,u14: propagation directions along the borders
c     
c     Output:
c       + Np: number of points for the generated contour
c       + ctr: generated contour
c       + on_left_side: true if the footprint sticks to the left side of contour 1
c       + on_right_side: true if the footprint sticks to the right side of contour 1
c       + on_bottom_side: true if the footprint sticks to the bottom side of contour 1
c       + on_top_side: true if the footprint sticks to the top side of contour 1
c     
c     I/O
      integer dim
      logical debug
      double precision mapXsize,mapYsize
      integer Ncontour_sm
      integer Nppc_sm(1:Ncontour_mx)
      double precision contour_sm(1:Ncontour_mx,
     &     1:Nppc_mx,1:Ndim_mx-1)
      double precision x0_seed(1:Ndim_mx)
      integer Np_seed
      double precision ctr_seed(1:Nppc_mx,1:Ndim_mx-1)
      double precision deltaX
      double precision deltaY
      integer ifootprint
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision P3(1:Ndim_mx)
      double precision P4(1:Ndim_mx)
      double precision u12(1:Ndim_mx)
      double precision u23(1:Ndim_mx)
      double precision u34(1:Ndim_mx)
      double precision u41(1:Ndim_mx)
      double precision u21(1:Ndim_mx)
      double precision u32(1:Ndim_mx)
      double precision u43(1:Ndim_mx)
      double precision u14(1:Ndim_mx)
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      logical on_left_side(1:Ncontour_mx)
      logical on_right_side(1:Ncontour_mx)
      logical on_bottom_side(1:Ncontour_mx)
      logical on_top_side(1:Ncontour_mx)
c     temp
      integer i,j
      double precision d
      integer Np1
      double precision ctr1(1:Nppc_mx,1:Ndim_mx-1)
      integer Np2
      double precision ctr2(1:Nppc_mx,1:Ndim_mx-1)
      integer Np3
      double precision ctr3(1:Nppc_mx,1:Ndim_mx-1)
      integer Np_tmp
      double precision ctr_tmp(1:Nppc_mx,1:Ndim_mx-1)
      integer direction
      double precision u1(1:Ndim_mx)
      double precision u2(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision x1x2(1:Ndim_mx)
      double precision n12(1:Ndim_mx)
      double precision x(1:Ndim_mx)
      double precision tmp(1:Ndim_mx),norm
      integer idx(1:2)
      double precision dir(1:2,1:Ndim_mx)
      double precision df(1:2)
      double precision main(1:Ndim_mx)
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,
     &     1:Nppc_mx,1:Ndim_mx-1)
      logical consistent
      logical intersect
      integer Nintersected
      integer intersected_ic(1:Ncontour_mx)
      logical is_insidej
      integer Nc1
      integer idx1(1:Ncontour_mx)
      logical is_insidei
      integer Nc2
      integer idx2(1:Ncontour_mx)
      logical intersection_found
      logical intersection1_found
      logical intersection2_found
      logical intersection3_found
      logical intersection4_found
      double precision Pint(1:Ndim_mx)
      double precision xint(1:Ndim_mx)
      double precision d2int
      double precision xmid(1:Ndim_mx),length
      integer icontour
      integer isegment
      integer Np_max
      double precision ctr_max(1:Nppc_mx,1:Ndim_mx-1)
      double precision dmin,dmax
      logical keep_looking,solution_found
      integer Niter
      double precision d2x1x2
      double precision A(1:Ndim_mx)
      double precision Axint(1:Ndim_mx),sp
      logical A_is_at_end
      integer end_side
      double precision ux(1:Ndim_mx)
      double precision uy(1:Ndim_mx)
      double precision mux(1:Ndim_mx)
      double precision muy(1:Ndim_mx)
c     parameters
      double precision epsilon_d
      parameter(epsilon_d=1.0D-2)
c     label
      character*(Nchar_mx) label
      label='subroutine growth'

c     Debug
      if (debug) then
         write(*,*) 'In: ',trim(label)
         write(*,*) 'xmin=',contour_sm(1,1,1)
         write(*,*) 'ymin=',contour_sm(1,1,2)
         write(*,*) 'xmax=',contour_sm(1,3,1)
         write(*,*) 'ymax=',contour_sm(1,3,2)
         do icontour=1,Ncontour_sm
            write(*,*) 'Nppc_sm(',icontour,')=',Nppc_sm(icontour)
            do i=1,Nppc_sm(icontour)
               write(*,*) (contour_sm(icontour,i,j),j=1,dim-1)
            enddo               ! i
         enddo                  ! icontour
      endif
c     Debug
      
      ux(1)=1.0D+0
      ux(2)=0.0D+0
      ux(3)=0.0D+0
      uy(1)=0.0D+0
      uy(2)=1.0D+0
      uy(3)=0.0D+0
      call scalar_vector(dim,-1.0D+0,ux,mux)
      call scalar_vector(dim,-1.0D+0,uy,muy)
c     
      on_left_side(ifootprint)=.false.
      on_right_side(ifootprint)=.false.
      on_bottom_side(ifootprint)=.false.
      on_top_side(ifootprint)=.false.
c     "ctr1" is the current valid contour
c     "ctr2" is the temporary growing contour, used to test consistency
      call duplicate_ctr(dim,Np_seed,ctr_seed,Np1,ctr1)
c     dummy "contour"
      Ncontour=0
      do icontour=1,Ncontour_sm
         Nppc(icontour)=Nppc_sm(icontour)
         call get_contour(dim,Ncontour_sm,Nppc_sm,contour_sm,icontour,
     &        Np_tmp,ctr_tmp)
         call add_ctr_to_contour(dim,Ncontour,Nppc,contour,
     &        Np_tmp,ctr_tmp)
      enddo                     ! icontour

c     Trying to grow in the specified direction
      do direction=1,4
c     Debug
         if (debug) then
            write(*,*) '===================================='
            write(*,*) 'direction=',direction
            write(*,*) '===================================='
         endif
c     Debug
         call duplicate_ctr(dim,Np1,ctr1,Np2,ctr2)
         call moving_points(dim,ifootprint,
     &        on_left_side,on_right_side,
     &        on_bottom_side,on_top_side,
     &        direction,
     &        u12,u23,u34,u41,u21,u32,u43,u14,
     &        idx,dir,df,main)
         call copy_vector(dim,main,u)
         if ((direction.eq.1).or.(direction.eq.2)) then
            d=deltaX
         else if ((direction.eq.3).or.(direction.eq.4)) then
            d=deltaY
         endif
c     Debug
         if (debug) then
            write(*,*) 'd=',d,' u=',u
            write(*,*) 'idx=',(idx(i),i=1,2)
         endif
c     Debug

c     Initial ctr_max
         call duplicate_ctr(dim,Np1,ctr1,Np_max,ctr_max)
c     Debug
         if (debug) then
            write(*,*) 'Building ctr_max from: Np_max=',Np_max
            do i=1,Np_max
               write(*,*) (ctr_max(i,j),j=1,dim-1)
            enddo               ! i
         endif
c     Debug
         call grow_contour(dim,Np_max,ctr_max,idx,u,d,
     &        Np_max,ctr_max)
c     Debug
         if (debug) then
            write(*,*) 'Initial ctr_max: Np_max=',Np_max
            do i=1,Np_max
               write(*,*) (ctr_max(i,j),j=1,dim-1)
            enddo               ! i
         endif
c     Debug
c     test contour consistency
         Ncontour=Ncontour_sm
         call add_ctr_to_contour(dim,Ncontour,Nppc,contour,
     &        Np_max,ctr_max)
         call identify_inconsistent_contour(dim,
     &        mapXsize,mapYsize,
     &        Ncontour,Nppc,contour,Ncontour,
     &        consistent,
     &        intersect,Nintersected,intersected_ic,
     &        is_insidej,Nc1,idx1,
     &        is_insidei,Nc2,idx2)
         if (consistent) then
c     Debug
            if (debug) then
               write(*,*) 'Initial ctr_max is consistent'
            endif
c     Debug
c     If "ctr_max" is consistent with current shadow mask, skip to next direction
            call duplicate_ctr(dim,Np_max,ctr_max,Np1,ctr1)
            goto 111
         else
c     Debug
            if (debug) then
               write(*,*) 'Initial ctr_max is NOT consistent'
               write(*,*) 'intersect=',intersect
               if (intersect) then
                  write(*,*) 'Nintersected=',Nintersected
                  do i=1,Nintersected
                     write(*,*) 'intersected_ic(',i,')=',
     &                    intersected_ic(i)
                  enddo         ! i
               endif
               write(*,*) 'is_insidej=',is_insidej
               write(*,*) 'is_insidei=',is_insidei
            endif
c     Debug
c     Use dichotomy to find best-fitting "ctr2"
            call duplicate_ctr(dim,Np1,ctr1,Np2,ctr2)
            dmin=0.0D+0
            dmax=d
c     Debug
            if (debug) then
               write(*,*) 'Starting dichotomy procedure'
               write(*,*) 'dmin=',dmin,' dmax=',dmax
            endif
c     Debug
            solution_found=.false.
            Niter=0
            keep_looking=.true.
            do while (keep_looking)
               Niter=Niter+1
c     Debug
               if (debug) then
                  write(*,*) '----------------------------------'
                  write(*,*) 'Niter=',Niter
                  write(*,*) '----------------------------------'
               endif
c     Debug
               d=(dmin+dmax)/2.0D+0
c     Debug
               if (debug) then
                  write(*,*) 'd=',d
               endif
c     Debug
c     grow the contour
               call grow_contour(dim,Np1,ctr1,idx,u,d,
     &              Np2,ctr2)
c     Debug
               if (debug) then
                  write(*,*) 'Current ctr2: Np2=',Np2
                  do i=1,Np2
                     write(*,*) (ctr2(i,j),j=1,dim-1)
                  enddo         ! i
               endif
c     Debug
c     test contour consistency
               Ncontour=Ncontour_sm
               call add_ctr_to_contour(dim,Ncontour,Nppc,contour,
     &              Np2,ctr2)
               call identify_inconsistent_contour(dim,
     &              mapXsize,mapYsize,
     &              Ncontour,Nppc,contour,Ncontour,
     &              consistent,
     &              intersect,Nintersected,intersected_ic,
     &              is_insidej,Nc1,idx1,
     &              is_insidei,Nc2,idx2)
c     Debug
               if (debug) then
                  write(*,*) 'consistent=',consistent
               endif
c     Debug
               if (consistent) then
c     "ctr2" is consistent
                  dmin=d
                  if (dmax-dmin.le.epsilon_d) then
                     solution_found=.true.
                     keep_looking=.false.
                  endif
c     Debug
                  if (debug) then
                     write(*,*) 'dmin=',dmin
                     write(*,*) 'dmax-dmin=',dmax-dmin,
     &                    ' epsilon_d=',epsilon_d
                  endif
c     Debug
               else             ! consistent=F
                  dmax=d
c     Debug
                  if (debug) then
                     write(*,*) 'dmax=',dmax
                  endif
c     Debug
               endif            ! consistent
               if (Niter.gt.Niter_mx) then
                  keep_looking=.false.
c     Debug
                  if (debug) then
                     write(*,*) 'Failing because Niter=',Niter,
     &                    ' > Niter_mx=',Niter_mx
                  endif
c     Debug
               endif
            enddo               ! while (keep_looking)
            if (.not.solution_found) then
               call error(label)
               write(*,*) 'Dichotomy found no solution'
               write(*,*) 'dmin=',dmin
               write(*,*) 'dmax=',dmax
               write(*,*) 'dmax-dmin=',dmax-dmin
               stop
            else                ! solution_found=F
c     Debug
               if (debug) then
                  write(*,*) 'Dichotomy found a solution'
               endif
c     Debug
               call duplicate_ctr(dim,Np2,ctr2,Np1,ctr1)
            endif               ! solution_found
         endif                  ! "ctr_max" is consistent
 111     continue
c     Debug
         if (debug) then
            write(*,*) 'Now ctr1:'
            write(*,*) 'Np1=',Np1
            do i=1,Np
               write(*,*) (ctr1(i,j),j=1,dim-1)
            enddo               ! i
         endif
c     Debug
      enddo                     ! direction
c      
      call duplicate_ctr(dim,Np1,ctr1,Np,ctr)
c
c     Debug
      if (debug) then
         write(*,*) 'Out: ',trim(label)
      endif
c     Debug
      return
      end



      subroutine grow_contour(dim,Np_in,ctr_in,idx,u,d,
     &     Np_out,ctr_out)
      implicit none
      include 'max.inc'
c     
c     Purpose: to grow a given contour in a given direction
c     
c     Input:
c       + dim: dimension of space
c       + Np_in: number of points that define the contour
c       + ctr_in: contour to grow
c       + idx: indexes of the points to move
c       + u: propagation direction
c       + d: required growth distance
c     
c     Output:
c       + Np_out: number of points that define the grown contour
c       + ctr_out: grown contour
c     
c     I/O
      integer dim
      integer Np_in
      double precision ctr_in(1:Nppc_mx,1:Ndim_mx-1)
      integer idx(1:2)
      double precision u(1:Ndim_mx)
      double precision d
      integer Np_out
      double precision ctr_out(1:Nppc_mx,1:Ndim_mx-1)
c     temp
      integer i
      double precision x(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine grow_contour'

      call duplicate_ctr(dim,Np_in,ctr_in,Np_out,ctr_out)
      do i=1,2
         call scalar_vector(dim,d,u,tmp)
         call get_position_from_ctr(dim,Np_in,ctr_in,
     &        idx(i),x)
         call add_vectors(dim,x,tmp,x)
         call set_position_in_ctr(dim,Np_out,ctr_out,
     &        idx(i),x)
      enddo                     ! i

      return
      end

      

      subroutine seeding(dim,debug,mapXsize,mapYsize,
     &     Ncontour_sm,Nppc_sm,contour_sm,
     &     Np0,ctr0,
     &     Niter_max,success,x0,Np,ctr)
      implicit none
      include 'max.inc'
c     
c     Purpose: to generate a position in the free space
c
c     Input:
c       + dim: dimension of space
c       + debug: true for debug mode
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Ncontour_sm: number of contours in the shadow mask
c       + Nppc_sm: numher of points for each contour in the shadow mask
c       + contour_sm: shadow mask contours
c       + Np0: number of points for "ctr0"
c       + ctr0: contour for the sector
c       + Niter_max: maximum number of iterations
c     
c     Output:
c       + success: true of a position was generated
c       + x0: central position
c       + Np: number of points for the generated contour
c       + ctr: generated contour
c     
c     I/O
      integer dim
      logical debug
      double precision mapXsize,mapYsize
      integer Ncontour_sm
      integer Nppc_sm(1:Ncontour_mx)
      double precision contour_sm(1:Ncontour_mx,
     &     1:Nppc_mx,1:Ndim_mx-1)
      integer Np0
      double precision ctr0(1:Nppc_mx,1:Ndim_mx-1)
      integer Niter_max
      logical success
      double precision x0(1:Ndim_mx)
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
c     temp
      integer Ncontour_sector
      integer Nppc_sector(1:Ncontour_mx)
      double precision contour_sector(1:Ncontour_mx,
     &     1:Nppc_mx,1:Ndim_mx-1)
      integer Niter,iter,i,icontour,j
      logical keep_trying
      integer Np_tmp
      double precision ctr_tmp(1:Nppc_mx,1:Ndim_mx-1)
      double precision xmin_sm1,xmax_sm1,ymin_sm1,ymax_sm1
      double precision x(1:Ndim_mx)
      logical inside,inside2
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      logical consistent
      logical intersect
      integer Nintersected
      integer intersected_ic(1:Ncontour_mx)
      logical is_insidej
      integer Nc1
      integer idx1(1:Ncontour_mx)
      logical is_insidei
      integer Nc2
      integer idx2(1:Ncontour_mx)
c     parameters
      double precision epsilonx,epsilony
      parameter(epsilonx=1.0D-2)
      parameter(epsilony=1.0D-2)
c     label
      character*(Nchar_mx) label
      label='subroutine seeding'

c     Debug
      if (debug) then
         write(*,*) 'In: ',trim(label)
      endif
c     Debug

      xmin_sm1=contour_sm(1,1,1)
      ymin_sm1=contour_sm(1,1,2)
      xmax_sm1=contour_sm(1,3,1)
      ymax_sm1=contour_sm(1,3,2)
c     create dummy contour
      Ncontour=0
      do icontour=1,Ncontour_sm
         if (icontour.eq.1) then
            call add_ctr_to_contour(dim,Ncontour,Nppc,contour,
     &           Np0,ctr0)
         else
            call get_contour(dim,
     &           Ncontour_sm,Nppc_sm,contour_sm,icontour,
     &           Np_tmp,ctr_tmp)
            call add_ctr_to_contour(dim,Ncontour,Nppc,contour,
     &           Np_tmp,ctr_tmp)
         endif                  ! icontour
      enddo                     ! icontour
c     
      Np=4
      Niter=0
      keep_trying=.true.
      do while (keep_trying)
         Niter=Niter+1
c     Debug
         if (debug) then
            write(*,*) 'Niter=',Niter
         endif
c     Debug
         call uniform(xmin_sm1,xmax_sm1,x(1))
         call uniform(ymin_sm1,ymax_sm1,x(2))
         x(3)=0.0D+0
c     Debug
         if (debug) then
            write(*,*) 'x=',x
            write(*,*) 'using is_inside_contour on:'
            write(*,*) 'Ncontour=',Ncontour
            do icontour=1,Ncontour
               write(*,*) 'Nppc(',icontour,')=',Nppc(icontour)
               do i=1,Nppc(icontour)
                  write(*,*) (contour(icontour,i,j),j=1,dim-1)
               enddo            ! i
            enddo               ! icontour
         endif
c     Debug
         call is_inside_contour(.false.,dim,
     &        Ncontour,Nppc,contour,
     &        1,x,inside)
c     Debug
         if (debug) then
            write(*,*) 'Inside contour 1:',inside
         endif
c     Debug
         if ((inside).and.(Ncontour.ge.2)) then
            do icontour=2,Ncontour
c     Debug
               if (debug) then
                  write(*,*) 'Testing inside contour:',icontour
               endif
c     Debug
               call is_inside_contour(.false.,dim,
     &              Ncontour,Nppc,contour,
     &              icontour,x,inside2)
c     Debug
               if (debug) then
                  write(*,*) 'inside2=',inside2
               endif
c     Debug
               if (inside2) then
                  inside=.false.
                  goto 111
               endif            ! inside 2
            enddo               ! icontour
         endif                  ! inside AND Ncontour>1
 111     continue
c     Debug
         if (debug) then
            write(*,*) 'inside=',inside
         endif
c     Debug
         if (inside) then
            call copy_vector(dim,x,x0)
c     generate seed contour
            ctr(1,1)=x0(1)-epsilonx
            ctr(1,2)=x0(2)-epsilony
            ctr(2,1)=x0(1)+epsilonx
            ctr(2,2)=x0(2)-epsilony
            ctr(3,1)=x0(1)+epsilonx
            ctr(3,2)=x0(2)+epsilony
            ctr(4,1)=x0(1)-epsilonx
            ctr(4,2)=x0(2)+epsilony
            Ncontour=Ncontour_sm
            call add_ctr_to_contour(dim,Ncontour,Nppc,contour,
     &           Np,ctr)
c     test contour consistency
            call identify_inconsistent_contour(dim,
     &           mapXsize,mapYsize,
     &           Ncontour,Nppc,contour,Ncontour,
     &           consistent,
     &           intersect,Nintersected,intersected_ic,
     &           is_insidej,Nc1,idx1,
     &           is_insidei,Nc2,idx2)
c     Debug
c            write(*,*) 'This is: ',trim(label)
c            write(*,*) 'consistent=',consistent
c            if (consistent) then
c               write(*,*) 'x0=',x0
c               do i=1,Np
c                  write(*,*) 'ctr(',i,')=',(ctr(i,j),j=1,dim-1)
c               enddo            ! i
c            endif
c            if (.not.consistent) then
c               write(*,*) 'intersect=',intersect
c               if (intersect) then
c                  write(*,*) 'Nintersected=',Nintersected
c                  do i=1,Nintersected
c                     write(*,*) 'intersected_ic(',i,')=',
c     &                    intersected_ic(i)
c                  enddo         ! i
c               endif
c               write(*,*) 'pic=',pic
c            endif
c     Debug
            if (consistent) then
               keep_trying=.false.
               success=.true.
            endif               ! consistent
         endif                  ! inside
c     
         if (keep_trying) then
            if (Niter.gt.Niter_max) then
               keep_trying=.false.
               success=.false.
            endif               ! Niter > Niter_max
         endif                  ! keep_trying
c     
      enddo                     ! while (keep_trying)
      
c     Debug
      if (debug) then
         write(*,*) 'Out: ',trim(label)
c         stop
      endif
c     Debug

      return
      end


      
      subroutine moving_points(dim,ifootprint,
     &     on_left_side,on_right_side,
     &     on_bottom_side,on_top_side,
     &     direction,
     &     u12,u23,u34,u41,u21,u32,u43,u14,
     &     idx,dir,df,main)
      implicit none
      include 'max.inc'
c     
c     Purpose: to provide the indexes of the 2 points that
c     should move for the provided growth direction
c     
c     Input:
c       + dim: dimension of space
c       + ifootprint: index of the growing footprint
c       + on_left_side: true if the footprint sticks to the left side of contour 1
c       + on_right_side: true if the footprint sticks to the right side of contour 1
c       + on_bottom_side: true if the footprint sticks to the bottom side of contour 1
c       + on_top_side: true if the footprint sticks to the top side of contour 1
c       + direction: growth direction
c       + u12,u23,u34,u41,u21,u32,u43,u14: propagation directions along the borders
c     
c     Output:
c       + idx: index of the first and second point that should move;
c         a null value indicates movement is forbidden for that variety, in that direction
c       + dir: direction of propagation, for each position
c       + df: multiplication factor to apply over the distance, for each point,
c         due to the fact that the direction of propagation is not aligned with the axis
c       + main: main propagation direction for the value of "direction"
c     
c     I/O
      integer dim
      integer ifootprint
      logical on_left_side(1:Ncontour_mx)
      logical on_right_side(1:Ncontour_mx)
      logical on_bottom_side(1:Ncontour_mx)
      logical on_top_side(1:Ncontour_mx)
      integer direction
      double precision u12(1:Ndim_mx)
      double precision u23(1:Ndim_mx)
      double precision u34(1:Ndim_mx)
      double precision u41(1:Ndim_mx)
      double precision u21(1:Ndim_mx)
      double precision u32(1:Ndim_mx)
      double precision u43(1:Ndim_mx)
      double precision u14(1:Ndim_mx)
      integer idx(1:2)
      double precision dir(1:2,1:Ndim_mx)
      double precision df(1:2)
      double precision main(1:Ndim_mx)
c     temp
      integer variety,i1,i2,j
      double precision u1(1:Ndim_mx)
      double precision u2(1:Ndim_mx)
      double precision ux(1:Ndim_mx)
      double precision uy(1:Ndim_mx)
      double precision mux(1:Ndim_mx)
      double precision muy(1:Ndim_mx)
      double precision sp
c     label
      character*(Nchar_mx) label
      label='subroutine moving_points'

      ux(1)=1.0D+0
      ux(2)=0.0D+0
      ux(3)=0.0D+0
      uy(1)=0.0D+0
      uy(2)=1.0D+0
      uy(3)=0.0D+0
      call scalar_vector(dim,-1.0D+0,ux,mux)
      call scalar_vector(dim,-1.0D+0,uy,muy)
      
      call identify_contour_variety(dim,
     &     ifootprint,
     &     on_left_side,on_right_side,on_bottom_side,on_top_side,
     &     variety)
      if (variety.eq.1) then
         if (direction.eq.1) then
            i1=1
            i2=4
            call copy_vector(dim,mux,u1)
            call copy_vector(dim,mux,u2)
         else if (direction.eq.2) then
            i1=2
            i2=3
            call copy_vector(dim,ux,u1)
            call copy_vector(dim,ux,u2)
         else if (direction.eq.3) then
            i1=1
            i2=2
            call copy_vector(dim,muy,u1)
            call copy_vector(dim,muy,u2)
         else if (direction.eq.4) then
            i1=3
            i2=4
            call copy_vector(dim,uy,u1)
            call copy_vector(dim,uy,u2)
         endif ! direction
      else  if (variety.eq.2) then
         if (direction.eq.1) then
            i1=1
            i2=4
            call copy_vector(dim,mux,u1)
            call copy_vector(dim,u23,u2)
         else if (direction.eq.2) then
            i1=2
            i2=3
            call copy_vector(dim,ux,u1)
            call copy_vector(dim,u32,u2)
         else if (direction.eq.3) then
            i1=1
            i2=2
            call copy_vector(dim,muy,u1)
            call copy_vector(dim,muy,u2)
         else if (direction.eq.4) then
            i1=0
            i2=0
         endif ! direction
      else if (variety.eq.3) then
         if (direction.eq.1) then
            i1=1
            i2=4
            call copy_vector(dim,u21,u1)
            call copy_vector(dim,mux,u2)
         else if (direction.eq.2) then
            i1=2
            i2=3
            call copy_vector(dim,u12,u1)
            call copy_vector(dim,mux,u2)
         else if (direction.eq.3) then
            i1=0
            i2=0
         else if (direction.eq.4) then
            i1=3
            i2=4
            call copy_vector(dim,uy,u1)
            call copy_vector(dim,uy,u2)
         endif ! direction
      else if (variety.eq.4) then
         if (direction.eq.1) then
            i1=1
            i2=4
            call copy_vector(dim,u14,u1)
            call copy_vector(dim,u23,u2)
         else if (direction.eq.2) then
            i1=2
            i2=3
            call copy_vector(dim,u41,u1)
            call copy_vector(dim,u32,u2)
         else if (direction.eq.3) then
            i1=0
            i2=0
         else if (direction.eq.4) then
            i1=0
            i2=0
         endif ! direction
      else if (variety.eq.5) then
         if (direction.eq.1) then
            i1=1
            i2=4
            call copy_vector(dim,mux,u1)
            call copy_vector(dim,mux,u2)
         else if (direction.eq.2) then
            i1=0
            i2=0
         else if (direction.eq.3) then
            i1=1
            i2=2
            call copy_vector(dim,muy,u1)
            call copy_vector(dim,u21,u2)
         else if (direction.eq.4) then
            i1=3
            i2=4
            call copy_vector(dim,u12,u1)
            call copy_vector(dim,uy,u2)
         endif ! direction
      else if (variety.eq.6) then
         if (direction.eq.1) then
            i1=1
            i2=4
            call copy_vector(dim,mux,u1)
            call copy_vector(dim,u23,u2)
         else if (direction.eq.2) then
            i1=0
            i2=0
         else if (direction.eq.3) then
            i1=1
            i2=2
            call copy_vector(dim,muy,u1)
            call copy_vector(dim,u21,u2)
         else if (direction.eq.4) then
            i1=0
            i2=0
         endif ! direction
      else if (variety.eq.7) then
         if (direction.eq.1) then
            i1=1
            i2=4
            call copy_vector(dim,u14,u1)
            call copy_vector(dim,mux,u2)
         else if (direction.eq.2) then
            i1=0
            i2=0
         else if (direction.eq.3) then
            i1=0
            i2=0
         else if (direction.eq.4) then
            i1=3
            i2=4
            call copy_vector(dim,u12,u1)
            call copy_vector(dim,uy,u2)
         endif ! direction
      else if (variety.eq.8) then
         if (direction.eq.1) then
            i1=1
            i2=4
            call copy_vector(dim,u14,u1)
            call copy_vector(dim,u23,u2)
         else if (direction.eq.2) then
            i1=0
            i2=0
         else if (direction.eq.3) then
            i1=0
            i2=0
         else if (direction.eq.4) then
            i1=0
            i2=0
         endif ! direction
      else if (variety.eq.9) then
         if (direction.eq.1) then
            i1=0
            i2=0
         else if (direction.eq.2) then
            i1=2
            i2=3
            call copy_vector(dim,ux,u1)
            call copy_vector(dim,ux,u2)
         else if (direction.eq.3) then
            i1=1
            i2=2
            call copy_vector(dim,u34,u1)
            call copy_vector(dim,muy,u2)
         else if (direction.eq.4) then
            i1=3
            i2=4
            call copy_vector(dim,uy,u1)
            call copy_vector(dim,u41,u2)
         endif ! direction
      else if (variety.eq.10) then
         if (direction.eq.1) then
            i1=0
            i2=0
         else if (direction.eq.2) then
            i1=2
            i2=3
            call copy_vector(dim,ux,u1)
            call copy_vector(dim,u32,u2)
         else if (direction.eq.3) then
            i1=1
            i2=2
            call copy_vector(dim,u34,u1)
            call copy_vector(dim,muy,u2)
         else if (direction.eq.4) then
            i1=0
            i2=0
         endif ! direction
      else if (variety.eq.11) then
         if (direction.eq.1) then
            i1=0
            i2=0
         else if (direction.eq.2) then
            i1=2
            i2=3
            call copy_vector(dim,u41,u1)
            call copy_vector(dim,ux,u2)
         else if (direction.eq.3) then
            i1=0
            i2=0
         else if (direction.eq.4) then
            i1=3
            i2=4
            call copy_vector(dim,uy,u1)
            call copy_vector(dim,u43,u2)
         endif ! direction
      else if (variety.eq.12) then
         if (direction.eq.1) then
            i1=0
            i2=0
         else if (direction.eq.2) then
            i1=2
            i2=3
            call copy_vector(dim,u41,u1)
            call copy_vector(dim,u32,u2)
         else if (direction.eq.3) then
            i1=0
            i2=0
         else if (direction.eq.4) then
            i1=0
            i2=0
         endif ! direction
      else if (variety.eq.13) then
         if (direction.eq.1) then
            i1=0
            i2=0
         else if (direction.eq.2) then
            i1=0
            i2=0
         else if (direction.eq.3) then
            i1=1
            i2=2
            call copy_vector(dim,u34,u1)
            call copy_vector(dim,u21,u2)
         else if (direction.eq.4) then
            i1=3
            i2=4
            call copy_vector(dim,u12,u1)
            call copy_vector(dim,u43,u2)
         endif ! direction
      else if (variety.eq.14) then
         if (direction.eq.1) then
            i1=0
            i2=0
         else if (direction.eq.2) then
            i1=0
            i2=0
         else if (direction.eq.3) then
            i1=1
            i2=2
            call copy_vector(dim,u34,u1)
            call copy_vector(dim,u21,u2)
         else if (direction.eq.4) then
            i1=0
            i2=0
         endif ! direction
      else if (variety.eq.15) then
         if (direction.eq.1) then
            i1=0
            i2=0
         else if (direction.eq.2) then
            i1=0
            i2=0
         else if (direction.eq.3) then
            i1=0
            i2=0
         else if (direction.eq.4) then
            i1=3
            i2=4
            call copy_vector(dim,u12,u1)
            call copy_vector(dim,u43,u2)
         endif ! direction
      else if (variety.eq.16) then
         if (direction.eq.1) then
            i1=0
            i2=0
         else if (direction.eq.2) then
            i1=0
            i2=0
         else if (direction.eq.3) then
            i1=0
            i2=0
         else if (direction.eq.4) then
            i1=0
            i2=0
         endif                  ! direction
      endif
      idx(1)=i1
      idx(2)=i2
      do j=1,dim
         dir(1,j)=u1(j)
         dir(2,j)=u2(j)
      enddo                     ! j
      if (direction.eq.1) then
         call copy_vector(dim,mux,main)
      else if (direction.eq.2) then
         call copy_vector(dim,ux,main)
      else if (direction.eq.3) then
         call copy_vector(dim,muy,main)
      else if (direction.eq.4) then
         call copy_vector(dim,uy,main)
      endif                     ! direction
      call scalar_product(dim,u1,main,sp)
      if (sp.eq.0.0D+0) then
         call error(label)
         write(*,*) 'sp=',sp
         write(*,*) 'u1=',u1
         write(*,*) 'main=',main
         stop
      else
         df(1)=1.0D+0/dabs(sp)
      endif                     ! sp=0
      call scalar_product(dim,u2,main,sp)
      if (sp.eq.0.0D+0) then
         call error(label)
         write(*,*) 'sp=',sp
         write(*,*) 'u2=',u2
         write(*,*) 'main=',main
         stop
      else
         df(2)=1.0D+0/dabs(sp)
      endif                     ! sp=0

      return
      end
