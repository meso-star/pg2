c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine invert_3x3_matrix(dim,M,inv_M)
      implicit none
      include 'max.inc'
c
c     Purpose: to invert a 3x3 matrix
c     
c     Input:
c       + dim: dimension of the matrix
c       + M: matrix to invert
c
c     Output:
c       + inv_M: inverse of M
c
c     I/O
      integer dim
      double precision M(1:Ndim_mx,1:Ndim_mx)
      double precision inv_M(1:Ndim_mx,1:Ndim_mx)
c     temp
      double precision M_cof(1:Ndim_mx,1:Ndim_mx)
      double precision M_tr(1:Ndim_mx,1:Ndim_mx)
      double precision det
c     Debug
c      integer i,j
c     Debug
c     label
      character*(Nchar_mx) label
      label='subroutine invert_3x3_matrix'

      if (dim.ne.3) then
         call error(label)
         write(*,*)' dim=',dim
         write(*,*) 'is different from 3'
         stop
      endif

      call det3(dim,M,det)
c     Debug
c      write(*,*) 'determinant=',det
c     Debug
      if (det.eq.0.0D+0) then
         call error(label)
         write(*,*) 'determinant=',det
         stop
      endif

c     Matrix of cofactors
      M_cof(1,1)=(M(2,2)*M(3,3)-M(2,3)*M(3,2))
      M_cof(1,2)=-(M(2,1)*M(3,3)-M(2,3)*M(3,1))
      M_cof(1,3)=(M(2,1)*M(3,2)-M(2,2)*M(3,1))
      M_cof(2,1)=-(M(1,2)*M(3,3)-M(1,3)*M(3,2))
      M_cof(2,2)=(M(1,1)*M(3,3)-M(1,3)*M(3,1))
      M_cof(2,3)=-(M(1,1)*M(3,2)-M(1,2)*M(3,1))
      M_cof(3,1)=(M(1,2)*M(2,3)-M(1,3)*M(2,2))
      M_cof(3,2)=-(M(1,1)*M(2,3)-M(1,3)*M(2,1))
      M_cof(3,3)=(M(1,1)*M(2,2)-M(1,2)*M(2,1))
c     Debug
c      write(*,*) 'M_cof:'
c      do i=1,dim
c         write(*,*) (M_cof(i,j),j=1,dim)
c      enddo ! i
c      write(*,*)
c     Debug
c     Transpose matrix of cofactors
      call transpose_matrix(dim,M_cof,M_tr)
c     Compute inverse matrix
      call scalar_matrix(dim,1.D+0/det,M_tr,inv_M)

      return
      end



      subroutine linear_system(db,n,a,y,valid,x)
      implicit none
      include 'max.inc'
c     
c     Purpose: to solve a Cramer linear system A*X=Y
c
c     Inputs:
c       + n: dimension of the system (must be greater than 1 !)
c       + a: (n*n) square matrix
c       + y: (n) vector of second-terms
c
c     Outputs:
c       + valid: 1 if a valid solution was found; 0 otherwise
c       + x: (n) vector of solutions if a valid solution was found
c

c     inputs
      integer n
      double precision a(1:Ndim_mx,1:Ndim_mx)
      double precision y(1:Ndim_mx)
c     outputs
      integer valid
      double precision x(1:Ndim_mx)
c     temp
      external determinant
      double precision a_tmp(1:Ndim_mx,1:Ndim_mx)
      double precision y_tmp(1:Ndim_mx)
      double precision det,eps
      double precision a11,b,c,sum
      double precision y_ref
      integer i,j,k,l,lif,li,iy
c     Debug
      integer db
c     Debug
c     label
      character*(Nchar_mx) label
      label='subroutine linear_system'

      if ((n.lt.1).or.(n.gt.Ndim_mx)) then
         call error(label)
         write(*,*) 'Bad input parameter:'
         write(*,*) 'n=',n
         if (n.gt.Ndim_mx) then
            write(*,*) 'while Ndim_mx=',Ndim_mx
         endif
         stop
      endif
      valid=1

c     ------------------------------------------------------
c     case of dimension 1 system (1 equation only)
      if (n.eq.1) then
         if (a(1,1).eq.0.0D+0) then
            call error(label)
            write(*,*) 'a*x=y with a=',a(1,1)
            write(*,*) 'no solution !'
            valid=0
            goto 666
         else
            x(1)=y(1)/a(1,1)
            goto 666
         endif
      endif

c     ------------------------------------------------------
c     in the case all solutions y(i) are identical
      iy=1
      y_ref=y(1)
      do i=2,n
         if (y(i).ne.y_ref) then
            iy=0
            goto 123
         endif
      enddo ! i
 123  continue
      if (iy.eq.1) then
         do i=1,n-1
            x(i)=0.0D+0
         enddo
         x(n)=y_ref
         goto 666
      endif

c     ------------------------------------------------------
c     all other cases...

c     copy "a" onto "a_tmp" and "y" onto "y_tmp"(that will be modified)
      call copy_matrix(n,a,a_tmp)
      call copy_vector(n,y,y_tmp)

c     computing determinant
c     --------------------------------
c     modif 2009/04/29
c     
c     if your compiler supports a recursive compilation option (ifort uses the "-recursive" option)
c     then you can use subroutine "determinant"
c     Otherwise, you MUST use subroutine "det3"
c
      call det3(n,a_tmp,det)
c      call determinant(n,a_tmp,det,determinant)
c     modif 2009/04/29
c     --------------------------------
      if (det.lt.1.0D-10) then
         call error(label)
         write(*,*) 'determinant=',det
         write(*,*) 'matrix='
         do i=1,n
            write(*,*) (a(i,j),j=1,n),y(i)
         enddo ! i
         stop
      endif
      
      eps=1.0D-10
c     solving system using the Gauss method
      do i=2,n
 222     continue
         a11=a_tmp(i-1,i-1)
         if (dabs(a11).lt.eps) then
            lif=0
            do l=i,n
               if (a_tmp(i-1,l).ne.0.0D+0) then
                  li=l
                  lif=1
                  goto 111
               endif
            enddo ! l
 111        continue
            if (lif.eq.0) then
               call error(label)
               write(*,*) 'going down'
               write(*,*) 'a(',i-1,',',i-1,')=',a11
               write(*,*) 'and no switching line found'
               valid=0
               goto 666
            else
c     switch lines 'i-1' and 'li'
               call switch_lines(n,a_tmp,y_tmp,i-1,li)
               goto 222
            endif
         endif
         do j=i,n
            b=a_tmp(j,i-1)
            c=b/a11
            do k=i,n
               a_tmp(j,k)=a_tmp(j,k)-a_tmp(i-1,k)*c
            enddo ! k
            y_tmp(j)=y_tmp(j)-y_tmp(i-1)*c
            a_tmp(j,i-1)=0.0D+0
         enddo ! j
      enddo ! i

c     Debug
      if (db.eq.1) then
         write(*,*)
         write(*,*) 'a_tmp='
         do i=1,n
            write(*,*) (a_tmp(i,j),j=1,n),y_tmp(i)
         enddo
      endif
c     Debug

      do i=n,1,-1
         sum=0.0D+0
         if (i+1.le.n) then
            do j=n,i+1,-1
               sum=sum+a_tmp(i,j)*x(j)
            enddo ! j
         endif
         if (a_tmp(i,i).eq.0.0D+0) then
            call error(label)
            write(*,*) 'going up'
            write(*,*) 'a_tmp(',i,',',i,')=',a_tmp(i,i)
            valid=0
            goto 666
         else
            x(i)=(y_tmp(i)-sum)/a_tmp(i,i)
c     Debug
            if (db.eq.1) then
               write(*,*) 'x(',i,')=',x(i)
            endif
c     Debug
         endif
      enddo ! i

 666  continue
      return
      end



      subroutine switch_lines(n,a,y,i,j)
      implicit none
      include 'max.inc'
c
c     Purpose: to switch two lines on linear system parameters
c
c     Inputs:
c       + n: dimension of the system
c       + a: matrix "A" in "A.X=Y"
c       + y: vector "Y" in "A.X=Y"
c       + i: index of the first line
c       + li: index of the second line
c
c     Outputs:
c       + a: (updated) matrix "A" in "A.X=Y"
c       + y: (updated) vector "Y" in "A.X=Y"
c     

c     inputs
      integer n
      double precision a(1:Ndim_mx,1:Ndim_mx)
      double precision y(1:Ndim_mx)
      integer i,j
c     temp
      double precision line_tmp(1:Ndim_mx)
      double precision y_tmp
      integer k
c     label
      character*(Nchar_mx) label
      label='subroutine switch_lines'

      if ((n.lt.1).or.(n.gt.Ndim_mx)) then
         call error(label)
         write(*,*) 'Bad input parameter:'
         write(*,*) 'n=',n
         if (n.gt.Ndim_mx) then
            write(*,*) 'while Ndim_mx=',Ndim_mx
         endif
         stop
      endif

      if ((i.lt.1).or.(i.gt.n)) then
         call error(label)
         write(*,*) 'Bad input parameter:'
         write(*,*) 'i=',i
         stop
      endif

      if ((j.lt.1).or.(j.gt.n)) then
         call error(label)
         write(*,*) 'Bad input parameter:'
         write(*,*) 'j=',j
         stop
      endif

      do k=1,n
         line_tmp(k)=a(j,k)
         a(j,k)=a(i,k)
      enddo
      y_tmp=y(j)
      y(j)=y(i)
      do k=1,n
         a(i,k)=line_tmp(k)
      enddo
      y(i)=y_tmp

      return
      end




      subroutine determinant(dim,mat,det,dummy_arg)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the determinant of a square matrix (of any size)
c     WARNING: this routine must not be used when you don't know whether or not
c     your compiler supports recursivity. g77 supports recursivity by default
c     (it does not need extra compilation options), ifort will compile and use this
c     routine, but execution results are WRONG if the program has not been
c     compiled using the "-recursive" option.
c
c     Inputs:
c       + dim: dimension of the matrix (>1)
c       + mat: dim*dim matrix
c     
c     Output:
c       + det: determinant
c       + dummy_arg: use "determinant" in the main call
c                  i.e. "call determinant(dim,mat,det,determinant)"
c                  This is a dummy argument, used for recursivity
c
c     input
      integer dim
      double precision mat(1:Ndim_mx,1:Ndim_mx)
c     output
      double precision det
c     temp
      external dummy_arg
      integer i,j
      integer row
      integer dim_tmp
      double precision sign
      double precision mat_tmp(1:Ndim_mx,1:Ndim_mx),det_tmp
c     label
      character*(Nchar_mx) label
      label='subroutine determinant'

c     Debug
c      write(*,*) 'mat='
c      do i=1,dim
c         write(*,*) (mat(i,j),j=1,dim)
c      enddo
c      write(*,*)
c     Debug

      if (dim.eq.3) then
         call det3(dim,mat,det)
      else
         dim_tmp=dim-1
         det=0.0D+0
         do row=1,dim
            sign=(-1.0D+0)**(row+1)
            if (row.gt.1) then
               do i=1,row-1
                  do j=2,dim
                     mat_tmp(j-1,i)=mat(j,i)
                  enddo         ! j
               enddo            ! i
            endif
            if (row.lt.dim) then
               do i=row+1,dim
                  do j=2,dim
                     mat_tmp(j-1,i-1)=mat(j,i)
                  enddo         ! j
               enddo            ! i
            endif
c     Debug
c     write(*,*) 'row=',row,' sign=',sign
c     do i=1,dim_tmp
c     write(*,*) (mat_tmp(i,j),j=1,dim_tmp)
c     enddo
c     write(*,*)
c     Debug
            call dummy_arg(dim_tmp,mat_tmp,det_tmp,dummy_arg)
            det=det+sign*mat(1,row)*det_tmp
         enddo                  ! row
      endif ! dim=3

      return
      end



      
      subroutine det3(dim,mat,det)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the determinant of a square 3*3 matrix
c
c     Inputs:
c       + dim: dimension of the matrix (MUST BE 3)
c       + mat: 3*3 matrix
c     
c     Output:
c       + det: determinant
c

c     input
      integer dim
      double precision mat(1:Ndim_mx,1:Ndim_mx)
c     output
      double precision det
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine det3'


      if (dim.ne.3) then
         call error(label)
         write(*,*) 'Bad input argument:'
         write(*,*) 'dim=',dim
         stop
      endif

      det=mat(1,1)*(mat(2,2)*mat(3,3)-mat(2,3)*mat(3,2))
     &     -mat(1,2)*(mat(2,1)*mat(3,3)-mat(2,3)*mat(3,1))
     &     +mat(1,3)*(mat(2,1)*mat(3,2)-mat(2,2)*mat(3,1))

      return
      end



      subroutine transpose_matrix(dim,M,M_tr)
      implicit none
      include 'max.inc'
c
c     Purpose: to transpose a matrix
c     
c     Input:
c       + dim: dimension of the matrix
c       + M: matrix to transpose
c     
c     Output:
c       + M_tr: transposed matrix
c
c     I/O
      integer dim
      double precision M(1:Ndim_mx,1:Ndim_mx)
      double precision M_tr(1:Ndim_mx,1:Ndim_mx)
c     temp
      integer i,j
c     label
      character*(Nchar_mx) label
      label='subroutine transpose_matrix'

      do i=1,dim
         do j=1,dim
            M_tr(i,j)=M(j,i)
         enddo ! j
      enddo ! i

      return
      end



      subroutine cross_product(n,u,v,ncp)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the cross-product between two given vectors
c     in a cartesian coordinates system
c
c     Inputs:
c       + n: size of vectors (MUST BE 3)
c       + u: (x,y,z) cartesian coordinates of the first vector
c       + v: (x,y,z) cartesian coordinates of the second vector
c     
c     Output:
c       + cp: (x,y,z) cartesian coordinates of the
c             cross-product between u and v (also a vector)
c

c     inputs
      integer n
      double precision u(1:Ndim_mx)
      double precision v(1:Ndim_mx)
c     output
      double precision ncp(1:Ndim_mx)
c     temp
      double precision cp(1:Ndim_mx)
      double precision nu(1:Ndim_mx)
      double precision nv(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine cross_product'

      if (n.ne.3) then
         call error(label)
         write(*,*) 'Bad input argument:'
         write(*,*) 'n=',n
         stop
      endif

      call normalize_vector(n,u,nu)
      call normalize_vector(n,v,nv)

      cp(1)=nu(2)*nv(3)-nu(3)*nv(2)
      cp(2)=nu(3)*nv(1)-nu(1)*nv(3)
      cp(3)=nu(1)*nv(2)-nu(2)*nv(1)
      call normalize_vector(n,cp,ncp)

      return
      end




      subroutine add_matrix(n,M1,M2,M)
      implicit none
      include 'max.inc'
c
c     Purpose: to add two square matrices
c
c     Inputs:
c       + n: size of the matrices
c       + M1: matrix number 1
c       + M2: matrix number 2
c     
c     Outputs:
c       + M=M1+M2 (also a square matrix of size n)
c
c     inputs
      integer n
      double precision M1(1:Ndim_mx,1:Ndim_mx)
      double precision M2(1:Ndim_mx,1:Ndim_mx)
c     outputs
      double precision M(1:Ndim_mx,1:Ndim_mx)
c     temp
      integer i,j
c     label
      character*(Nchar_mx) label
      label='subroutine add_matrix'

      do i=1,n
         do j=1,n
            M(i,j)=M1(i,j)+M2(i,j)
         enddo ! j
      enddo ! i

      return
      end



      subroutine multiply_matrix(n,M1,M2,M)
      implicit none
      include 'max.inc'
c
c     Purpose: to multiply two square matrices
c
c     Inputs:
c       + n: size of the matrices
c       + M1: matrix number 1
c       + M2: matrix number 2
c     
c     Outputs:
c       + M=M1*M2 (also a square matrix of size n)
c
c     inputs
      integer n
      double precision M1(1:Ndim_mx,1:Ndim_mx)
      double precision M2(1:Ndim_mx,1:Ndim_mx)
c     outputs
      double precision M(1:Ndim_mx,1:Ndim_mx)
c     temp
      integer i,j,k
c     label
      character*(Nchar_mx) label
      label='subroutine multiply_matrix'

      do i=1,n
         do j=1,n
            M(i,j)=0.0D+0
            do k=1,n
               M(i,j)=M(i,j)+M1(i,k)*M2(k,j)
            enddo ! k
         enddo ! j
      enddo ! i

      return
      end



      subroutine matrix_vector(n,M1,vector,M)
      implicit none
      include 'max.inc'
c
c     Purpose: to multiply a square matrix by a vector
c
c     Inputs:
c       + n: size of matrix M1 and vector
c       + M1: square matrix
c       + vector: vector of size n
c     
c     Outputs:
c       + M=M1*vector (also a vector of size n)
c
c     inputs
      integer n
      double precision M1(1:Ndim_mx,1:Ndim_mx)
      double precision vector(1:Ndim_mx)
c     outputs
      double precision M(1:Ndim_mx)
c     temp
      integer i,k
c     label
      character*(Nchar_mx) label
      label='subroutine matrix_vector'

      do i=1,n
         M(i)=0.0D+0
         do k=1,n
            M(i)=M(i)+M1(i,k)*vector(k)
         enddo                  ! k
      enddo                     ! i

      return
      end



      subroutine matrix_scalar(n,M1,scalar,M)
      implicit none
      include 'max.inc'
c
c     Purpose: to multiply a square matrix by a real
c
c     Inputs:
c       + n: size of matrix M1 and vector
c       + M1: square matrix
c       + scalar: the real value the M1 matrix has to be multiplied by
c     
c     Outputs:
c       + M=M1*scalar (also a square matrix of size n)
c
c     inputs
      integer n
      double precision M1(1:Ndim_mx,1:Ndim_mx)
      double precision scalar
c     outputs
      double precision M(1:Ndim_mx,1:Ndim_mx)
c     temp
      integer i,j
c     label
      character*(Nchar_mx) label
      label='subroutine matrix_scalar'

      do i=1,n
         do j=1,n
            M(i,j)=M1(i,j)*scalar
         enddo                  ! j
      enddo                     ! i

      return
      end



      subroutine are_colinear(n,u,v,col)
      implicit none
      include 'max.inc'
c
c     Purpose: determine whether or not provided vectors "u" and "v"
c     are colinear
c
c     col: 0 if "u" and "v" are not colinear; 1 if they are
c
c     I/O
      integer n,col
      double precision u(1:Ndim_mx)
      double precision v(1:Ndim_mx)
c     temp
      double precision prod,epsilon
      parameter(epsilon=1.0D-3)
      logical u_is_null,v_is_null
      integer i
c     label
      character*(Nchar_mx) label
      label='subroutine are_colinear'

      u_is_null=.true.
      do i=1,n
         if (u(i).ne.0.0D+0) then
            u_is_null=.false.
            goto 111
         endif
      enddo                     ! i
 111  continue
      v_is_null=.true.
      do i=1,n
         if (v(i).ne.0.0D+0) then
            v_is_null=.false.
            goto 112
         endif
      enddo                     ! i
 112  continue
c     
      if ((u_is_null).and.(v_is_null)) then
c     special case of null vectors
         col=1
      else
c     all other casers
         call scalar_product(n,u,v,prod)
         if (dabs(1.0D+0-dabs(prod)).lt.epsilon) then
            col=1
         else
            col=0
         endif
      endif

      return
      end
      


      subroutine scalar_product(n,u,v,prod)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the scalar product of two vectors
c     Input vectors u and v will be normalized so that the
c     resulting scalar product is between -1 and 1 (it can be
c     interpreted as the cosine of an angle between 0 and pi)
c
      integer n
      double precision u(1:Ndim_mx)
      double precision v(1:Ndim_mx)
      double precision prod
c     temp
      double precision nu(1:Ndim_mx)
      double precision nv(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine scalar_product'

      call normalize_vector(n,u,nu)
      call normalize_vector(n,v,nv)
      call vector_vector(n,nu,nv,prod)

      if (prod.gt.1.0D+0) then
         prod=1.0D+0
      else if (prod.lt.-1.0D+0) then
         prod=-1.0D+0
      endif

      return
      end



      subroutine vectorial_product(n,u,v,w)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the vectorial product of two vectors
c     Input vectors u and v will be normalized 
c
      integer n
      double precision u(1:Ndim_mx)
      double precision v(1:Ndim_mx)
      double precision w(1:Ndim_mx)
c     temp
      double precision nu(1:Ndim_mx)
      double precision nv(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine vectorial_product'

      call normalize_vector(n,u,nu)
      call normalize_vector(n,v,nv)
      w(1)=u(2)*v(3)-v(2)*u(3)
      w(2)=-(u(1)*v(3)-v(1)*u(3))
      w(3)=u(1)*v(2)-v(1)*u(2)

      return
      end



      subroutine vector_vector(n,u,v,prod)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the product of two vectors
c
c     I/O
      integer n
      double precision u(1:Ndim_mx)
      double precision v(1:Ndim_mx)
      double precision prod
c     temp
      integer i
c     label
      character*(Nchar_mx) label
      label='subroutine vector_vector'

      prod=0.0D+0
      do i=1,n
         prod=prod+u(i)*v(i)
      enddo

      return
      end



      subroutine normalize_vector(n,u,nu)
      implicit none
      include 'max.inc'
c     I/O
      integer n
      double precision u(1:Ndim_mx)
      double precision nu(1:Ndim_mx)
c     temp
      double precision length
      integer i
c     label
      character*(Nchar_mx) label
      label='subroutine normalize_vector'

      call vector_length(n,u,length)
c     Debug
c      write(*,*) 'length=',length
c     Debug
      do i=1,n
         nu(i)=u(i)/length
      enddo

      return
      end



      subroutine add_vectors(n,u1,u2,v)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the sum of two vectors
c
c     I/O
      integer n
      double precision u1(1:Ndim_mx)
      double precision u2(1:Ndim_mx)
      double precision v(1:Ndim_mx)
c     temp
      integer i
c     label
      character*(Nchar_mx) label
      label='subroutine add_vectors'

      do i=1,n
         v(i)=u1(i)+u2(i)
      enddo

      return
      end



      subroutine substract_vectors(n,u1,u2,v)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the difference of two vectors: v=u1-u2
c
c     I/O
      integer n
      double precision u1(1:Ndim_mx)
      double precision u2(1:Ndim_mx)
      double precision v(1:Ndim_mx)
c     temp
      integer i
c     label
      character*(Nchar_mx) label
      label='subroutine substract_vectors'

      do i=1,n
         v(i)=u1(i)-u2(i)
      enddo

      return
      end



      subroutine scalar_vector(n,scalar,u,v)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the product of a scalar and a vector
c
c     I/O
      integer n
      double precision scalar
      double precision u(1:Ndim_mx)
      double precision v(1:Ndim_mx)
c     temp
      integer i
c     label
      character*(Nchar_mx) label
      label='subroutine scalar_vector'

      do i=1,n
         v(i)=u(i)*scalar
      enddo

      return
      end



      subroutine copy_vector(n,u1,u2)
      implicit none
      include 'max.inc'
c     I/O
      integer n
      double precision u1(1:Ndim_mx)
      double precision u2(1:Ndim_mx)
c     temp
      integer i
c     label
      character*(Nchar_mx) label
      label='subroutine copy_vector'

      do i=1,n
         u2(i)=u1(i)
      enddo

      return
      end



      subroutine scalar_matrix(n,scalar,M1,M2)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the product of a scalar and a matrix
c
c     I/O
      integer n
      double precision scalar
      double precision M1(1:Ndim_mx,1:Ndim_mx)
      double precision M2(1:Ndim_mx,1:Ndim_mx)
c     temp
      integer i,j
c     label
      character*(Nchar_mx) label
      label='subroutine scalar_matrix'

      do i=1,n
         do j=1,n
            M2(i,j)=M1(i,j)*scalar
         enddo                  ! j
      enddo                     ! i

      return
      end



      subroutine copy_matrix(n,mat1,mat2)
      implicit none
      include 'max.inc'
c     I/O
      integer n
      double precision mat1(1:Ndim_mx,1:Ndim_mx)
      double precision mat2(1:Ndim_mx,1:Ndim_mx)
c     temp
      integer i,j
c     label
      character*(Nchar_mx) label
      label='subroutine copy_matrix'

      do i=1,n
         do j=1,n
            mat2(i,j)=mat1(i,j)
         enddo ! j
      enddo ! i

      return
      end



      subroutine identity_matrix(n,Id)
      implicit none
      include 'max.inc'
c     I/O
      integer n
      double precision Id(1:Ndim_mx,1:Ndim_mx)
c     temp
      integer i,j
c     label
      character*(Nchar_mx) label
      label='subroutine identity_matrix'

      do i=1,n
         do j=1,n
            if (i.ne.j) then
               Id(i,j)=0.0D+0
            else
               Id(i,j)=1.0D+0
            endif
         enddo ! j
      enddo ! i

      return
      end



      subroutine vector_length(n,u,length)
      implicit none
      include 'max.inc'
c     I/O
      integer n
      double precision u(1:Ndim_mx)
      double precision length
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine vector_length'
      
      call distance_cart(0.0D+0,0.0D+0,0.0D+0,u(1),u(2),u(3),length)

      return
      end



      subroutine identical_vectors(n,u,v,identical)
      implicit none
      include 'max.inc'
c
c     Purpose: to compare two vectors
c
c     Input:
c       + n: dimension of vectors
c       + u: first vector
c       + v: second vector
c
c     Output:
c       + identical: true if u and v are identical, false otherwise
c
c     I/O
      integer n
      double precision u(1:Ndim_mx)
      double precision v(1:Ndim_mx)
      logical identical
c     temp
      integer col
      double precision length_u,length_v
      double precision epsilon_l
c     label
      character*(Nchar_mx) label
      label='subroutine identical_vectors'

      epsilon_l=1.0D-5

      call are_colinear(n,u,v,col)
c     Debug
c      write(*,*) 'col=',col
c     Debug
      call vector_length(n,u,length_u)
c     Debug
c      write(*,*) 'length_u=',length_u
c     Debug
      call vector_length(n,v,length_v)
c     Debug
c      write(*,*) 'length_v=',length_v
c      write(*,*) dabs(length_u-length_v),epsilon_l
c     Debug

      if ((col.eq.1).and.
     &     (dabs(length_u-length_v).lt.epsilon_l)) then
         identical=.true.
      else
         identical=.false.
      endif

      return
      end


      
      subroutine identical_positions(n,x1,x2,identical)
      implicit none
      include 'max.inc'
c
c     Purpose: to compare two positions
c
c     Input:
c       + n: dimension of vectors
c       + x1: first position
c       + x2: second position
c
c     Output:
c       + identical: true if x1 and x2 are identical, false otherwise
c
c     I/O
      integer n
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      logical identical
c     temp
      double precision x1x2(1:Ndim_mx)
      double precision length
      double precision epsilon_l
c     label
      character*(Nchar_mx) label
      label='subroutine identical_positions'

      epsilon_l=1.0D-3

      call substract_vectors(n,x2,x1,x1x2)
      call vector_length(n,x1x2,length)

      if (dabs(length).lt.epsilon_l) then
         identical=.true.
      else
         identical=.false.
      endif

      return
      end
