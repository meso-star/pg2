      subroutine trees(dim,contour_file,trees_density)
      implicit none
      include 'max.inc'
c     
c     Purpose: to draw trees on a almost finished city
c     
c     Input:
c       + dim: dimension of space
c       + contour_file: file that contains information about every contour
c       + trees_density: surface density for trees [#/Ha]
c     
c     Output: results/vegetation.in
c     
c     I/O
      integer dim
      character*(Nchar_mx) contour_file
      double precision trees_density
c     temp
      logical keep_planting
      integer ios,iostatus
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer ilevel
      integer icontour,i,j
c     label
      character*(Nchar_mx) label
      label='subroutine trees'

      open(12,file=trim(contour_file),iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File could not be found:'
         write(*,*) trim(contour_file)
         stop
      endif
      keep_planting=.true.
      do while (keep_planting)
         read(12,*,iostat=iostatus) Ncontour
         if (iostatus.eq.0) then
            read(12,*) ilevel
            do icontour=1,Ncontour
               read(12,*) Nppc(icontour)
               do i=1,Nppc(icontour)
                  read(12,*) (contour(icontour,i,j),j=1,dim-1)
               enddo            ! i
            enddo               ! icontour
            call fill_with_trees(dim,1,Ncontour,Nppc,contour,
     &           trees_density,ilevel)
         else                   ! iostatus.ne.0
            keep_planting=.false.
         endif                  ! iostatus
      enddo ! while (keep_planting)

      return
      end
