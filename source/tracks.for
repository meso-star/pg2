c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine ctr2track(dim,Npc,ctr,Npt,track)
      implicit none
      include 'max.inc'
c     
c     Purpose: produce a track from a contour
c     
c     Input:
c       + dim: dimension of space
c       + Npc: number of points that define the contour
c       + ctr: contour
c     
c     Output:
c       + Npt: number of points that define the track
c       + track: track
c     
c     I/O
      integer dim
      integer Npc
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      integer Npt
      double precision track(1:Nppt_mx,1:Ndim_mx-1)
c     temp
      integer i,j
c     label
      character*(Nchar_mx) label
      label='subroutine ctr2track'

      Npt=Npc+1
      do i=1,Npc
         do j=1,dim-1
            track(i,j)=ctr(i,j)
         enddo                  ! j
      enddo                     ! i
      do j=1,dim-1
         track(Npt,j)=track(1,j)
      enddo                     ! j

      return
      end
      


      subroutine get_position_from_track(dim,Npoints,track,i,position)
      implicit none
      include 'max.inc'
c     
c     Purpose: to extract a position from a track
c     
c     Input:
c       + dim: dimension of space
c       + Npoints: number of points for the extracted contour
c       + track: track 
c       + i: index of the position to extract
c     
c     Output:
c       + position: required position
c     
c     I/O
      integer dim
      integer Npoints
      double precision track(1:Nppt_mx,1:Ndim_mx-1)
      integer i
      double precision position(1:Ndim_mx)
c     temp
      integer j
c     label
      character*(Nchar_mx) label
      label='subroutine get_position_from_track'

      if (i.lt.1) then
         call error(label)
         write(*,*) 'Bad input argument: i=',i
         write(*,*) 'should be > 0'
         stop
      endif
      if (i.gt.Npoints) then
         call error(label)
         write(*,*) 'Bad input argument: i=',i
         write(*,*) 'should be <= Npoints=',Npoints
         stop
      endif

      do j=1,dim-1
         position(j)=track(i,j)
      enddo                     ! j
      position(dim)=0.0D+0
      
      return
      end
