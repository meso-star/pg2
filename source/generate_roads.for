c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine connect_bridges(dim,mapXsize,mapYsize,
     &     Nroad,road_width,road_Nppt,road_track,
     &     generate_river,river_branch,
     &     Nriver,river_width,river_Nppt,river_track,
     &     Nbridge,bridge_width,bridge_track,
     &     draw_white_bands,side,river_track_index,
     &     Nsector,xcenter)
      implicit none
      include 'max.inc'
      include 'city_parameters.inc'
c
c     Purpose: to generate the roads that will connect bridges to the existing road network
c
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Nroad: number of existing roads
c       + road_width: width for each road [m]
c       + road_Nppt: number of points for each road track
c       + road_track: track that define the position of each road
c       + generate_river: set to true if a river has to be generated
c       + river_branch: set to true if a river branch has to be generated
c       + Nriver: number of rivers that have been generated
c       + river_width: width of each river that has been generated
c       + river_Nppt: number of points for the description of the central track for each river
c       + river_track: list of positions that describe the central track of each river
c       + Nbridge: number of bridges that have been generated
c       + bridge_width: width for each bridge [m]
c       + bridge_track: track that define the position of each bridge
c       + draw_white_bands: true if white bands have to be drawn
c       + side: side of the river road index 1 is on
c         - side=0: when generate_river=F (road 1 exists)
c         - side=-1: when generate_river=T; road 1 is on the left side of the river
c         - side=1:  when generate_river=T; road 1 is on the right_side of the river,
c                    in the upper corner of the map when river_branch=T
c       + river_track_index: index of the river track each bridge is located on
c        -river_track_index=1: when river_brach=F, or upper part of the main river when river_branch=T
c        -river_track_index=2: lower part of the main river when river_branch=T
c        -river_track_index=3: secondary river when river_branch=T
c       + Nsector: number of angular sectors
c       + xcenter: position of the ring center
c     
c     Output:
c       + Nroad, road_width, road_Nppt, road_track: updated values
c
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Nroad
      double precision road_width(1:Nroad_mx)
      integer road_Nppt(1:Nroad_mx)
      double precision road_track(1:Nroad_mx,
     &     1:Nppt_mx,1:Ndim_mx-1)
      logical generate_river
      logical river_branch
      integer Nriver
      double precision river_width(1:2)
      integer river_Nppt(1:2)
      double precision river_track(1:2,1:Nppt_mx,1:Ndim_mx-1)
      integer Nbridge
      double precision bridge_width(1:Nbridge_mx)
      double precision bridge_track(1:Nbridge_mx,1:2,1:Ndim_mx-1)
      logical draw_white_bands
      integer side
      integer river_track_index(1:Nbridge_mx)
      integer Nsector
      double precision xcenter(1:Ndim_mx)
c     temp
      integer i0,i1
      integer iroad,ibridge,jbridge,i,j,itrack,isector,isector_min
      double precision max_radius,radius
      double precision alpha0,alpha1,alpha,dalpha
      double precision v(1:Ndim_mx)
      double precision u12(1:Ndim_mx)
      double precision u21(1:Ndim_mx)
      double precision u23(1:Ndim_mx)
      double precision n(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
      double precision x0(1:Ndim_mx)
      double precision xd(1:Ndim_mx)
      double precision xd2(1:Ndim_mx)
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision x3(1:Ndim_mx)
      double precision x(1:Ndim_mx)
      double precision x1i(1:Ndim_mx)
      double precision x2i(1:Ndim_mx)
      double precision x1j(1:Ndim_mx)
      double precision x2j(1:Ndim_mx)
      logical intersection_found
      double precision xint(1:Ndim_mx)
      double precision d2int,d0,dmin,d23
      integer elem_type
      integer elem_index
      integer Nppt
      double precision width,rwidth
      double precision track(1:Nppt_mx,1:2)
      integer lv2rr_Nppt
      double precision lv2rr_width
      double precision lv2rr_track(1:Nppt_mx,1:2)
      logical keep_looking,border_found
      double precision xborder(1:Ndim_mx)
      double precision normal(1:Ndim_mx)
      integer Nsup_road
      double precision sup_road_width(1:Nroad_mx)
      integer sup_road_Nppt(1:Nroad_mx)
      double precision sup_road_track(1:Nroad_mx,
     &     1:Nppt_mx,1:Ndim_mx-1)
      character*(Nchar_mx) road_mat
      character*(Nchar_mx) wbands_mat
      logical left_end_is_connected(1:Nbridge_mx)
      logical right_end_is_connected(1:Nbridge_mx)
      logical jbridge_found
      integer jend_index
      double precision xdj(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine connect_bridges'
c
      do ibridge=1,Nbridge
         left_end_is_connected(ibridge)=.false.
         right_end_is_connected(ibridge)=.false.
      enddo                     ! ibridge
c      
      road_mat=trim(roads_material)
      wbands_mat=trim(white_bands_material)
      Nsup_road=0
      call level2_ring_road_index(generate_river,Nsector,i1)
      call extract_road_track(dim,
     &     Nroad,road_width,road_Nppt,road_track,
     &     i1,lv2rr_width,lv2rr_Nppt,lv2rr_track)
c
      do ibridge=1,Nbridge
c     Debug
c         write(*,*) 'ibridge=',ibridge
c     Debug
         rwidth=bridge_width(ibridge)-2*bridge_side_width
         call extract_bridge_track(dim,
     &        Nbridge,bridge_width,bridge_track,
     &        ibridge,width,track)
         do j=1,dim-1
            x1(j)=track(1,j)
            x2(j)=track(2,j)
         enddo                  ! j
         x1(dim)=0.0D+0
         x2(dim)=0.0D+0
         call substract_vectors(dim,x2,x1,tmp)
         call normalize_vector(dim,tmp,u12)
         call scalar_vector(dim,-1.0D+0,u12,u21)
c     -----------------------------------------------------------------------
c     left-side of the bridge
c     -----------------------------------------------------------------------
         call bridge_connection_position(dim,
     &        Nbridge,bridge_width,bridge_track,
     &        ibridge,-1,x1i,x2i,xd)
c     Try connecting to the existing road network
         intersection_found=.false.
         call line_scene_intersection(dim,mapXsize,mapYsize,
     &        x1,u21,generate_river,river_branch,
     &        Nriver,river_width,river_Nppt,river_track,
     &        Nroad,road_width,road_Nppt,road_track,
     &        intersection_found,xint,d2int,elem_type,elem_index)
c       + elem_type: type of element that was intersected
c         - elem_type=0: border of the map
c         - elem_type=1: river
c         - elem_type=2: road
c       + elem_index: index of the intersected element
         if (intersection_found) then
            if (elem_type.eq.2) then
               call substract_vectors(dim,xint,xd,tmp)
               call vector_length(dim,tmp,d23)
               call normalize_vector(dim,tmp,u23)
               d23=d23+road_width(elem_index)
               call scalar_vector(dim,d23,u23,tmp)
               call add_vectors(dim,xd,tmp,x3)
               Nppt=3
               do j=1,dim-1
                  track(1,j)=x1(j)
                  track(2,j)=xd(j)
                  track(3,j)=x3(j)
               enddo            ! j
               call add_road_track(dim,
     &              Nsup_road,sup_road_Nppt,
     &              sup_road_width,sup_road_track,
     &              rwidth,Nppt,track) ! automatically updates Nsup_road
               left_end_is_connected(ibridge)=.true.
            endif               ! elem_type=2
         else
            call error(label)
            write(*,*) 'intersection not found'
            stop
         endif                  ! intersection_found
c     
         if (river_track_index(ibridge).eq.3) then
            if (.not.left_end_is_connected(ibridge)) then
c     For a bridge on the secondary river:
c     1) try connecting to the another bridge on the same map area         jbridge_found=.false.
               do j=1,Nbridge
                  if (j.ne.ibridge) then
                     if ((river_track_index(j).eq.2).and.
     &                    (.not.right_end_is_connected(j)).and.
     &                    (side.eq.-1)) then
                        jbridge_found=.true.
                        jbridge=j
                        jend_index=1
                        goto 110
                     endif
                     if ((river_track_index(j).eq.3).and.
     &                    (.not.left_end_is_connected(j)).and.
     &                    (side.eq.-1)) then
                        jbridge_found=.true.
                        jbridge=j
                        jend_index=-1
                        goto 110
                     endif
                  endif         ! j.ne.ibridge
               enddo            ! j
 110           continue
               if (jbridge_found) then
                  call bridge_connection_position(dim,
     &                 Nbridge,bridge_width,bridge_track,
     &                 jbridge,jend_index,x1j,x2j,xdj)
                  Nppt=4
                  do j=1,dim-1
                     track(1,j)=x1(j)
                     track(2,j)=xd(j)
                     track(3,j)=xdj(j)
                     if (jend_index.eq.-1) then
                        track(4,j)=x1j(j)
                     else
                        track(4,j)=x2j(j)
                     endif
                  enddo         ! j
                  call add_road_track(dim,
     &                 Nsup_road,sup_road_Nppt,
     &                 sup_road_width,sup_road_track,
     &                 rwidth,Nppt,track) ! automatically updates Nsup_road
                  left_end_is_connected(ibridge)=.true.
                  if (jend_index.eq.-1) then
                     left_end_is_connected(jbridge)=.true.
                  else
                     right_end_is_connected(jbridge)=.true.
                  endif
               endif            ! jbridge_found
            endif               ! .not.left_end_is_connected(ibridge)
c     
            if (.not.left_end_is_connected(ibridge)) then
c     Try to connect to the border of the map
               call locate_border(dim,mapXsize,mapYsize,
     &              Nroad,road_width,road_Nppt,road_track,
     &              generate_river,river_branch,
     &              Nriver,river_width,river_Nppt,river_track,xd,
     &              border_found,xborder,normal)
               if (border_found) then
                  call scalar_vector(dim,20.0D+0,normal,tmp)
                  call add_vectors(dim,xborder,tmp,xd2)
                  Nppt=4
                  do j=1,dim-1
                     track(1,j)=x1(j)
                     track(2,j)=xd(j)
                     track(3,j)=xd2(j)
                     track(4,j)=xborder(j)
                  enddo         ! j
c     add road the the scene instead of temporary "sup_road"
                  call add_road_track(dim,
     &                 Nroad,road_Nppt,
     &                 road_width,road_track,
     &                 rwidth,Nppt,track) ! automatically updates Nsup_road
c     and record its definition file
                  call record_road_definition_file(dim,Nroad,
     &                 rwidth,Nppt,track,
     &                 draw_white_bands,
     &                 road_mat,wbands_mat)
                  left_end_is_connected(ibridge)=.true.
               endif            ! border_found               
            endif               ! .not.left_end_is_connected(ibridge)
c     
         endif                  ! river_track_index(ibridge)=3
c     -----------------------------------------------------------------------
c     right-side of the bridge
c     -----------------------------------------------------------------------
         call bridge_connection_position(dim,
     &        Nbridge,bridge_width,bridge_track,
     &        ibridge,1,x1i,x2i,xd)
c     Try connecting to another bridge on the same map area
         jbridge_found=.false.
         do j=1,Nbridge
            if (j.ne.ibridge) then
               if (river_branch) then
                  if (((river_track_index(ibridge).eq.1).or.
     &                 (river_track_index(ibridge).eq.3)).and.
     &                 ((river_track_index(j).eq.1).or.
     &                 (river_track_index(j).eq.3)).and.
     &                 (.not.right_end_is_connected(j)).and.
     &                 (side.eq.-1)) then
                     jbridge_found=.true.
                     jbridge=j
                     jend_index=1
                     goto 111
                  endif
                  if ((river_track_index(ibridge).eq.2).and.
     &                 (river_track_index(j).eq.2).and.
     &                 (.not.right_end_is_connected(j))) then
                     jbridge_found=.true.
                     jbridge=j
                     jend_index=1
                     goto 111
                  endif
                  if ((river_track_index(ibridge).eq.2).and.
     &                 (river_track_index(j).eq.3).and.
     &                 (.not.left_end_is_connected(j))) then
                     jbridge_found=.true.
                     jbridge=j
                     jend_index=-1
                     goto 111
                  endif
               else             ! river_branch=F
                  if ((river_track_index(ibridge).eq.1).and.
     &                 (river_track_index(j).eq.1).and.
     &                 (.not.right_end_is_connected(j)).and.
     &                 (side.eq.-1)) then
                     jbridge_found=.true.
                     jbridge=j
                     jend_index=1
                     goto 111
                  endif
               endif            ! river_branch
            endif               ! j.ne.ibridge
         enddo                  ! j
 111     continue
         if (jbridge_found) then
            call bridge_connection_position(dim,
     &           Nbridge,bridge_width,bridge_track,
     &           jbridge,jend_index,x1j,x2j,xdj)
            Nppt=4
            do j=1,dim-1
               track(1,j)=x2(j)
               track(2,j)=xd(j)
               track(3,j)=xdj(j)
               if (jend_index.eq.-1) then
                  track(4,j)=x1j(j)
               else
                  track(4,j)=x2j(j)
               endif
            enddo               ! j
            call add_road_track(dim,
     &           Nsup_road,sup_road_Nppt,
     &           sup_road_width,sup_road_track,
     &           rwidth,Nppt,track) ! automatically updates Nsup_road
            right_end_is_connected(ibridge)=.true.
            if (jend_index.eq.-1) then
               left_end_is_connected(jbridge)=.true.
            else
               right_end_is_connected(jbridge)=.true.
            endif
         endif                  ! jbridge_found
c     
         if (.not.right_end_is_connected(ibridge)) then
c     Try connecting to a existing road
            intersection_found=.false.
            call line_scene_intersection(dim,mapXsize,mapYsize,
     &           x2,u12,generate_river,river_branch,
     &           Nriver,river_width,river_Nppt,river_track,
     &           Nroad,road_width,road_Nppt,road_track,
     &           intersection_found,xint,d2int,elem_type,elem_index)
c     + elem_type: type of element that was intersected
c     - elem_type=0: border of the map
c     - elem_type=1: river
c     - elem_type=2: road
            if (intersection_found) then
               if (elem_type.eq.2) then
                  call substract_vectors(dim,xint,xd,tmp)
                  call vector_length(dim,tmp,d23)
                  call normalize_vector(dim,tmp,u23)
                  d23=d23+road_width(elem_index)
                  call scalar_vector(dim,d23,u23,tmp)
                  call add_vectors(dim,xd,tmp,x3)
                  Nppt=3
                  do j=1,dim-1
                     track(1,j)=x2(j)
                     track(2,j)=xd(j)
                     track(3,j)=x3(j)
                  enddo         ! j
                  call add_road_track(dim,
     &                 Nsup_road,sup_road_Nppt,
     &                 sup_road_width,sup_road_track,
     &                 rwidth,Nppt,track) ! automatically updates Nsup_road
                  right_end_is_connected(ibridge)=.true.
               else             ! intersection found, but not with a road
c     Try to connect to the border of the map
                  call locate_border(dim,mapXsize,mapYsize,
     &                 Nroad,road_width,road_Nppt,road_track,
     &                 generate_river,river_branch,
     &                 Nriver,river_width,river_Nppt,river_track,xd,
     &                 border_found,xborder,normal)
                  if (border_found) then
                     call scalar_vector(dim,20.0D+0,normal,tmp)
                     call add_vectors(dim,xborder,tmp,xd2)
                     Nppt=4
                     do j=1,dim-1
                        track(1,j)=x2(j)
                        track(2,j)=xd(j)
                        track(3,j)=xd2(j)
                        track(4,j)=xborder(j)
                     enddo      ! j
c     add road the the scene instead of temporary "sup_road"
                     call add_road_track(dim,
     &                    Nroad,road_Nppt,
     &                    road_width,road_track,
     &                    rwidth,Nppt,track) ! automatically updates Nsup_road
c     and record its definition file
                     call record_road_definition_file(dim,Nroad,
     &                    rwidth,Nppt,track,
     &                    draw_white_bands,
     &                    road_mat,wbands_mat)
                     right_end_is_connected(ibridge)=.true.
                  endif         ! border_found
c     
               endif
            endif               ! intersection_found
         endif                  ! .not.right_end_is_connected(ibridge)
      enddo                     ! ibridge
c     
      do itrack=1,Nsup_road
c     add to "road_track"
         call extract_road_track(dim,
     &        Nsup_road,sup_road_width,sup_road_Nppt,sup_road_track,
     &        itrack,width,Nppt,track)
         call add_road_track(dim,
     &        Nroad,road_Nppt,road_width,road_track,
     &        width,Nppt,track)
c     generate definition file
         call record_road_definition_file(dim,Nroad,
     &        width,Nppt,track,
     &        draw_white_bands,
     &        road_mat,wbands_mat)
      enddo                     ! itrack
      
      return
      end



      subroutine locate_border(dim,mapXsize,mapYsize,
     &     Nroad,road_width,road_Nppt,road_track,
     &     generate_river,river_branch,
     &     Nriver,river_width,river_Nppt,river_track,x0,
     &     border_found,xborder,normal)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c     
c     Purpose: to identify a possible position on the border of the map
c     for creating a road
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Nroad: number of existing roads
c       + road_width: width for each road [m]
c       + road_Nppt: number of points for each road track
c       + road_track: track that define the position of each road
c       + generate_river: set to true if a river has to be generated
c       + river_branch: set to true if a river branch has to be generated
c       + Nriver: number of rivers that have been generated
c       + river_width: width of each river that has been generated
c       + river_Nppt: number of points for the description of the central track for each river
c       + river_track: list of positions that describe the central track of each river
c       + x0: position from which visibility to the border of the map has to be found
c     
c     Output:
c       + border_found: true if a position could be found on the border of the map
c       + xborder: position on the border of the map
c       + normal: normal to the border @ xborder
c     
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Nroad
      double precision road_width(1:Nroad_mx)
      integer road_Nppt(1:Nroad_mx)
      double precision road_track(1:Nroad_mx,
     &     1:Nppt_mx,1:Ndim_mx-1)
      logical generate_river
      logical river_branch
      integer Nriver
      double precision river_width(1:2)
      integer river_Nppt(1:2)
      double precision river_track(1:2,1:Nppt_mx,1:Ndim_mx-1)
      double precision x0(1:Ndim_mx)
      logical border_found
      double precision xborder(1:Ndim_mx)
      double precision normal(1:Ndim_mx)
c     temp
      integer i
      double precision alpha,dalpha
      logical keep_looking
      double precision v(1:Ndim_mx)
      logical intersection_found
      double precision xint(1:Ndim_mx)
      double precision d2int
      integer elem_type,elem_index
c     label
      character*(Nchar_mx) label
      label='subroutine locate_border'

      i=0
      dalpha=1.0D-1*pi
      keep_looking=.true.
      border_found=.false.
      do while (keep_looking)
         i=i+1
         alpha=i*dalpha
         v(1)=dcos(alpha)
         v(2)=dsin(alpha)
         v(3)=0.0D+0
         call line_scene_intersection(dim,mapXsize,mapYsize,
     &        x0,v,generate_river,river_branch,
     &        Nriver,river_width,river_Nppt,river_track,
     &        Nroad,road_width,road_Nppt,road_track,
     &        intersection_found,xborder,d2int,
     &        elem_type,elem_index)
         if ((intersection_found)
     &        .and.(elem_type.eq.0)) then
            border_found=.true.
            keep_looking=.false.
         endif
         if (alpha.gt.2.0D+0*pi) then
            keep_looking=.false.
         endif
      enddo                     ! while (keep_looking)
c     
      if (border_found) then
         if (elem_index.eq.1) then
            normal(1)=0.0D+0
            normal(2)=1.0D+0
            normal(3)=0.0D+0
         else if (elem_index.eq.2) then
            normal(1)=-1.0D+0
            normal(2)=0.0D+0
            normal(3)=0.0D+0
         else if (elem_index.eq.3) then
            normal(1)=0.0D+0
            normal(2)=-1.0D+0
            normal(3)=0.0D+0
         else if (elem_index.eq.4) then
            normal(1)=1.0D+0
            normal(2)=0.0D+0
            normal(3)=0.0D+0
         else
            call error(label)
            write(*,*) 'elem_type=',elem_type
            write(*,*) 'elem_index=',elem_index
            write(*,*) 'should be in the [1-4] range'
            stop
         endif
      endif                     ! border_found
c     
      return
      end
      
      

      subroutine bridge_connection_position(dim,
     &     Nbridge,bridge_width,bridge_track,
     &     ibridge,end_index,x1,x2,xc)
      implicit none
      include 'max.inc'
      include 'city_parameters.inc'
c     
c     Purpose: to generate the connection position for a given
c     bridge, on a given end
c     
c     Input:
c       + dim: dimension of space
c       + Nbridge: number of bridges that have been generated
c       + bridge_width: width for each bridge [m]
c       + bridge_track: track that define the position of each bridge
c       + ibridge: index of the bridge
c       + end_index: index of the end of the bridge [-1,1]
c     
c     Output:
c       + x1: left-end position of the bridge
c       + x2: righ-end position of the bridge
c       + xc: connection position
c     
c     I/O
      integer dim
      integer Nbridge
      double precision bridge_width(1:Nbridge_mx)
      double precision bridge_track(1:Nbridge_mx,1:2,1:Ndim_mx-1)
      integer ibridge
      integer end_index
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision xc(1:Ndim_mx)
c     temp
      integer j
      double precision width,d0
      double precision track(1:Nppt_mx,1:2)
      double precision tmp(1:Ndim_mx)
      double precision u12(1:Ndim_mx)
      double precision u21(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine bridge_connection_position'
c
      call extract_bridge_track(dim,
     &     Nbridge,bridge_width,bridge_track,
     &     ibridge,width,track)
      do j=1,dim-1
         x1(j)=track(1,j)
         x2(j)=track(2,j)
      enddo                     ! j
      x1(dim)=0.0D+0
      x2(dim)=0.0D+0
      call substract_vectors(dim,x2,x1,tmp)
      call normalize_vector(dim,tmp,u12)
      call scalar_vector(dim,-1.0D+0,u12,u21)
c
      d0=(river_road_distance-bridge_overlapping_distance)/2.0D+0
      if (end_index.eq.-1) then
         call scalar_vector(dim,d0,u21,tmp)
         call add_vectors(dim,x1,tmp,xc)
      else if (end_index.eq.1) then
         call scalar_vector(dim,d0,u12,tmp)
         call add_vectors(dim,x2,tmp,xc)
      else
         call error(label)
         write(*,*) 'end_index=',end_index
         write(*,*) 'allowed values: -1, 1'
         stop
      endif
         
      return
      end



      subroutine generate_external_roads(dim,mapXsize,mapYsize,
     &     Nroad,road_width,road_Nppt,road_track,
     &     generate_river,river_branch,
     &     Nriver,river_width,river_Nppt,river_track,
     &     draw_white_bands,
     &     Nsector,xcenter)
      implicit none
      include 'max.inc'
      include 'constants.inc'
      include 'city_parameters.inc'
c
c     Purpose: to generate external roads (radiating from the level2 ring road)
c
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Nroad: number of existing roads
c       + road_width: width for each road [m]
c       + road_Nppt: number of points for each road track
c       + road_track: track that define the position of each road
c       + generate_river: set to true if a river has to be generated
c       + river_branch: set to true if a river branch has to be generated
c       + Nriver: number of rivers that have been generated
c       + river_width: width of each river that has been generated
c       + river_Nppt: number of points for the description of the central track for each river
c       + river_track: list of positions that describe the central track of each river
c       + draw_white_bands: true if white bands have to be drawn
c       + Nsector: number of angular sectors
c       + xcenter: position of the ring center
c     
c     Output:
c       + Nroad, road_width, road_Nppt, road_track: updated values
c
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Nroad
      double precision road_width(1:Nroad_mx)
      integer road_Nppt(1:Nroad_mx)
      double precision road_track(1:Nroad_mx,
     &     1:Nppt_mx,1:Ndim_mx-1)
      logical generate_river
      logical river_branch
      integer Nriver
      double precision river_width(1:2)
      integer river_Nppt(1:2)
      double precision river_track(1:2,1:Nppt_mx,1:Ndim_mx-1)
      logical draw_white_bands
      integer Nsector
      double precision xcenter(1:Ndim_mx)
c     temp
      integer i0,i1
      integer iroad,i,j,itrack,isector
      double precision max_radius,radius
      double precision alpha0,alpha1,alpha
      double precision u(1:Ndim_mx)
      double precision n(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
      double precision xer2(1:Ndim_mx)
      double precision x0(1:Ndim_mx)
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision x(1:Ndim_mx)
      logical intersection_found
      double precision xint(1:Ndim_mx)
      double precision d2int,d0
      integer elem_type
      integer elem_index
      integer Nppt
      double precision width
      double precision track(1:Nppt_mx,1:2)
      integer lv2rr_Nppt
      double precision lv2rr_width
      double precision lv2rr_track(1:Nppt_mx,1:2)
      integer Next_road
      double precision ext_road_width(1:Nroad_mx)
      integer ext_road_Nppt(1:Nroad_mx)
      double precision ext_road_track(1:Nroad_mx,
     &     1:Nppt_mx,1:Ndim_mx-1)
      character*(Nchar_mx) road_mat
      character*(Nchar_mx) wbands_mat
c     label
      character*(Nchar_mx) label
      label='subroutine generate_external_roads'

      Next_road=0
      width=normal_road_width
      iroad=1
      call level2_ring_road_index(generate_river,Nsector,i1)
      call extract_road_track(dim,
     &     Nroad,road_width,road_Nppt,road_track,
     &     i1,lv2rr_width,lv2rr_Nppt,lv2rr_track)
c
      do i=1,Nsector
         do j=1,dim-1
            xer2(j)=lv2rr_track(i,j)
         enddo                  ! j
         xer2(dim)=0.0D+0
         call substract_vectors(dim,xer2,xcenter,tmp)
         call vector_length(dim,tmp,d0)
         call normalize_vector(dim,tmp,u)
         d0=d0+normal_road_width
         call scalar_vector(dim,d0,u,tmp)
         call add_vectors(dim,xcenter,tmp,x0)
         call line_scene_intersection(dim,mapXsize,mapYsize,
     &        x0,u,generate_river,river_branch,
     &        Nriver,river_width,river_Nppt,river_track,
     &        Nroad,road_width,road_Nppt,road_track,
     &        intersection_found,xint,d2int,elem_type,elem_index)
         if (intersection_found) then
            if ((elem_type.eq.0).and.(d2int.gt.100.0D+0)) then
c     randomize start position of the road from the intersection
c     position with map contour
               call uniform(-pi/2.0D+0,pi/2.0D+0,alpha1)
               if (elem_index.eq.1) then
                  alpha=alpha1-pi/2.0D+0
                  n(1)=0.0D+0
                  n(2)=1.0D+0
                  n(3)=0.0D+0
               else if (elem_index.eq.2) then
                  alpha=alpha1
                  n(1)=-1.0D+0
                  n(2)=0.0D+0
                  n(3)=0.0D+0
               else if (elem_index.eq.3) then
                  alpha=alpha1+pi/2.0D+0
                  n(1)=0.0D+0
                  n(2)=-1.0D+0
                  n(3)=0.0D+0
               else if (elem_index.eq.4) then
                  alpha=alpha1+pi
                  n(1)=1.0D+0
                  n(2)=0.0D+0
                  n(3)=0.0D+0
               else
                  call error(label)
                  write(*,*) 'elem_type=',elem_type
                  write(*,*) 'elem_index=',elem_index
                  write(*,*) 'should be in the [1-4] range'
                  stop
               endif
               u(1)=dcos(alpha)
               u(2)=dsin(alpha)
               u(3)=0.0D+0
               radius=20.0D+0
               call scalar_vector(dim,radius,u,tmp)
               call add_vectors(dim,xint,tmp,x1)
               call clamp_on_ground(dim,0.0D+0,mapXsize,
     &              0.0D+0,mapYsize,x1)
               call scalar_vector(dim,10.0D+0,n,tmp)
               call add_vectors(dim,x1,tmp,x2)
c     
               iroad=iroad+1
               Nppt=3
               do j=1,dim-1
                  track(1,j)=x1(j)
                  track(2,j)=x2(j)
                  track(3,j)=lv2rr_track(i,j)
               enddo            ! j
               call add_road_track(dim,
     &              Next_road,ext_road_Nppt,
     &              ext_road_width,ext_road_track,
     &              width,Nppt,track) ! automatically updates Next_road
            else if ((elem_type.eq.2).and.(elem_index.eq.1)) then ! connect to road 1
               call substract_vectors(dim,xint,xer2,tmp)
               Nppt=2
               do j=1,dim-1
                  track(1,j)=xer2(j)
                  track(2,j)=xint(j)
               enddo            ! j
               call add_road_track(dim,
     &              Next_road,ext_road_Nppt,
     &              ext_road_width,ext_road_track,
     &              width,Nppt,track) ! automatically updates Next_road
            endif               ! elem_type / elem_type
         else
            call error(label)
            write(*,*) 'intersection not found'
            stop
         endif                  ! intersection_found
      enddo                     ! i
c     
      do itrack=1,Next_road
c     add to "road_track"
         call extract_road_track(dim,
     &        Next_road,ext_road_width,ext_road_Nppt,ext_road_track,
     &        itrack,width,Nppt,track)
         call add_road_track(dim,
     &        Nroad,road_Nppt,road_width,road_track,
     &        width,Nppt,track)
c     generate definition file
         road_mat=trim(roads_material)
         wbands_mat=trim(white_bands_material)
         call record_road_definition_file(dim,Nroad,
     &        width,Nppt,track,
     &        draw_white_bands,
     &        road_mat,wbands_mat)
      enddo                     ! itrack
      
      return
      end

      

      subroutine generate_level2_central_roads(dim,mapXsize,mapYsize,
     &     Nroad,road_width,road_Nppt,road_track,
     &     generate_river,river_branch,
     &     Nriver,river_width,river_Nppt,river_track,
     &     draw_white_bands,
     &     Nsector,xcenter)
      implicit none
      include 'max.inc'
      include 'constants.inc'
      include 'city_parameters.inc'
c
c     Purpose: to generate level2 radius roads
c
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Nroad: number of existing roads
c       + road_width: width for each road [m]
c       + road_Nppt: number of points for each road track
c       + road_track: track that define the position of each road
c       + generate_river: set to true if a river has to be generated
c       + river_branch: set to true if a river branch has to be generated
c       + Nriver: number of rivers that have been generated
c       + river_width: width of each river that has been generated
c       + river_Nppt: number of points for the description of the central track for each river
c       + river_track: list of positions that describe the central track of each river
c       + draw_white_bands: true if white bands have to be drawn
c       + Nsector: number of angular sectors
c       + xcenter: position of the ring center
c     
c     Output:
c       + Nroad, road_width, road_Nppt, road_track: updated values
c
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Nroad
      double precision road_width(1:Nroad_mx)
      integer road_Nppt(1:Nroad_mx)
      double precision road_track(1:Nroad_mx,
     &     1:Nppt_mx,1:Ndim_mx-1)
      logical generate_river
      logical river_branch
      integer Nriver
      double precision river_width(1:2)
      integer river_Nppt(1:2)
      double precision river_track(1:2,1:Nppt_mx,1:Ndim_mx-1)
      logical draw_white_bands
      integer Nsector
      double precision xcenter(1:Ndim_mx)
c     temp
      integer i0,i1
      integer iroad,i,j,itrack,isector,ring_road_index
      double precision max_radius,radius,alpha,dalpha
      double precision u(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
      double precision x(1:Ndim_mx)
      logical intersection_found
      double precision xint(1:Ndim_mx)
      double precision d2int
      integer elem_type
      integer elem_index
      integer Nppt
      double precision width
      double precision track(1:Nppt_mx,1:2)
      integer lv1rr_Nppt
      double precision lv1rr_width
      double precision lv1rr_track(1:Nppt_mx,1:2)
      integer lv2rr_Nppt
      double precision lv2rr_width
      double precision lv2rr_track(1:Nppt_mx,1:2)
      integer Ncentral_road
      double precision central_road_width(1:Nroad_mx)
      integer central_road_Nppt(1:Nroad_mx)
      double precision central_road_track(1:Nroad_mx,
     &     1:Nppt_mx,1:Ndim_mx-1)
      character*(Nchar_mx) road_mat
      character*(Nchar_mx) wbands_mat
c     label
      character*(Nchar_mx) label
      label='subroutine generate_level2_central_roads'

      Ncentral_road=0
      width=normal_road_width
      iroad=1
      call level1_ring_road_index(generate_river,i0)
      call level2_ring_road_index(generate_river,Nsector,i1)
      call extract_road_track(dim,
     &     Nroad,road_width,road_Nppt,road_track,
     &     i0,lv1rr_width,lv1rr_Nppt,lv1rr_track)
      call extract_road_track(dim,
     &     Nroad,road_width,road_Nppt,road_track,
     &     i1,lv2rr_width,lv2rr_Nppt,lv2rr_track)
      
c     generate all central roads
      Nppt=2
      do i=1,Nsector
         iroad=iroad+1
         do j=1,dim-1
            track(1,j)=lv2rr_track(i,j)
            track(2,j)=lv1rr_track(i,j)
         enddo                  ! j
         call add_road_track(dim,
     &        Ncentral_road,central_road_Nppt,
     &        central_road_width,central_road_track,
     &        width,Nppt,track) ! automatically updates Ncentral_road
      enddo                     ! isector
c     
      do itrack=1,Ncentral_road
c     add to "road_track"
         call extract_road_track(dim,
     &        Ncentral_road,central_road_width,
     &        central_road_Nppt,central_road_track,
     &        itrack,width,Nppt,track)
         call add_road_track(dim,
     &        Nroad,road_Nppt,road_width,road_track,
     &        width,Nppt,track)
c     generate definition file
         road_mat=trim(roads_material)
         wbands_mat=trim(white_bands_material)
         call record_road_definition_file(dim,Nroad,
     &        width,Nppt,track,
     &        draw_white_bands,
     &        road_mat,wbands_mat)
      enddo                     ! itrack
      
      return
      end


      
      subroutine generate_level1_central_roads(dim,mapXsize,mapYsize,
     &     Nroad,road_width,road_Nppt,road_track,
     &     generate_river,river_branch,
     &     Nriver,river_width,river_Nppt,river_track,
     &     draw_white_bands,
     &     Nsector,xcenter,
     &     csquare_Ncontour,csquare_Nppc,csquare_contour)
      implicit none
      include 'max.inc'
      include 'constants.inc'
      include 'city_parameters.inc'
c
c     Purpose: to generate level1 radius roads
c
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Nroad: number of existing roads
c       + road_width: width for each road [m]
c       + road_Nppt: number of points for each road track
c       + road_track: track that define the position of each road
c       + generate_river: set to true if a river has to be generated
c       + river_branch: set to true if a river branch has to be generated
c       + Nriver: number of rivers that have been generated
c       + river_width: width of each river that has been generated
c       + river_Nppt: number of points for the description of the central track for each river
c       + river_track: list of positions that describe the central track of each river
c       + draw_white_bands: true if white bands have to be drawn
c       + Nsector: number of angular sectors
c       + xcenter: position of the ring center
c       + csquare_Ncontour: number of contours for the central square
c       + csquare_Nppc: number of points that define each contour
c       + csquare_contour: central square contour
c     
c     Output:
c       + Nroad, road_width, road_Nppt, road_track: updated values
c
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Nroad
      double precision road_width(1:Nroad_mx)
      integer road_Nppt(1:Nroad_mx)
      double precision road_track(1:Nroad_mx,
     &     1:Nppt_mx,1:Ndim_mx-1)
      logical generate_river
      logical river_branch
      integer Nriver
      double precision river_width(1:2)
      integer river_Nppt(1:2)
      double precision river_track(1:2,1:Nppt_mx,1:Ndim_mx-1)
      logical draw_white_bands
      integer Nsector
      double precision xcenter(1:Ndim_mx)
      integer csquare_Ncontour
      integer csquare_Nppc(1:Ncontour_mx)
      double precision csquare_contour(1:Ncontour_mx,1:Nppc_mx,
     &     1:Ndim_mx-1)
c     temp
      integer iroad,i,j,itrack,isector,ring_road_index
      double precision max_radius,radius,alpha,dalpha
      double precision u(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
      double precision x(1:Ndim_mx)
      logical intersection_found
      double precision xint(1:Ndim_mx)
      double precision d2int
      integer elem_type
      integer elem_index
      double precision width
      double precision rr_width
      integer Nppt,cs_Nppt,new_Nppt,rr_Nppt
      double precision track(1:Nppt_mx,1:2)
      double precision rr_track(1:Nppt_mx,1:2)
      double precision cs_track(1:Nppt_mx,1:2)
      double precision new_track(1:Nppt_mx,1:2)
      integer Ncentral_road
      double precision central_road_width(1:Nroad_mx)
      integer central_road_Nppt(1:Nroad_mx)
      double precision central_road_track(1:Nroad_mx,
     &     1:Nppt_mx,1:Ndim_mx-1)
      character*(Nchar_mx) road_mat
      character*(Nchar_mx) wbands_mat
      logical success
c     label
      character*(Nchar_mx) label
      label='subroutine generate_level1_central_roads'

      Ncentral_road=0
      width=minor_road_width
      iroad=1
c     make track from contour of the central square
      cs_Nppt=Nsector
      do i=1,csquare_Nppc(1)
         do j=1,dim-1
            cs_track(i,j)=csquare_contour(1,i,j)
         enddo                  ! j
      enddo                     ! i
c     "generate_close_contour" works on a track that defines a close contour,
c     and the last point has to be different from the first one.
c      call generate_close_contour(dim,
c     &     cs_Nppt,cs_track,width/2.0D+0,.false.,
c     &     new_Nppt,new_track)
      call generate_close_contour2(dim,.false.,cs_Nppt,cs_track,
     &     0.0D+0,width/2.0D+0,.false.,.true.,success,
     &     new_Nppt,new_track)
      if (.not.success) then
         call error(label)
         write(*,*) 'could not generate outward contour'
         write(*,*) 'for the road at distance:'
         write(*,*) 'width/2=',width/2.0D+0
         stop
      endif                     ! success=F
c     but in this case, the track has to be closed: the last point
c     should be identical to the first one
      new_Nppt=new_Nppt+1
      do j=1,dim-1
         new_track(new_Nppt,j)=new_track(1,j)
      enddo                     ! j
c     then it can be added to the list of roads
      call add_road_track(dim,
     &     Ncentral_road,central_road_Nppt,
     &     central_road_width,central_road_track,
     &     width,new_Nppt,new_track)

c     The first road is the major road
c     if generate_river, roads index 2 and 3 are dirt roads
c     the next one is the ring road
      call level1_ring_road_index(generate_river,ring_road_index)
      call extract_road_track(dim,
     &     Nroad,road_width,road_Nppt,road_track,
     &     ring_road_index,rr_width,rr_Nppt,rr_track)
      
c     generate all central roads
      Nppt=2
      do i=1,Nsector
         iroad=iroad+1
         do j=1,dim-1
            track(1,j)=rr_track(i,j)
            track(2,j)=new_track(i,j)
         enddo                  ! j
         call add_road_track(dim,
     &        Ncentral_road,central_road_Nppt,
     &        central_road_width,central_road_track,
     &        width,Nppt,track) ! automatically updates Ncentral_road
      enddo                     ! isector
c     
      do itrack=1,Ncentral_road
c     add to "road_track"
         call extract_road_track(dim,
     &        Ncentral_road,central_road_width,
     &        central_road_Nppt,central_road_track,
     &        itrack,width,Nppt,track)
         call add_road_track(dim,
     &        Nroad,road_Nppt,road_width,road_track,
     &        width,Nppt,track)
c     generate definition file
         road_mat=trim(roads_material)
         wbands_mat=trim(white_bands_material)
         call record_road_definition_file(dim,Nroad,
     &        width,Nppt,track,
     &        draw_white_bands,
     &        road_mat,wbands_mat)
      enddo                     ! itrack
      
      return
      end

      

      subroutine level2_ring_road_index(generate_river,Nsector,
     &     ring_road_index)
      implicit none
      include 'max.inc'
c     
c     Purpose: to get the index of the level 1 ring road
c     
c     Input:
c       + generate_river: set to true if a river has to be generated
c       + Nsector: number of angular sectors
c     
c     Output:
c       + ring_road_index: index of the level2 ring road
c     
c     I/O
      logical generate_river
      integer Nsector
      integer ring_road_index
c     temp
      integer i0
c     label
      character*(Nchar_mx) label
      label='subroutine level2_ring_road_index'

      call level1_ring_road_index(generate_river,i0)
      ring_road_index=i0+Nsector+2

      return
      end

      

      subroutine level1_ring_road_index(generate_river,ring_road_index)
      implicit none
      include 'max.inc'
c     
c     Purpose: to get the index of the level 1 ring road
c     
c     Input:
c       + generate_river: set to true if a river has to be generated
c     
c     Output:
c       + ring_road_index: index of the level1 ring road
c     
c     I/O
      logical generate_river
      integer ring_road_index
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine level1_ring_road_index'

      if (generate_river) then
         ring_road_index=4
      else
         ring_road_index=2
      endif

      return
      end
      
      

      subroutine generate_level2_road_ring(dim,mapXsize,mapYsize,
     &     Nroad,road_width,road_Nppt,road_track,
     &     generate_river,river_branch,
     &     Nriver,river_width,river_Nppt,river_track,
     &     draw_white_bands,Nsector,xcenter,alpha1,
     &     alpha2)
      implicit none
      include 'max.inc'
      include 'constants.inc'
      include 'city_parameters.inc'
c
c     Purpose: to generate the level2 road ring
c
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Nroad: number of existing roads
c       + road_width: width for each road [m]
c       + road_Nppt: number of points for each road track
c       + road_track: track that define the position of each road
c       + generate_river: set to true if a river has to be generated
c       + river_branch: set to true if a river branch has to be generated
c       + Nriver: number of rivers that have been generated
c       + river_width: width of each river that has been generated
c       + river_Nppt: number of points for the description of the central track for each river
c       + river_track: list of positions that describe the central track of each river
c       + draw_white_bands: true if white bands have to be drawn
c       + Nsector: number of angular sectors
c       + xcenter: position of the ring center
c       + alpha2: angle for generating level1 ring road contour
c     
c     Output:
c       + alpha2: angle for generating level2 ring road contour
c       + Nroad, road_width, road_Nppt, road_track: updated values
c
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Nroad
      double precision road_width(1:Nroad_mx)
      integer road_Nppt(1:Nroad_mx)
      double precision road_track(1:Nroad_mx,
     &     1:Nppt_mx,1:Ndim_mx-1)
      logical generate_river
      logical river_branch
      integer Nriver
      double precision river_width(1:2)
      integer river_Nppt(1:2)
      double precision river_track(1:2,1:Nppt_mx,1:Ndim_mx-1)
      logical draw_white_bands
      integer Nsector
      double precision alpha1
      double precision alpha2
      double precision xcenter(1:Ndim_mx)
c     temp
      integer i0,is1,is2
      double precision as1,as2
      integer iroad,i,j,itrack,isector
      double precision max_radius,radius,alpha,dalpha
      double precision x0(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
      double precision x(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      logical intersection_found
      double precision xint(1:Ndim_mx)
      double precision d2int,d0,dmax,d1,d2,dr
      integer elem_type
      integer elem_index
      double precision width
      integer Nppt
      double precision track(1:Nppt_mx,1:2)
      integer Nppt_rr1
      double precision track_rr1(1:Nppt_mx,1:2)
      integer Nroad_in_ring
      double precision road_in_ring_width(1:Nroad_mx)
      integer road_in_ring_Nppt(1:Nroad_mx)
      double precision road_in_ring_track(1:Nroad_mx,
     &     1:Nppt_mx,1:Ndim_mx-1)
      character*(Nchar_mx) road_mat
      character*(Nchar_mx) wbands_mat
c     label
      character*(Nchar_mx) label
      label='subroutine generate_level2_road_ring'

      Nroad_in_ring=1
      iroad=1
      road_in_ring_width(1)=normal_road_width
      road_in_ring_Nppt(1)=0
      dalpha=2.0D+0*pi/dble(Nsector)
c     call uniform(0.0D+0,dalpha,alpha2)
      alpha2=alpha1
      call level1_ring_road_index(generate_river,i0)
      call extract_road_track(dim,
     &     Nroad,road_width,road_Nppt,road_track,
     &     i0,width,Nppt_rr1,track_rr1)
c     computation of "dmax", the maximum distance between "xcenter"
c     and the points of the level 1 road ring contour
      dmax=0.0D+0
      do isector=1,Nsector
         do j=1,dim-1
            x0(j)=track_rr1(isector,j)
         enddo                  ! j
         x0(dim)=0.0D+0
         call substract_vectors(dim,x0,xcenter,tmp)
         call vector_length(dim,tmp,d0)
         if (d0.gt.dmax) then
            dmax=d0
         endif
      enddo                     ! isector
      dmax=dmax+10.0D+0
c     
      do isector=1,Nsector
         call sample_normal(4.0D+2,4.0D+1,max_radius)
         alpha=alpha2+dalpha*dble(isector-1)
         do j=1,dim-1
            P1(j)=track_rr1(isector,j)
         enddo                  ! j
         P1(dim)=0.0D+0
         call substract_vectors(dim,xcenter,P1,tmp)
         call vector_length(dim,tmp,d1)
         d1=d1+normal_road_width
         call sample_normal(mu_lv2rr_radius,sigma_lv2rr_radius,dr) ! dr: additionnal length from d1
         u(1)=dcos(alpha)
         u(2)=dsin(alpha)
         u(3)=0.0D+0
         call scalar_vector(dim,d1,u,tmp)
         call add_vectors(dim,xcenter,tmp,x0) ! x0=xcenter+dmax*u
c     look for intersections between (xcenter,u) and elements of the scene
         call line_scene_intersection(dim,mapXsize,mapYsize,
     &        x0,u,generate_river,river_branch,
     &        Nriver,river_width,river_Nppt,river_track,
     &        Nroad,road_width,road_Nppt,road_track,
     &        intersection_found,xint,d2int,elem_type,elem_index)
         if ((intersection_found).and.(d2int.le.dr+20.0D+0)) then
            radius=d1+0.70D+0*d2int
         else
            radius=d1+dr
         endif
c     Debug
c         if (isector.eq.9) then
c            write(*,*) 'intersection_found=',intersection_found
c            if (intersection_found) then
c               write(*,*) 'd2int=',d2int
c               write(*,*) 'dr=',dr,' d1=',d1
c               write(*,*) 'radius=',radius
c            endif
c            stop
c         endif
c     Debug
         call scalar_vector(dim,radius,u,tmp)
         call add_vectors(dim,xcenter,tmp,x)
         call clamp_near_ground(dim,0.0D+0,mapXsize,0.0D+0,mapYsize,x)
         road_in_ring_Nppt(iroad)=road_in_ring_Nppt(iroad)+1
         if (road_in_ring_Nppt(iroad).gt.Nppt_mx) then
            call error(label)
            write(*,*) 'road_in_ring_Nppt(',iroad,')=',
     &           road_in_ring_Nppt(1)
            write(*,*) '> Nppt_mx=',Nppt_mx
            stop
         else
            do j=1,dim-1
               road_in_ring_track(iroad,road_in_ring_Nppt(iroad),j)=
     &              x(j)
            enddo               ! j
         endif                  ! road_in_ring_Nppt(iroad)>Nppt_mx
      enddo                     ! isector
c     duplicate first position (close the loop)
      road_in_ring_Nppt(iroad)=road_in_ring_Nppt(iroad)+1
      if (road_in_ring_Nppt(iroad).gt.Nppt_mx) then
         call error(label)
         write(*,*) 'road_in_ring_Nppt(',iroad,')=',
     &        road_in_ring_Nppt(1)
         write(*,*) '> Nppt_mx=',Nppt_mx
         stop
      else
         do j=1,dim-1
            road_in_ring_track(iroad,road_in_ring_Nppt(iroad),j)=
     &           road_in_ring_track(iroad,1,j)
         enddo                  ! j
      endif                     ! road_in_ring_Nppt(iroad)>Nppt_mx
c     
      do itrack=1,Nroad_in_ring
c     add to "road_track"
         call extract_road_track(dim,
     &        Nroad_in_ring,road_in_ring_width,
     &        road_in_ring_Nppt,road_in_ring_track,
     &        itrack,width,Nppt,track)
         call add_road_track(dim,
     &        Nroad,road_Nppt,road_width,road_track,
     &        width,Nppt,track)
c     generate definition file
         road_mat=trim(roads_material)
         wbands_mat=trim(white_bands_material)
         call record_road_definition_file(dim,Nroad,
     &        width,Nppt,track,
     &        draw_white_bands,
     &        road_mat,wbands_mat)
      enddo                     ! itrack

      return
      end
      
      

      subroutine generate_level1_road_ring(dim,mapXsize,mapYsize,
     &     Nroad,road_width,road_Nppt,road_track,
     &     generate_river,river_branch,
     &     Nriver,river_width,river_Nppt,river_track,
     &     draw_white_bands,
     &     Nsector,alpha1,xcenter)
      implicit none
      include 'max.inc'
      include 'constants.inc'
      include 'city_parameters.inc'
c
c     Purpose: to generate the level1 ring of road that define the downtown zone
c
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Nroad: number of existing roads
c       + road_width: width for each road [m]
c       + road_Nppt: number of points for each road track
c       + road_track: track that define the position of each road
c       + generate_river: set to true if a river has to be generated
c       + river_branch: set to true if a river branch has to be generated
c       + Nriver: number of rivers that have been generated
c       + river_width: width of each river that has been generated
c       + river_Nppt: number of points for the description of the central track for each river
c       + river_track: list of positions that describe the central track of each river
c       + draw_white_bands: true if white bands have to be drawn
c     
c     Output:
c       + Nsector: number of angular sectors
c       + alpha1: angle for generating level1 ring road contour
c       + xcenter: position of the ring center
c       + Nroad, road_width, road_Nppt, road_track: updated values
c
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Nroad
      double precision road_width(1:Nroad_mx)
      integer road_Nppt(1:Nroad_mx)
      double precision road_track(1:Nroad_mx,
     &     1:Nppt_mx,1:Ndim_mx-1)
      logical generate_river
      logical river_branch
      integer Nriver
      double precision river_width(1:2)
      integer river_Nppt(1:2)
      double precision river_track(1:2,1:Nppt_mx,1:Ndim_mx-1)
      logical draw_white_bands
      integer Nsector
      double precision alpha1
      double precision xcenter(1:Ndim_mx)
c     temp
      integer iroad,i,j,itrack,isector
      double precision max_radius,radius,alpha,dalpha
      double precision u(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
      double precision x(1:Ndim_mx)
      logical intersection_found
      double precision xint(1:Ndim_mx)
      double precision d2int
      integer elem_type
      integer elem_index
      double precision width
      integer Nppt
      double precision track(1:Nppt_mx,1:2)
      integer Nroad_in_ring
      double precision road_in_ring_width(1:Nroad_mx)
      integer road_in_ring_Nppt(1:Nroad_mx)
      double precision road_in_ring_track(1:Nroad_mx,
     &     1:Nppt_mx,1:Ndim_mx-1)
      character*(Nchar_mx) road_mat
      character*(Nchar_mx) wbands_mat
c     label
      character*(Nchar_mx) label
      label='subroutine generate_level1_road_ring'

      if (generate_river) then
         xcenter(1)=0.35D+0*mapXsize
         xcenter(2)=0.50D+0*mapYsize
      else
         xcenter(1)=0.50D+0*mapXsize
         xcenter(2)=0.50D+0*mapYsize
      endif
      xcenter(dim)=0.0D+0

      Nroad_in_ring=1
      iroad=1
      road_in_ring_width(1)=normal_road_width
      road_in_ring_Nppt(1)=0
      call choose_uniform_integer(Nsector_min,Nsector_max,Nsector)
      dalpha=2.0D+0*pi/dble(Nsector)
      call uniform(0.0D+0,dalpha,alpha1)
      do isector=1,Nsector
         call sample_normal(mu_lv1rr_radius,sigma_lv1rr_radius,
     &        max_radius)
         alpha=alpha1+dalpha*dble(isector-1)
         u(1)=dcos(alpha)
         u(2)=dsin(alpha)
         u(3)=0.0D+0
c     look for intersections between (xcenter,u) and elements of the scene
         call line_scene_intersection(dim,mapXsize,mapYsize,
     &        xcenter,u,generate_river,river_branch,
     &        Nriver,river_width,river_Nppt,river_track,
     &        Nroad,road_width,road_Nppt,road_track,
     &        intersection_found,xint,d2int,elem_type,elem_index)
         if ((intersection_found).and.
     &        (d2int.lt.max_radius+40.0D+0)) then
            radius=0.60D+0*d2int
         else                   ! no intersection
            radius=max_radius
         endif
         call scalar_vector(dim,radius,u,tmp)
         call add_vectors(dim,xcenter,tmp,x)
         call clamp_near_ground(dim,0.0D+0,mapXsize,0.0D+0,mapYsize,x)
         road_in_ring_Nppt(iroad)=road_in_ring_Nppt(iroad)+1
         if (road_in_ring_Nppt(iroad).gt.Nppt_mx) then
            call error(label)
            write(*,*) 'road_in_ring_Nppt(',iroad,')=',
     &           road_in_ring_Nppt(1)
            write(*,*) '> Nppt_mx=',Nppt_mx
            stop
         else
            do j=1,dim-1
               road_in_ring_track(iroad,road_in_ring_Nppt(iroad),j)=
     &              x(j)
            enddo               ! j
         endif                  ! road_in_ring_Nppt(iroad)>Nppt_mx
      enddo                     ! isector
c     duplicate first position (close the loop)
      road_in_ring_Nppt(iroad)=road_in_ring_Nppt(iroad)+1
      if (road_in_ring_Nppt(iroad).gt.Nppt_mx) then
         call error(label)
         write(*,*) 'road_in_ring_Nppt(',iroad,')=',
     &        road_in_ring_Nppt(1)
         write(*,*) '> Nppt_mx=',Nppt_mx
         stop
      else
         do j=1,dim-1
            road_in_ring_track(iroad,road_in_ring_Nppt(iroad),j)=
     &           road_in_ring_track(iroad,1,j)
         enddo                  ! j
      endif                     ! road_in_ring_Nppt(iroad)>Nppt_mx
c     
      do itrack=1,Nroad_in_ring
c     add to "road_track"
         call extract_road_track(dim,
     &        Nroad_in_ring,road_in_ring_width,
     &        road_in_ring_Nppt,road_in_ring_track,
     &        itrack,width,Nppt,track)
         call add_road_track(dim,
     &        Nroad,road_Nppt,road_width,road_track,
     &        width,Nppt,track)
c     generate definition file
         road_mat=trim(roads_material)
         wbands_mat=trim(white_bands_material)
         call record_road_definition_file(dim,Nroad,
     &        width,Nppt,track,
     &        draw_white_bands,
     &        road_mat,wbands_mat)
      enddo                     ! itrack
      
      return
      end

      

      subroutine generate_major_roads(dim,mapXsize,mapYsize,
     &     Nroad,road_width,road_Nppt,road_track,
     &     generate_river,river_branch,
     &     Nriver,river_width,river_Nppt,river_track,
     &     draw_white_bands,side)
      implicit none
      include 'max.inc'
      include 'city_parameters.inc'
c     
c     Purpose: to generate major roads
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Nroad: number of existing roads
c       + road_width: width for each road [m]
c       + road_Nppt: number of points for each road track
c       + road_track: track that define the position of each road
c       + generate_river: set to true if a river has to be generated
c       + river_branch: set to true if a river branch has to be generated
c       + Nriver: number of rivers that have been generated
c       + river_width: width of each river that has been generated
c       + river_Nppt: number of points for the description of the central track for each river
c       + river_track: list of positions that describe the central track of each river
c       + draw_white_bands: true if white bands have to be drawn
c     
c     Output:
c       + Nroad,road_width, road_Nppt, road_track: updated values
c       + side: side of the river road index 1 is on
c         - side=0: when generate_river=F (road 1 exists)
c         - side=-1: when generate_river=T; road 1 is on the left side of the river
c         - side=1:  when generate_river=T; road 1 is on the right_side of the river,
c                    in the upper corner of the map when river_branch=T
c     
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Nroad
      double precision road_width(1:Nroad_mx)
      integer road_Nppt(1:Nroad_mx)
      double precision road_track(1:Nroad_mx,
     &     1:Nppt_mx,1:Ndim_mx-1)
      logical generate_river
      logical river_branch
      integer Nriver
      double precision river_width(1:2)
      integer river_Nppt(1:2)
      double precision river_track(1:2,1:Nppt_mx,1:Ndim_mx-1)
      logical draw_white_bands
      integer side
c     temp
      integer i,j,itrack
      double precision xmin,xmax,ymin,ymax
      double precision Lmin,Lmax,L1,L2,L3,L4
      double precision central_track(1:Nppt_mx,1:Ndim_mx-1)
      double precision distance
      double precision change_side
      integer Nppt,Nppt1,Nppt2
      double precision width
      double precision track(1:Nppt_mx,1:Ndim_mx-1)
      double precision track1(1:Nppt_mx,1:Ndim_mx-1)
      double precision track2(1:Nppt_mx,1:Ndim_mx-1)
      integer duplicated_Nppt
      double precision duplicated_track(1:Nppt_mx,1:Ndim_mx-1)
      character*(Nchar_mx) road_mat
      character*(Nchar_mx) wbands_mat
      integer Nmajorroad
      double precision majorroad_width(1:Nroad_mx)
      integer majorroad_Nppt(1:Nroad_mx)
      double precision majorroad_track(1:Nroad_mx,
     &     1:Nppt_mx,1:Ndim_mx-1)
c     label
      character*(Nchar_mx) label
      label='subroutine generate_major_roads'

      Nmajorroad=0
      distance=river_width(1)/2.0D+0
     &     +river_road_distance
     &     +major_road_width/2.0D+0
      if (.not.generate_river) then
         side=0
c     A road track has to be generated: re-use the simple geometry for a river, without branching
         Nmajorroad=Nmajorroad+1
         if (Nmajorroad.gt.Nroad_mx) then
            call error(label)
            write(*,*) 'Nmajorroad=',Nmajorroad
            write(*,*) '> Nroad_mx=',Nroad_mx
            stop
         endif
         majorroad_Nppt(Nmajorroad)=4 ! number of points needed to describe the central track
         majorroad_width(Nmajorroad)=major_road_width
c     sample position at the top of the map
         xmin=0.50D+0*mapXsize+majorroad_width(Nmajorroad)/2.0D+0
         xmax=0.90D+0*mapXsize-majorroad_width(Nmajorroad)/2.0D+0
         call uniform(xmin,xmax,central_track(1,1))
         central_track(1,2)=mapYsize
c     sample length of the first segment
         Lmin=0.1D+0*mapYsize
         Lmax=0.25D+0*mapYsize
         call uniform(Lmin,Lmax,L1)
c     position at the end of the first segment
         central_track(2,1)=central_track(1,1)
         central_track(2,2)=central_track(1,2)-L1
c     sample position at the bottom of the map
         call uniform(xmin,xmax,central_track(4,1))
         central_track(4,2)=0.0D+0
c     sample length of the 3rd segment
         call uniform(Lmin,Lmax,L3)
c     position at the end of the second segment
         central_track(3,1)=central_track(4,1)
         central_track(3,2)=central_track(4,2)+L3
c     add 'central_track' to 'majorroad_track'
         do i=1,majorroad_Nppt(Nmajorroad)
            do j=1,dim-1
               majorroad_track(Nmajorroad,i,j)=central_track(i,j)
            enddo               ! j
         enddo                  ! i
      else                      ! generate_river=T
         side=-1                ! left side of the river
         call random_gen(change_side)
         if (change_side.lt.0.50D+0) then
            side=1              ! right side of the river
         endif
         if (river_branch) then
            call extract_river_track(dim,Nriver,river_Nppt,
     &           river_track,1,Nppt1,track1)
            if (side.eq.-1) then
               call extract_river_track(dim,Nriver,river_Nppt,
     &              river_track,2,Nppt2,track2)
            else
               call extract_river_track(dim,Nriver,river_Nppt,
     &              river_track,3,Nppt2,track2)
            endif
            Nppt=Nppt1+Nppt2-1
            do i=1,Nppt1-1
               do j=1,dim-1
                  track(i,j)=track1(i,j)
               enddo            ! j
            enddo               ! i
            do i=1,Nppt2
               do j=1,dim-1
                  track(Nppt1-1+i,j)=track2(i,j)
               enddo            ! j
            enddo               ! i
         else                   ! river_branch=.false.
            call extract_river_track(dim,Nriver,river_Nppt,
     &           river_track,1,Nppt,track)
         endif                  ! river_branch
         call duplicate_track(dim,mapXsize,mapYsize,
     &        Nppt,track,distance,side,
     &        duplicated_Nppt,central_track)
         Nmajorroad=Nmajorroad+1
         if (Nmajorroad.gt.Nroad_mx) then
            call error(label)
            write(*,*) 'Nmajorroad=',Nmajorroad
            write(*,*) '> Nroad_mx=',Nroad_mx
            stop
         endif
         majorroad_Nppt(Nmajorroad)=duplicated_Nppt
         majorroad_width(Nmajorroad)=major_road_width
         do i=1,majorroad_Nppt(Nmajorroad)
            do j=1,dim-1
               majorroad_track(Nmajorroad,i,j)=central_track(i,j)
            enddo               ! j
         enddo                  ! i
      endif                     ! generate_river
c     
      do itrack=1,Nmajorroad
c     add to "road_track"
         call extract_road_track(dim,
     &        Nmajorroad,majorroad_width,
     &        majorroad_Nppt,majorroad_track,
     &        itrack,width,Nppt,track)
         call add_road_track(dim,
     &        Nroad,road_Nppt,road_width,road_track,
     &        width,Nppt,track)
c     generate definition file
         road_mat=trim(roads_material)
         wbands_mat=trim(white_bands_material)
         call record_road_definition_file(dim,Nroad,
     &        width,Nppt,track,
     &        draw_white_bands,
     &        road_mat,wbands_mat)
      enddo                     ! itrack
      
      return
      end



      subroutine generate_dirt_roads(dim,mapXsize,mapYsize,
     &     Nroad,road_width,road_Nppt,road_track,
     &     generate_river,river_branch,
     &     Nriver,river_width,river_Nppt,river_track)
      implicit none
      include 'max.inc'
      include 'city_parameters.inc'
c     
c     Purpose: to generate dirt roads
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Nroad: number of existing roads
c       + road_width: width for each road [m]
c       + road_Nppt: number of points for each road track
c       + road_track: track that define the position of each road
c       + generate_river: set to true if a river has to be generated
c       + river_branch: set to true if a river branch has to be generated
c       + Nriver: number of rivers that have been generated
c       + river_width: width of each river that has been generated
c       + river_Nppt: number of points for the description of the central track for each river
c       + river_track: list of positions that describe the central track of each river
c     
c     Output:
c       + Nroad, road_width, road_Nppt, road_track: updated values
c     
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Nroad
      double precision road_width(1:Nroad_mx)
      integer road_Nppt(1:Nroad_mx)
      double precision road_track(1:Nroad_mx,
     &     1:Nppt_mx,1:Ndim_mx-1)
      logical generate_river
      logical river_branch
      integer Nriver
      double precision river_width(1:2)
      integer river_Nppt(1:2)
      double precision river_track(1:2,1:Nppt_mx,1:Ndim_mx-1)
c     temp
      logical draw_white_bands
      integer i,j,itrack
      double precision xmin,xmax,ymin,ymax
      double precision Lmin,Lmax,L1,L2,L3,L4
      double precision central_track(1:Nppt_mx,1:Ndim_mx-1)
      double precision distance
      integer side
      double precision change_side
      integer Nppt,Nppt1,Nppt2
      double precision width
      double precision track(1:Nppt_mx,1:Ndim_mx-1)
      double precision track1(1:Nppt_mx,1:Ndim_mx-1)
      double precision track2(1:Nppt_mx,1:Ndim_mx-1)
      integer duplicated_Nppt
      double precision duplicated_track(1:Nppt_mx,1:Ndim_mx-1)
      character*(Nchar_mx) road_mat
      character*(Nchar_mx) wbands_mat
      integer Ndirtroad
      double precision dirtroad_width(1:Nroad_mx)
      integer dirtroad_Nppt(1:Nroad_mx)
      double precision dirtroad_track(1:Nroad_mx,
     &     1:Nppt_mx,1:Ndim_mx-1)
c     label
      character*(Nchar_mx) label
      label='subroutine generate_dirt_roads'

      draw_white_bands=.false.
      distance=river_width(1)/2.0D+0
     &     +river_dirtroad_distance
     &     +dirt_road_width/2.0D+0
      Ndirtroad=0
      if (generate_river) then
c     side: -1
         side=-1
         if (river_branch) then
            call extract_river_track(dim,Nriver,river_Nppt,
     &           river_track,1,Nppt1,track1)
            call extract_river_track(dim,Nriver,river_Nppt,
     &           river_track,2,Nppt2,track2)
            Nppt=Nppt1+Nppt2-1
            do i=1,Nppt1-1
               do j=1,dim-1
                  track(i,j)=track1(i,j)
               enddo            ! j
            enddo               ! i
            do i=1,Nppt2
               do j=1,dim-1
                  track(Nppt1-1+i,j)=track2(i,j)
               enddo            ! j
            enddo               ! i
         else                   ! river_branch=.false.
            call extract_river_track(dim,Nriver,river_Nppt,
     &           river_track,1,Nppt,track)
         endif
         call duplicate_track(dim,mapXsize,mapYsize,
     &        Nppt,track,distance,side,
     &        duplicated_Nppt,central_track)
         Ndirtroad=Ndirtroad+1
         if (Ndirtroad.gt.Nroad_mx) then
            call error(label)
            write(*,*) 'Ndirtroad=',Ndirtroad
            write(*,*) '> Nroad_mx=',Nroad_mx
            stop
         endif
         dirtroad_Nppt(Ndirtroad)=duplicated_Nppt
         dirtroad_width(Ndirtroad)=dirt_road_width
         do i=1,dirtroad_Nppt(Ndirtroad)
            do j=1,dim-1
               dirtroad_track(Ndirtroad,i,j)=central_track(i,j)
            enddo               ! j
         enddo                  ! i
c     generate definition file
         road_mat=trim(tracks_material)
         wbands_mat=trim(white_bands_material)
         call record_road_definition_file(dim,Nroad+Ndirtroad,
     &        dirtroad_width(Ndirtroad),
     &        dirtroad_Nppt(Ndirtroad),central_track,
     &        draw_white_bands,
     &        road_mat,wbands_mat)
c     
         if (river_branch) then
c     Upper part of the map
            call extract_river_track(dim,Nriver,river_Nppt,
     &           river_track,1,Nppt1,track1)
            call extract_river_track(dim,Nriver,river_Nppt,
     &           river_track,3,Nppt2,track2)
            Nppt=Nppt1+Nppt2-1
            do i=1,Nppt1-1
               do j=1,dim-1
                  track(i,j)=track1(i,j)
               enddo            ! j
            enddo               ! i
            do i=1,Nppt2
               do j=1,dim-1
                  track(Nppt1-1+i,j)=track2(i,j)
               enddo            ! j
            enddo               ! i
            side=1
            call duplicate_track(dim,mapXsize,mapYsize,
     &           Nppt,track,distance,side,
     &           duplicated_Nppt,central_track)
            Ndirtroad=Ndirtroad+1
            if (Ndirtroad.gt.Nroad_mx) then
               call error(label)
               write(*,*) 'Ndirtroad=',Ndirtroad
               write(*,*) '> Nroad_mx=',Nroad_mx
               stop
            endif
            dirtroad_Nppt(Ndirtroad)=duplicated_Nppt
            dirtroad_width(Ndirtroad)=dirt_road_width
            do i=1,dirtroad_Nppt(Ndirtroad)
               do j=1,dim-1
                  dirtroad_track(Ndirtroad,i,j)=central_track(i,j)
               enddo            ! j
            enddo               ! i
c     generate definition file
            road_mat=trim(tracks_material)
            wbands_mat=trim(white_bands_material)
            call record_road_definition_file(dim,Nroad+Ndirtroad,
     &           dirtroad_width(Ndirtroad),
     &           dirtroad_Nppt(Ndirtroad),central_track,
     &           draw_white_bands,
     &           road_mat,wbands_mat)
         else                   ! river_branch=.false.
            side=1
            call duplicate_track(dim,mapXsize,mapYsize,
     &           Nppt,track,distance,side,
     &           duplicated_Nppt,central_track)
            Ndirtroad=Ndirtroad+1
            if (Ndirtroad.gt.Nroad_mx) then
               call error(label)
               write(*,*) 'Ndirtroad=',Ndirtroad
               write(*,*) '> Nroad_mx=',Nroad_mx
               stop
            endif
            dirtroad_Nppt(Ndirtroad)=duplicated_Nppt
            dirtroad_width(Ndirtroad)=dirt_road_width
            do i=1,dirtroad_Nppt(Ndirtroad)
               do j=1,dim-1
                  dirtroad_track(Ndirtroad,i,j)=central_track(i,j)
               enddo            ! j
            enddo               ! i
c     generate definitio
            road_mat=trim(tracks_material)
            wbands_mat=trim(white_bands_material)
            call record_road_definition_file(dim,Nroad+Ndirtroad,
     &           dirtroad_width(Ndirtroad),
     &           dirtroad_Nppt(Ndirtroad),central_track,
     &           draw_white_bands,
     &           road_mat,wbands_mat)
         endif                  ! river_branch
      endif                     ! generate_river
c     
      do itrack=1,Ndirtroad
c     add to "road_track"
         call extract_road_track(dim,
     &        Ndirtroad,dirtroad_width,
     &        dirtroad_Nppt,dirtroad_track,
     &        itrack,width,Nppt,track)
         call add_road_track(dim,
     &        Nroad,road_Nppt,road_width,road_track,
     &        width,Nppt,track)
c     generate definition file
         road_mat=trim(tracks_material)
         wbands_mat=trim(white_bands_material)
         call record_road_definition_file(dim,Nroad,
     &        width,Nppt,track,
     &        draw_white_bands,
     &        road_mat,wbands_mat)
      enddo                     ! itrack
      
      return
      end



      subroutine duplicate_track(dim,mapXsize,mapYsize,
     &     Nppt,track,distance,side,
     &     duplicated_Nppt,duplicated_track)
      implicit none
      include 'max.inc'
c     
c     Purpose: to duplicate a given track, at a given distance
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Nppt: number of points for the track to duplicate
c       + track: track to duplicate   
c       + distance: duplication distance
c       + side: 1 for +normal side, -1 for -normal side
c
c     Output:
c       + duplicated_Nppt: number of points for the duplicated track
c       + duplicated_track: duplicated track
c     
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Nppt
      double precision track(1:Nppt_mx,1:Ndim_mx-1)
      double precision distance
      integer side
      integer duplicated_Nppt
      double precision duplicated_track(1:Nppt_mx,1:Ndim_mx-1)
c     temp
      double precision sign
      integer Nsegment,isegment,j,i
      double precision p0(1:Ndim_mx)
      double precision p1(1:Ndim_mx)
      double precision x1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision v(1:Ndim_mx)
      double precision n(1:Ndim_mx)
      double precision n_tmp(1:Ndim_mx)
      double precision n1(1:Ndim_mx)
      double precision n2(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine duplicate_track'

      if ((Nppt.lt.1).or.(Nppt.gt.Nppt_mx)) then
         call error(label)
         write(*,*) 'Nppt=',Nppt
         write(*,*) 'should be in the [1-',Nppt_mx,'] range'
         stop
      else
         duplicated_Nppt=Nppt
      endif
      if ((side.ne.-1).and.(side.ne.1)) then
         call error(label)
         write(*,*) 'side=',side
         write(*,*) 'should be equal to -1 or 1'
         stop
      else
         sign=dble(side)
      endif
      
      Nsegment=Nppt-1
      if (Nsegment.lt.1) then
         call error(label)
         write(*,*) 'Nsegment=',Nsegment
         write(*,*) 'should be > 0'
         stop
      endif
      
c     loop over segments
      do isegment=1,Nsegment
         do j=1,dim-1
            p1(j)=track(isegment,j)
            p2(j)=track(isegment+1,j)
         enddo                  ! j
         p1(dim)=0.0D+0
         p2(dim)=0.0D+0
         call normal_for_segment(dim,p1,p2,n)
c     n is the normal for the current segment
         if (isegment.eq.1) then
            call copy_vector(dim,n,n1)
         else                   ! isegment>1
            do j=1,dim-1
               p0(j)=track(isegment-1,j)
            enddo               ! j
            p0(dim)=0.0D+0
            call normal_for_segment(dim,p0,p1,n_tmp)
            call add_vectors(dim,n_tmp,n,u)
            call normalize_vector(dim,u,n1)
         endif                  ! isegment=1
         call scalar_vector(dim,sign,n1,n1)
c     n1 is the normal to use for P1
         call scalar_vector(dim,distance,n1,v)
         call add_vectors(dim,p1,v,x1)
c     add position "x1" to "duplicated_track"
         i=isegment
         do j=1,dim-1
            duplicated_track(i,j)=x1(j)
         enddo                  ! j
c     do it one last time for second point of last segment
         if (isegment.eq.Nsegment) then
            call copy_vector(dim,n,n2)
            call scalar_vector(dim,sign,n2,n2)
c     n2 is the normal to use for P2
            call scalar_vector(dim,distance,n2,v)
            call add_vectors(dim,p2,v,x2)
c     add position "x2" to "duplicated_track"
            i=Nsegment+1
            do j=1,dim-1
               duplicated_track(i,j)=x2(j)
            enddo               ! j
         endif                  ! isegment=Nsegment
      enddo                     ! isegment
         
      return
      end



      subroutine extract_road_track(dim,
     &     Nroad,road_width,road_Nppt,road_track,
     &     itrack,width,Nppt,track)
      implicit none
      include 'max.inc'
c     
c     Purpose: extract a track from the list of road tracks
c     
c     Input:
c       + dim: dimension of space
c       + Nroad: number of roads
c       + road_width: width of each road [m]
c       + road_Nppt: number of points that define the track of each road
c       + road_track: track of each road
c       + itrack: index of the track to extract
c     
c     Output:
c       + width: width of the extracted track [m]
c       + Nppt: number of points that define the extracted track
c       + track: extracted track
c     
c     I/O
      integer dim
      double precision road_width(1:Nroad_mx)
      integer Nroad
      integer road_Nppt(1:Nroad_mx)
      double precision road_track(1:Nroad_mx,
     &     1:Nppt_mx,1:Ndim_mx-1)
      integer itrack
      double precision width
      integer Nppt
      double precision track(1:Nppt_mx,1:Ndim_mx-1)
c     temp
      integer i,j
c     label
      character*(Nchar_mx) label
      label='subroutine extract_road_track'

      if (itrack.lt.1) then
         call error(label)
         write(*,*) 'itrack=',itrack
         write(*,*) 'should be > 0'
         stop
      endif
      if (itrack.gt.Nroad) then
         call error(label)
         write(*,*) 'itrack=',itrack
         write(*,*) 'should be < Nroad=',Nroad
         stop
      endif

      if (road_width(itrack).le.0.0D+0) then
         call error(label)
         write(*,*) 'road_width(',itrack,')=',road_width(itrack)
         write(*,*) 'should be positive'
         stop
      else
         width=road_width(itrack)
      endif                     ! road_width(itrack)<0
      if ((road_Nppt(itrack).lt.1).or.
     &     (road_Nppt(itrack).gt.Nppt_mx)) then
         call error(label)
         write(*,*) 'road_Nppt(',itrack,')=',road_Nppt(itrack)
         write(*,*) 'should be in the [1-',Nppt_mx,'] range'
         stop
      else
         Nppt=road_Nppt(itrack)
      endif                     ! road_Nppt(itrack) in range
      do i=1,Nppt
         do j=1,dim-1
            track(i,j)=road_track(itrack,i,j)
         enddo                  ! j
      enddo                     ! i

      return
      end



      subroutine add_road_track(dim,
     &     Nroad,road_Nppt,road_width,road_track,
     &     width,Nppt,track)
      implicit none
      include 'max.inc'
c     
c     Purpose: add a track to the list of road tracks
c     
c     Input:
c       + dim: dimension of space
c       + Nroad: number of roads
c       + road_width: width of each road [m]
c       + road_Nppt: number of points that define the track of each road
c       + road_track: track of each road
c       + width: width of the track to be added
c       + Nppt: number of points that define the track to be added
c       + track: track to be added
c     
c     Output:
c       updated values of Nroad, road_width, road_Nppt, road_track
c     
c     I/O
      integer dim
      integer Nroad
      double precision road_width(1:Nroad_mx)
      integer road_Nppt(1:Nroad_mx)
      double precision road_track(1:Nroad_mx,
     &     1:Nppt_mx,1:Ndim_mx-1)
      double precision width
      integer Nppt
      double precision track(1:Nppt_mx,1:Ndim_mx-1)
c     temp
      integer i,j
c     label
      character*(Nchar_mx) label
      label='subroutine add_road_track'

      Nroad=Nroad+1
      if (Nroad.gt.Nroad_mx) then
         call error(label)
         write(*,*) 'Nroad=',Nroad
         write(*,*) '> Nroad_mx',Nroad_mx
         stop
      else
         if (width.le.0.0D+0) then
            call error(label)
            write(*,*) 'width=',width
            write(*,*) 'should be positive'
            stop
         else
            road_width(Nroad)=width
         endif                  ! width<0
         if ((Nppt.lt.1).or.(Nppt.gt.Nppt_mx)) then
            call error(label)
            write(*,*) 'Nppt=',Nppt
            write(*,*) 'should be in the [1-',Nppt_mx,'] range'
            stop
         else
            road_Nppt(Nroad)=Nppt
         endif                  ! Nppt in range
         do i=1,Nppt
            do j=1,dim-1
               road_track(Nroad,i,j)=track(i,j)
            enddo               ! j
         enddo                  ! i
      endif                     ! Nroad>Nroad_mx

      return
      end
