      subroutine write_obj(dim,output_file,invert_normal,
     &     Nv,Nf,vertices,faces)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to write the main mesh to a wavefront file
c
c     Input:
c       + dim: dimension of the physical space
c       + output_file: name of the output wavefront file
c       + invert_normal: true if the direction of normals has to be inverted
c       + Nv: number of vertices
c       + Nf: number of faces
c       + vertices: array of verices definition (3D position of each vertex)
c       + faces: array of faces (index of vertices that belong to each face)
c
c     I/O
      integer dim
      character*(Nchar_mx) output_file
      logical invert_normal
      integer Nv,Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:3)
c     temp
      integer idim
      integer*8 iv,iface,tmp1
c     label
      character*(Nchar_mx) label
      label='subroutine write_obj'

      if (Nv.gt.10000000) then
         call error(label)
         write(*,*) 'Number of vertices:',Nv
         write(*,*) '> 10^7'
         write(*,*) 'modify format 22 in file formats.inc'
         stop
      else
c         write(*,31) 'Number of vertices:',Nv
      endif

      if (invert_normal) then
         do iface=1,Nf
            tmp1=faces(iface,2)
            faces(iface,2)=faces(iface,3)
            faces(iface,3)=tmp1
         enddo ! iface
      endif ! invert_normal

      open(12,file=trim(output_file))
      write(*,*) 'Recording wavefront file: ',
     &     trim(output_file)
      do iv=1,Nv
         write(12,21) 'v',(vertices(iv,idim),idim=1,dim)
      enddo ! iv
      do iface=1,Nf
         write(12,22) 'f',(faces(iface,iv),iv=1,3)
      enddo ! iface
      close(12)

      write(*,*) 'Output file has been successfuly generated:',
     &     trim(output_file)

      return
      end



      subroutine write_single_obj(dim,output_file,invert_normal,
     &     Nv,Nf,vertices,faces)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to write the mesh for a single surface to a wavefront file
c
c     Input:
c       + dim: dimension of the physical space
c       + output_file: name of the output wavefront file
c       + invert_normal: true if the direction of normals has to be inverted
c       + Nv: number of vertices
c       + Nf: number of faces
c       + vertices: array of verices definition (3D position of each vertex)
c       + faces: array of faces (index of vertices that belong to each face)
c
c     I/O
      integer dim
      character*(Nchar_mx) output_file
      logical invert_normal
      integer Nv,Nf
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:3)
c     temp
      integer idim
      integer*8 iv,iface,tmp1
c     label
      character*(Nchar_mx) label
      label='subroutine write_single_obj'

      if (Nv.gt.10000000) then
         call error(label)
         write(*,*) 'Number of vertices:',Nv
         write(*,*) '> 10^7'
         write(*,*) 'modify format 22 in file formats.inc'
         stop
      endif

      if (invert_normal) then
         do iface=1,Nf
            tmp1=faces(iface,2)
            faces(iface,2)=faces(iface,3)
            faces(iface,3)=tmp1
         enddo ! iface
      endif ! invert_normal

      open(12,file=trim(output_file))
      do iv=1,Nv
         write(12,21) 'v',(vertices(iv,idim),idim=1,dim)
      enddo ! iv
      do iface=1,Nf
         write(12,22) 'f',(faces(iface,iv),iv=1,3)
      enddo ! iface
      close(12)

      return
      end



      subroutine record_group(dim,output_file,invert_normal,
     &     usemtl,mtl_file,Ngroup,group_name,mtl_name,igroup,Nv1,
     &     Nv,Nf,vertices,faces)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to write the mesh for a single surface to a wavefront file
c
c     Input:
c       + dim: dimension of the physical space
c       + output_file: name of the output wavefront file
c       + invert_normal: true if the direction of normals has to be inverted
c       + usemtl: T if a material file is used
c       + mtl_file: name of the material file is one is used
c       + Ngroup: number of groups (0 if none)
c       + group_name: name of each group
c       + mtl_name: material used for each group (if usemtl=T)
c       + igroup: index of the group to record
c       + Nv1: number of vertices in group 1
c       + Nv: number of vertices
c       + Nf: number of faces
c       + vertices: array of verices definition (3D position of each vertex)
c       + faces: array of faces (index of vertices that belong to each face)
c
c     I/O
      integer dim
      character*(Nchar_mx) output_file
      logical invert_normal
      logical usemtl
      character*(Nchar_mx) mtl_file
      integer Ngroup
      character*(Nchar_mx) group_name(1:Ngroup_mx)
      character*(Nchar_mx) mtl_name(1:Ngroup_mx)
      integer igroup
      integer Nv1
      integer Nv,Nf
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:3)
c     temp
      integer idim
      integer*8 iv,iface,tmp1
      character*(Nchar_mx) output_file2
c     label
      character*(Nchar_mx) label
      label='subroutine record_group'

c     check input
      if (Nv.gt.10000000) then
         call error(label)
         write(*,*) 'Number of vertices:',Nv
         write(*,*) '> 10^7'
         write(*,*) 'modify format 22 in file formats.inc'
         stop
      endif
      if (Ngroup.le.0) then
         call error(label)
         write(*,*) 'Ngroup=',Ngroup
         write(*,*) 'should be > 0'
         stop
      endif
      if (Ngroup.gt.Ngroup_mx) then
         call error(label)
         write(*,*) 'Ngroup=',Ngroup
         write(*,*) 'should be <= Ngroup_mx=',Ngroup_mx
         stop
      endif
      if (igroup.lt.0) then
         write(*,*) 'igroup=',igroup
         write(*,*) 'should be >= 0'
         stop
      endif
      if (igroup.gt.Ngroup) then
         write(*,*) 'igroup=',igroup
         write(*,*) 'should be <= Ngroup=',Ngroup
         stop
      endif

c     invert normals if required
      if (invert_normal) then
         do iface=1,Nf
            tmp1=faces(iface,2)
            faces(iface,2)=faces(iface,3)
            faces(iface,3)=tmp1
         enddo ! iface
      endif ! invert_normal

      if (igroup.eq.0) then
         open(12,file=trim(output_file))
         if (usemtl) then
            write(12,10) 'mtllib '//trim(mtl_file)
         endif
      else
         open(12,file=trim(output_file),access='append')
         if (usemtl) then
            write(12,10) 'g '//trim(group_name(igroup))
            write(12,10) 'usemtl '//trim(mtl_name(igroup))
         endif
         do iv=1,Nv
            write(12,21) 'v',(vertices(iv,idim),idim=1,dim)
         enddo                  ! iv
         do iface=1,Nf
            write(12,22) 'f',(faces(iface,iv),iv=1,3)
         enddo                  ! iface
c     record as separate file
         if (igroup.eq.1) then
            output_file2=output_file(1:len_trim(output_file)-4)
     &           //'_trunk.obj'
         else if (igroup.eq.2) then
            output_file2=output_file(1:len_trim(output_file)-4)
     &           //'_foliage.obj'
            do iface=1,Nf
               do iv=1,3
                  faces(iface,iv)=faces(iface,iv)-Nv1
               enddo            ! iv
            enddo               ! iface
         else
            call error(label)
            write(*,*) 'igroup=',igroup
            stop
         endif
         open(14,file=trim(output_file2))
         do iv=1,Nv
            write(14,21) 'v',(vertices(iv,idim),idim=1,dim)
         enddo                  ! iv
         do iface=1,Nf
            write(14,22) 'f',(faces(iface,iv),iv=1,3)
         enddo                  ! iface
         close(14)
c     
      endif                     ! igroup
      close(12)
      

      return
      end
