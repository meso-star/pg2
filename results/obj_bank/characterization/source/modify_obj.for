      subroutine check_vertex_indexes(Nf_so,f_so)
      implicit none
      include 'max.inc'
c
c     Purpose: to check for inconsistencies in the vertex indexes
c     for each face
c
c     Input:
c       + Nf_so: number of faces
c       + f_so: faces
c
c     Output:
c       + f_so: updated
c
c     I/O
      integer Nf_so
      integer f_so(1:Nf_so_mx,1:3)
c     temp
      integer if
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine check_vertex_indexes'

      do if=1,Nf_so
         if (f_so(if,1).eq.f_so(if,2)) then
            call error(label)
            write(*,*) 'f_so(',if,',1)=',f_so(if,1)
            write(*,*) 'f_so(',if,',2)=',f_so(if,2)
            stop
         endif
         if (f_so(if,1).eq.f_so(if,3)) then
            call error(label)
            write(*,*) 'f_so(',if,',1)=',f_so(if,1)
            write(*,*) 'f_so(',if,',3)=',f_so(if,3)
            stop
         endif
         if (f_so(if,2).eq.f_so(if,3)) then
            call error(label)
            write(*,*) 'f_so(',if,',2)=',f_so(if,2)
            write(*,*) 'f_so(',if,',3)=',f_so(if,3)
            stop
         endif
      enddo ! i

      return
      end


      
      subroutine rotate_obj(dim,Nv_so,v_so,M)
      implicit none
      include 'max.inc'
c
c     Purpose: to rotate an object
c
c     Input:
c       + dim: dimension of space
c       + Nv_so: number of vertices
c       + v_so: vertices
c       + M: rotation matrix
c
c     Output:
c       + v_so: updated
c
c     I/O
      integer dim
      integer Nv_so
      double precision v_so(1:Nv_so_mx,1:Ndim_mx)
      double precision M(1:Ndim_mx,1:Ndim_mx)
c     temp
      integer iv,i
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine rotate_obj'

      do iv=1,Nv_so
         do i=1,dim
            x1(i)=v_so(iv,i)
         enddo ! i
         call matrix_vector(dim,M,x1,x2)
         do i=1,dim
            v_so(iv,i)=x2(i)
         enddo ! i
      enddo ! iv

      return
      end



      subroutine move_obj(dim,Nv_so,v_so,center)
      implicit none
      include 'max.inc'
c
c     Purpose: to move an object
c
c     Input:
c       + dim: dimension of space
c       + Nv_so: number of vertices
c       + v_so: vertices
c       + center: cartesian coordinates of the new center
c
c     Output:
c       + v_so: updated
c
c     I/O
      integer dim
      integer Nv_so
      double precision v_so(1:Nv_so_mx,1:Ndim_mx)
      double precision center(1:Ndim_mx)
c     temp
      integer iv,i
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine move_obj'

      do iv=1,Nv_so
         do i=1,dim
            v_so(iv,i)=v_so(iv,i)+center(i)
         enddo ! i
      enddo ! iv

      return
      end



      subroutine scale_obj(dim,Nv_so,v_so,scale)
      implicit none
      include 'max.inc'
c
c     Purpose: to scale an object
c
c     Input:
c       + dim: dimension of space
c       + Nv_so: number of vertices
c       + v_so: vertices
c       + scale: scaling ratio
c
c     Output:
c       + v_so: updated
c
c     I/O
      integer dim
      integer Nv_so
      double precision v_so(1:Nv_so_mx,1:Ndim_mx)
      double precision scale
c     temp
      integer iv,i
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine scale_obj'

      if (scale.le.0.0D+0) then
         call error(label)
         write(*,*) 'scale=',scale
         write(*,*) 'should be positive'
         stop
      endif

      do iv=1,Nv_so
         do i=1,dim
            v_so(iv,i)=v_so(iv,i)*scale
         enddo ! i
      enddo ! iv

      return
      end
