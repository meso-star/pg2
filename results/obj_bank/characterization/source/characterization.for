      program characterization
      implicit none
      include 'max.inc'
      include 'formats.inc'
      include 'param.inc'
c      
c     Purpose: to extract various characteristics about normalized tree models
c      
c     Variables
      integer dim
      character*(Nchar_mx) datafile
      character*(Nchar_mx) obj_file
      character*(Nchar_mx) output_file
      integer Nv,Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:3)
      integer Nv_so,Nf_so,Nv1
      double precision v_so(1:Nv_so_mx,1:Ndim_mx)
      integer f_so(1:Nf_so_mx,1:3)
      integer iv,i,j,iface,igroup
      double precision extrema(1:Ndim_mx,1:2)
      double precision x(1:Ndim_mx),d,r
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision p3(1:Ndim_mx)
      double precision area,farea
      integer Nint,int_idx
      double precision int_z(1:Nint_mx,1:2),deltaZ
      double precision int_area(1:Nint_mx)
      integer int_p1,int_p2,int_p3
      integer int_min,int_max
      double precision sum_int_area
      integer imodel,Nmodel
      character*2 str2
      logical file_exist
c     label
      character*(Nchar_mx) label
      label='program characterization'

      dim=3

c     Number of obj files to analyze
      Nmodel=8

c     Output file
      output_file='./results/normalized_trees_characteristics.txt'
c     Number of intervals for obj area sorting
      Nint=10
      if (Nint.gt.Nint_mx) then
         call error(label)
         write(*,*) 'Nint=',Nint
         write(*,*) '> Nint_mx=',Nint_mx
         stop
      endif

      open(21,file=trim(output_file))

      do imodel=1,Nmodel
         call num2str2(imodel,str2)
         obj_file='./data/Tree_'//trim(str2)//'_foliage.obj'
         inquire(file=trim(obj_file),exist=file_exist)
         if (.not.file_exist) then
            call error(label)
            write(*,*) 'No obj file found for model index:',imodel
            stop
         endif
         call read_obj(obj_file,dim,Nv_so,Nf_so,v_so,f_so)

c     Identify min and max for each direction
         do i=1,dim
            extrema(i,1)=v_so(1,i)
            extrema(i,2)=v_so(1,i)
            do iv=1,Nv_so
               if (v_so(iv,i).lt.extrema(i,1)) then
                  extrema(i,1)=v_so(iv,i)
               endif
               if (v_so(iv,i).gt.extrema(i,2)) then
                  extrema(i,2)=v_so(iv,i)
               endif
            enddo               ! iv
         enddo                  ! i
         
c     Radius of the smallest circle that encompases the obj
         r=0.0D+0
         do iv=1,Nv_so
            do i=1,dim-1
               x(i)=v_so(iv,i)
            enddo               ! i
            x(dim)=0.0D+0
            call vector_length(dim,x,d)
            if (d.gt.r) then
               r=d
            endif
         enddo                  ! iv
         write(21,*) 'Model index:',imodel
         write(21,*) 'Radius [m]'
         write(21,*) r

c     Total area of the obj and area in intervals
         area=0.0D+0
         do int_idx=1,Nint
            int_area(int_idx)=0.0D+0
         enddo                  ! int_idx
         deltaZ=(extrema(dim,2)-extrema(dim,1))/dble(Nint)
         do iface=1,Nf_so
            do i=1,dim
               p1(i)=v_so(f_so(iface,1),i)
               p2(i)=v_so(f_so(iface,2),i)
               p3(i)=v_so(f_so(iface,3),i)
            enddo               ! i
            call triangle_area(dim,p1,p2,p3,farea)
            area=area+farea
            if (p1(dim).eq.extrema(dim,2)) then
               int_p1=Nint
            else
               int_p1=int((p1(dim)-extrema(dim,1))/deltaZ)+1
            endif
            if (p2(dim).eq.extrema(dim,2)) then
               int_p2=Nint
            else
               int_p2=int((p2(dim)-extrema(dim,1))/deltaZ)+1
            endif
            if (p3(dim).eq.extrema(dim,2)) then
               int_p3=Nint
            else
               int_p3=int((p3(dim)-extrema(dim,1))/deltaZ)+1
            endif
            if (int_p1.lt.int_p2) then
               int_min=int_p1
            else
               int_min=int_p2
            endif
            if (int_p3.lt.int_min) then
               int_min=int_p3
            endif
            if (int_p1.gt.int_p2) then
               int_max=int_p1
            else
               int_max=int_p2
            endif
            if (int_p3.gt.int_max) then
               int_max=int_p3
            endif
c     Affect the area of the triangle to every intersected interval
c            do int_idx=int_min,int_max
c               int_area(int_idx)=int_area(int_idx)+farea
c             enddo               ! int_idx
c
c     Affect the area of the triangle only to the first interval
            int_area(int_min)=int_area(int_min)+farea
         enddo                  ! iface

         write(21,*) 'Total area [m²]'
         write(21,*) area
         write(21,*) 'Number of intervals'
         write(21,*) Nint
         write(21,*) 'Interval limits [m]'
         write(21,*) extrema(dim,1)
         do int_idx=1,Nint
            write(21,*) extrema(dim,1)+deltaZ*int_idx
         enddo                  ! int_idx
         write(21,*) 'Area per interval [m²]'
         do int_idx=1,Nint
            write(21,*) int_area(int_idx)
         enddo                  ! int_idx
         if (imodel.lt.Nmodel) then
            write(21,*)
         endif
      enddo                     ! imodel

      close(21)
      write(*,*) 'Output file was generated:'
      write(*,*) trim(output_file)
      
      end
