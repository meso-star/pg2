	double precision pi
	double precision Pl
	double precision AU
	double precision M_sun
	double precision M_earth
	double precision R_sun
	double precision R_earth
	double precision Earth_obliquity
	double precision Earth_synodic_period
	double precision Earth_sidereal_period
	double precision Earth_angular_speed
	double precision Earth_anomalistic_year
	double precision G
	double precision kBz
	double precision c0
	double precision hPl
	double precision Td,Tu,deltaT
        parameter(pi=3.141592653589793238462643383279502884197169399D+0)
        parameter(Pl=5.670399D-8)      ! W/m2/K4 (constante de Stefan-Boltzman)
	parameter(AU=149597870.691D+3) ! m
	parameter(M_sun=1.9884D+30) ! kg
	parameter(M_earth=5.9722D+24) ! kg
	parameter(R_sun=6.95991756D+8) ! m
	parameter(R_earth=6378.136D+3) ! m
c	-------------------------------------------------
c	parameter(Earth_anomalistic_year=365.259635864D+0) ! days of 24h
	parameter(Earth_anomalistic_year=360.0D+0) ! days of 24h
c	-------------------------------------------------
	parameter(Earth_obliquity=23.45D+0) ! deg
c	parameter(Earth_obliquity=23.58D+0) ! deg
c	-------------------------------------------------
c	Earth_synodic_period: time taken to be in the same position relative to the sun (def. of the day)
	parameter(Earth_synodic_period=24.0D+0) ! hours
c	-------------------------------------------------
c	Earth_sidereal_period: time taken to be in the same postion relative to distant stars
c	parameter(Earth_sidereal_period=23.93419D+0) ! hours
	parameter(Earth_sidereal_period=Earth_synodic_period
     &           *Earth_anomalistic_year
     &          /(Earth_anomalistic_year+1.0D+0)) ! hours
c	-------------------------------------------------
c	Earth angular speed (sidereal):
	parameter(Earth_angular_speed=2.0D+0*pi/Earth_sidereal_period) ! rad/hour
c	-------------------------------------------------
	parameter(G=6.6742867D-11) ! m^3.kg^-1.s^-2
        parameter(kBz=1.3806D-23)    ! J/K     (Constante de Boltzmann)
        parameter(c0=2.9979D+8)      ! m/s     (c<E9>l<E9>rit<E9> de la lumi<E8>re dans le vide)
        parameter(hPl=6.6262D-34)    ! Js      (constante de Planck)

	parameter(Td=300.0D+0)
	parameter(Tu=320.0D+0)
	parameter(deltaT=10.0D+0)