      subroutine read_data(dim,datafile,
     &     obj_file,displacement,rotation)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read the input data file for program "check_obj"
c     
c     Input:
c       + dim: dimension of space
c       + datafile: name of the file to read
c     
c     Output:
c       + obj_file: name of the OBJ file
c       + displacement: displacement in each direction
c       + rotation: rotation over each axis
c     
c     I/O
      integer dim
      character*(Nchar_mx) datafile
      character*(Nchar_mx) obj_file
      double precision displacement(1:Ndim_mx)
      double precision rotation(1:Ndim_mx)
c     temp
      integer i,j,ios
c     label
      character*(Nchar_mx) label
      label='subroutine read_data'

      open(11,file=trim(datafile),iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(datafile)
         stop
      else
         do i=1,3
            read(11,*)
         enddo                  ! i
         read(11,*) obj_file
         read(11,*)
         do j=1,dim
            read(11,*) displacement(j)
         enddo                  ! j
         read(11,*)
         do j=1,dim
            read(11,*) rotation(j)
         enddo                  ! j
      endif
      close(11)

      return
      end
