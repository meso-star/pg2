      program check_obj
      implicit none
      include 'max.inc'
      include 'param.inc'
c      
c     Purpose: general purpose program to check various aspects
c     on a given obj mesh
c      
c     Variables
      character*(Nchar_mx) datafile
      character*(Nchar_mx) obj_file
      double precision displacement(1:Ndim_mx)
      double precision rotation(1:Ndim_mx)
      character*(Nchar_mx) data_file,temp_file,output_file
      integer dim
      integer Nv,Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:3)
      integer Nv_so,Nf_so
      double precision v_so(1:Nv_so_mx,1:Ndim_mx)
      integer f_so(1:Nf_so_mx,1:3)
      integer iv,i,j,iface
      double precision extrema(1:Ndim_mx,1:2)
      double precision alpha
      double precision axe(1:Ndim_mx)
      double precision M(1:Ndim_mx,1:Ndim_mx)
      logical invert_normal
      double precision lmax,scale
c     label
      character*(Nchar_mx) label
      label='program check_obj'

      dim=3
      datafile='./data.in'
      call read_data(dim,datafile,
     &     obj_file,displacement,rotation)
      
      Nv=0
      Nf=0
      data_file='./data/'//trim(obj_file)
      temp_file='./data/temp.obj'
      call parse_obj(data_file,temp_file)
      call read_obj(temp_file,dim,
     &     Nv_so,Nf_so,v_so,f_so)
c     Debug
      write(*,*) 'Nvertices=',Nv_so
      write(*,*) 'Nfaces=',Nf_so
c     Debug

      do j=1,dim
         do i=1,2
            extrema(j,i)=v_so(1,j)
         enddo ! i
      enddo ! j
      do iv=1,Nv_so
         do j=1,dim
            if (v_so(iv,j).lt.extrema(j,1)) then
               extrema(j,1)=v_so(iv,j)
            endif
            if (v_so(iv,j).gt.extrema(j,2)) then
               extrema(j,2)=v_so(iv,j)
            endif
         enddo ! j
      enddo                     ! iv
c     Debug
      do j=1,dim
         write(*,*) 'extrema(',j,')=',(extrema(j,i),i=1,2)
      enddo ! j
c     Debug
      lmax=0.0D+0
c      do j=1,dim
c         if (dabs(extrema(j,2)-extrema(j,1)).gt.lmax) then
c            lmax=dabs(extrema(j,2)-extrema(j,1))
c         endif
c     enddo ! j
      lmax=extrema(2,2)-extrema(2,1)

c     move and rotate object
      do j=1,dim
         if (rotation(j).ne.0.0D+0) then
            do i=1,dim
               axe(i)=0.0D+0
            enddo               ! i
            axe(j)=1.0D+0
            alpha=rotation(j)*pi/180.0D+0
            call rotation_matrix(dim,alpha,axe,M)
            call rotate_obj(dim,Nv_so,v_so,M)
         endif                  ! rotation(j).ne.0
      enddo                     ! j
      call move_obj(dim,Nv_so,v_so,displacement)
c     normalize
c     ---------------------------------------------------------------
      if (lmax.gt.0.0D+0) then
         scale=1.0D+0/lmax
         call scale_obj(dim,Nv_so,v_so,scale)
      endif
      do j=1,dim
         do i=1,2
            extrema(j,i)=v_so(1,j)
         enddo ! i
      enddo ! j
      do iv=1,Nv_so
         do j=1,dim
            if (v_so(iv,j).lt.extrema(j,1)) then
               extrema(j,1)=v_so(iv,j)
            endif
            if (v_so(iv,j).gt.extrema(j,2)) then
               extrema(j,2)=v_so(iv,j)
            endif
         enddo ! j
      enddo                     ! iv
c     Debug
      do j=1,dim
         write(*,*) 'extrema(',j,')=',(extrema(j,i),i=1,2)
      enddo ! j
c     Debug
c     ---------------------------------------------------------------
c     add obj to global obj and write it
      invert_normal=.false.
      call add2obj(dim,
     &     Nv,Nf,vertices,faces,
     &     invert_normal,
     &     Nv_so,Nf_so,v_so,f_so)
      output_file='./results/'//trim(obj_file)
      call write_obj(dim,output_file,invert_normal,
     &     Nv,Nf,vertices,faces)
      
      end
      
