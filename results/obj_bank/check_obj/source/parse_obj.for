      subroutine parse_obj(data_file,output_file)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to parse a given obj file and extract only vertices and faces definitions
c     
c     Input:
c       + data_file: input data file
c
c     Output:
c       + output_file: output data file
c
c     I/O
      character*(Nchar_mx) data_file
      character*(Nchar_mx) output_file
c     temp
      integer ios,iline,Nlines
      character*(Nchar_mx) ch_line
c     label
      character*(Nchar_mx) label
      label='subroutine parse_obj'

      call get_nlines(data_file,Nlines)
      
      open(11,file=trim(data_file)
     &     ,status='old',iostat=ios)
      if (ios.ne.0) then        ! file not found
         call error(label)
         write(*,*) 'file could not be found:'
         write(*,*) trim(data_file)
         stop
      else
         write(*,*) 'reading file: ',trim(data_file)
         open(12,file=trim(output_file))
         do iline=1,Nlines
            read(11,10) ch_line
            if ((iline.eq.1).or.
     &           (ch_line(1:2).eq.'v ').or.
     &           (ch_line(1:1).eq.'f')) then
               write(12,10) trim(ch_line)
            endif
         enddo                  ! iline
         close(12)
         write(*,*) 'Temporary file was generated: ',
     &        trim(output_file)
      endif
      close(11)

      return
      end
      
