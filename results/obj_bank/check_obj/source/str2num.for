      subroutine str2num(str,num,error)
      implicit none
      include 'max.inc'
c
c     Purpose: to convert a character string to an integer
c
c     Inputs:
c       + str: character string
c
c     Outputs:
c       + num: integer value
c       + error: 0 indicates no error; 1 indicates "str" contains non-numeric characters
c

c     I/O
      character*(Nchar_mx) str
      integer num,error
c     temp
      integer absnum,index
      integer n,i,sign,first
      character*1 char
      integer strlen
      character*(Nchar_mx) label
      label='subroutine str2num'

      n=strlen(str)
c     Debug
c      write(*,*) 'str="',str(1:strlen(str)),'"'
c      write(*,*) 'n=',n
c     Debug
      if (str(1:1).eq.'-') then
         sign=-1
         first=2
      else
         sign=1
         first=1
      endif
c     Debug
c      write(*,*) 'sign=',sign,' first=',first
c     Debug

      absnum=0
      error=0
      do i=first,n
         char=str(i:i)
         call identify_index(char,index)
c     Debug
c         write(*,*) 'index=',index
c     Debug
         if (index.eq.-1) then
            error=1
            goto 666
         endif
         absnum=10*absnum+index
      enddo ! i

      if (sign.eq.-1) then
         num=-absnum
      else
         num=absnum
      endif

 666  continue
      return
      end


      
      subroutine identify_index(char,index)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to identify the index that corresponds to the input character
c
c     Input:
c       + char: single character
c
c     Outputs:
c       + index: numerical index (0-9); a value of -1 indicates "char" is a non-numeric character
c

c     I/O
      character*1 char
      integer index
c     temp
      integer i,indexf
      character*1 ich
      integer strlen
      character*(Nchar_mx) label
      label='subroutine identify_index'

      indexf=0
      do i=0,9
         write(ich,11) i
         if (char(1:strlen(char)).eq.ich(1:strlen(ich))) then
            indexf=1
            index=i
c     Debug
c            write(*,*) 'ich=',ich(1:strlen(ich)),' indexf=',indexf,' e'
c     Debug
            goto 111
         endif
c     Debug
c         write(*,*) 'ich=',ich(1:strlen(ich)),' indexf=',indexf
c     Debug
      enddo ! i
 111  continue
      if (indexf.eq.0) then
         index=-1
      endif

      return
      end

      
