# Unit of area: square meters
# Area of ground (includes everything):
   18000.000000000000     
# Area of rivers:
   0.0000000000000000     
# Area of bridges:
   0.0000000000000000     
# Area of roads (including central plazza):
   0.0000000000000000     
# Area of districts (includes terrains and streets):
   1600.0000000000000     
# Area of terrains (includes buildings, walls & hedges, courtyards and parks):
   11200.000000000000     
# Area of streets:
  -9600.0000000000000     
# Area of buildings:
   2105.0395737450863     
# Area of courtyards:
   0.0000000000000000     
# Area of parks (terrain - walls & hedges):
   0.0000000000000000     
# Area of walls & hedges:
   0.0000000000000000     
